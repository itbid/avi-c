import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { SpinnerOverlayService } from '../spinner-overlay/spinner-overlay.service'
import { RxStompService } from '@stomp/ng2-stompjs';
import { Message } from '@stomp/stompjs';
import { Subcategoria } from '../db_classes/subcategoria';
import { Comprador } from '../db_classes/comprador';
import { Recommendation } from '../db_classes/recommendation';
import { ChartDataset, ChartType } from 'chart.js';
// import { Subscription } from 'rxjs';


@Component({
  selector: 'app-recommender',
  templateUrl: './recommender.component.html',
  styleUrls: ['./recommender.component.scss']
})
export class RecommenderComponent implements OnInit, OnDestroy {
  public selectedSubcategory: number | undefined;
  public receivedMessage: string;
  public compradores: Comprador[] = [];
  public compradorSeleccionado: Comprador;
  public subcategorias: Subcategoria[] = [];
  public subcategoriaSeleccionada: Subcategoria;
  public baseline: number | undefined;
  public pesoAhorro: number | undefined;
  public pesoProductividad: number | undefined;
  public pesoCompetitividad: number | undefined;
  public screenNumber: number | undefined;
  public recomendaciones: Recommendation[] = [];
  public recommendacionSeleccionada: Recommendation;
  public totalProveedores: number | undefined;
  
  public chartType: ChartType = 'radar';
  chartData: ChartDataset[] = [];
  chartLabels: string[] = [];
  chartOptions = {
    responsive: true,
    maintainAspectRatio: false
  };
  chartLegend = true;
  chartPlugins = [];

  // public topicSubscription: Subscription;

  constructor(private rxStompService: RxStompService, private spinner: SpinnerOverlayService) { 
    this.receivedMessage = "";
    this.pesoAhorro = 0;
    this.pesoProductividad = 0;
    this.pesoCompetitividad = 0;
    this.compradorSeleccionado = new Comprador();
    this.subcategoriaSeleccionada = new Subcategoria();
    this.recommendacionSeleccionada = new Recommendation();
    this.totalProveedores = 0;
    this.screenNumber = 0;

    this.chartLabels = ['confianza', 'kpi', 'competitividad', 'productividad', 'ahorro'];
    this.chartData = [];
  }

  ngOnInit(): void {
    // this.clients = this.service.getClients();

    //this.topicSubscription = 
    this.rxStompService
      .watch('results')
      .subscribe((message: Message) => {
        // ===================================================
        // Message Received
        // ===================================================
        this.spinner.hide();

        try {
          let json_message = JSON.parse(message.body);
          this.receivedMessage = json_message;

          if(typeof this.receivedMessage === 'object') {
            if (this.receivedMessage['from'] == 'get_interface_data') {
              this.compradores = this.receivedMessage['results']['compradores'];
              this.subcategorias = this.receivedMessage['results']['subcategorias'];
              this.baseline  = this.receivedMessage['results']['baseline'];
              this.pesoAhorro  = this.receivedMessage['results']['peso_ahorro'] * 1.0;
              this.pesoProductividad  = this.receivedMessage['results']['peso_productividad'];
              this.pesoCompetitividad  = this.receivedMessage['results']['peso_competitividad'];
            }
            else {
              if (this.receivedMessage['from'] == 'get_recommendation') {
                this.recomendaciones = this.receivedMessage['results']['recomendaciones'];
                this.totalProveedores = this.receivedMessage['results']['total_proveedores'];
                this.screenNumber = 1;

                this.chartData = [];

                for (let i = 0; i < this.recomendaciones.length; i++) {
                  this.chartData.push({
                    data: [
                      this.recomendaciones[i].confianza,
                      this.recomendaciones[i].kpi,
                      this.recomendaciones[i].competitividad,
                      this.recomendaciones[i].productividad,
                      this.recomendaciones[i].ahorro
                    ],
                    label: this.recomendaciones[i].tipo
                  });
                }
              }
            }
          }
          
        }
        catch(error:any) {
          this.receivedMessage = error.toString();
        }
      });

      this.rxStompService.publish({destination: 'clients', body: 'get_interface_data'});

  
  }
  
  ngAfterViewInit () {
    // Ahora puedes utilizar el radar-chart component
  }

  ngOnDestroy() {
    //this.topicSubscription.unsubscribe();
  }

  onSendMessage(message: string) {
    // ===================================================
    // Message Sent
    // ===================================================
    this.rxStompService.publish({destination: 'clients', body: message});
    this.spinner.show("");
  }

  onObtenerRecomendacion() {
    if (this.subcategoriaSeleccionada.id == 0) {
      alert("Seleccione la subcategoría");
    }
    else {
      if (!this.baseline) {
        alert("Indique el baseline");
      }
      else {
        if (!this.pesoAhorro || !this.pesoProductividad || !this.pesoCompetitividad) {
          alert("Indique unos pesos válidos");
        }
        else {
          this.onSendMessage(this.subcategoriaSeleccionada.id.toString() + "," + 
                            this.pesoAhorro.toString() + "," +
                            this.pesoProductividad.toString() + "," +
                            this.pesoCompetitividad.toString());
        }
      }
    }
  }

  onVerDetalle(recomendacion:Recommendation) {
    this.recommendacionSeleccionada = recomendacion;
    this.screenNumber = 2;
  }

  onVolverARecomendador() {
    this.screenNumber = 0;
  }

  onVolverARecomendaciones() {
    this.screenNumber = 1;
  }

}
