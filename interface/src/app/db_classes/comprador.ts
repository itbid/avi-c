export class Comprador {
    id: number;
    nombre: string;
    negociaciones: number;

    constructor() { 
        this.id = 0;
        this.nombre = '';
        this.negociaciones = 0;
    }
}
