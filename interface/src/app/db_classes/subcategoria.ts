export class Subcategoria {
    id: number;
    nombre: string;
    proveedores_invitados: number;

    constructor() { 
        this.id = 0;
        this.nombre = '';
        this.proveedores_invitados = 0;
    }
}
