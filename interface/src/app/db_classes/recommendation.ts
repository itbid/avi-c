import { Criterio } from './criterio';

export class Recommendation {
    id: number;
    tipo: string;
    duracion: number;
    tiempo_lanzamiento: number;
    proveedores_participantes: number;
    criterios_economicos: Criterio[];
    criterios_tecnicos: Criterio[];
    baseline: number;
    kpi: number;
    ahorro: number;
    confianza: number;
    competitividad: number;
    productividad: number;
    url: string;

    constructor() { 
        this.id = -1;
        this.tipo = '';
        this.duracion = 0;
        this.tiempo_lanzamiento = 0;
        this.proveedores_participantes = 0;
        this.criterios_economicos = [];
        this.criterios_tecnicos = [];
        this.baseline = 0;
        this.kpi = 0;
        this.ahorro = 0;
        this.confianza = 0;
        this.competitividad = 0;
        this.productividad = 0;
        this.url = '';
    }
}