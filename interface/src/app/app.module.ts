import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {
  InjectableRxStompConfig,
  RxStompService,
  rxStompServiceFactory,
} from '@stomp/ng2-stompjs';
import { myRxStompConfig } from './my-rx-stomp.config';
import { AppRoutingModule } from './app-routing.module';
import { RecommenderComponent } from './recommender/recommender.component';
import { SpinnerOverlayComponent } from './spinner-overlay/spinner-overlay.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { NgChartsModule } from 'ng2-charts';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    RecommenderComponent,
    SpinnerOverlayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OverlayModule,
    NgChartsModule,
    FormsModule
  ],
  providers: [
  {
    provide: InjectableRxStompConfig,
    useValue: myRxStompConfig,
  },
  {
    provide: RxStompService,
    useFactory: rxStompServiceFactory,
    deps: [InjectableRxStompConfig],
  }],
  bootstrap: [RecommenderComponent],
  entryComponents: [
    SpinnerOverlayComponent
  ]  
})
export class AppModule { }
