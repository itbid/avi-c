# Contributing to AVI

The main developer branch is `develop`, and the release branch is `master`.
Both of them are protected and no one can commit on them except through a merge request.

So this is what you should do each time you want to contribute:

1. Start with a clean repository (either `git clone`, or using `git stash...`, etc.)
2. `$ git fetch`
3. `$ git checkout develop`
4. `$ git branch -b feature/my-wonderful-feature`
5. Add your code, tests, etc.
6. `$ git commit -m "Put a descriptive message on what are yo doing"`
7. Incorporate any changes from `develop`: `$ git fetch && git merge develop`.
   * Resolve any conflicts, giving priority to the `develop` branch.
     If there is no clear way to resolve the conflicts, discuss them with the last
     `develop` commiter(s).
   * Commit any changes derived from the merge
8. Push your final work to the GitLab server: `$ git push`
9. Enter the GitLab site and make a merge request.
10. Rinse and repeat...


## Work with Docker containers

* To start all containers:
  $ docker-compose up --build -d --remove-orphans
* To stop all containers and their volumes:
  $ docker-compose down --volumes
* To know the status of your containers:
  $ docker ps -a
* To known why a container exited:
  $ docker logs -t <container>
* To know the IP of a container (search for IPAddress):
  $ docker inspect <container>
* To enter inside a running container:
  $ docker exec -it <container> bash
  $ exit
* To run an stopped container:
  $ docker-compose up <container>
  Please, do not use "docker run" because the container must be in the same "network" that the other containers to work well.
* To start a running container:
  $ docker-compose restart <container>
* To remove all exited containers:
  $ docker rm -v $(docker ps -a -q -f status=exited)

## AI Model Development Instructions

To develop the AI model:

1. Disable the model execution at "model/init.sh" by commenting this line:
   # python ./model.py &
2. Start all containers:
   $ ./install.sh
3. Enter inside the model container:
   $ docker exec -it model bash
5. Work on model.py
6. Run the model:
   python3 ./model.py
7. Use the interface for sending data to the model:
   http://localhost:4200/
8. Inspect the model output
9. When done, uncomment the "model/init.sh" line:
   python ./model.py &
10. Restart the container
   docker-compose restart model

## Interface Development Instructions

To develop the interface:

1. Start all containers:
   $ ./install.sh
2. Access the interface at this url:
   http://localhost:4200/
3. Work on interface/src/app
4. To solve "Failed to compile" do:
   docker logs -t interface


## Code Quality

Since this project is developed using Python framework you should follow most recommendations from PEP and pass
[`flake8`](https://flake8.pycqa.org/en/latest/) tool on your code.

```
python3 -m pip install flake8

flake8 path/to/code/to/check.py  --ignore E501
# or
flake8 path/to/code/
```

There most important ones are:

* Proper indentation: no tabs, four spaces.
* Do not use wildcard imports like `from whatever import *`, use explicit imports: `from whatever import ThatThing`
* Do not leave commented code, and above all docstring code (''') blocks.
  Use [docstring tests](https://docs.python.org/3/library/doctest.html) if you care to, 
  or proper [`logging`](https://docs.python.org/3/library/logging.html).
* Your code *should not* break any preceding code (see testing).
* Avoid using *cath-all* exceptions: `try:... except:...`.
  Whenever possible use explicit exceptions like `try:... except
  ObjectDoesNotExist`, or similar.
* Do not put personal information on the code (no names, no emails, etc.)


### Testing

Whenever possible, test your code at least at a basic level.

### Running

There are some ways of running this project, please read the /README.md file to know how to.


