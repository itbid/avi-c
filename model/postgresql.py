#!/usr/bin/env python

"""postgresql.py: This script opens a connection to a PostgreSQL database."""

__author__ = "UDT"

import psycopg2  # pip3 install psycopg2-binary

POSTGRESQL_HOST = 'localhost'
POSTGRESQL_USER = 'root'
POSTGRESQL_PASS = 'root'
POSTGRESQL_DB = 'rhcitquot_251'
POSTGRESQL_PORT = '5432'


def main():
    # Establishing the connection
    conn = psycopg2.connect(
        database=POSTGRESQL_DB,
        user=POSTGRESQL_USER,
        password=POSTGRESQL_PASS,
        host=POSTGRESQL_HOST,
        port=POSTGRESQL_PORT)

    # Setting auto commit
    # conn.autocommit = True

    # Creating a cursor object using the cursor() method
    cursor = conn.cursor()

    # Retrieving data
    cursor.execute('''select * from public.client order by id''')

    # Fetching all rows from the table
    result = cursor.fetchall()
    print(result)

    # Commit your changes in the database
    # conn.commit()

    # Closing the connection
    conn.close()


if __name__ == "__main__":
    main()
