#!/usr/bin/env python

import sys
import os
import udt_rabbitmq as mq
import subprocess
import time
import json


RABBITMQ_HOST = os.getenv('RABBITMQ_ADDR')
RABBITMQ_CLIENTS_QUEUE = os.getenv('RABBITMQ_CLIENTS_QUEUE')
RABBITMQ_RESULTS_QUEUE = os.getenv('RABBITMQ_RESULTS_QUEUE')
RABBITMQ_RESULTS_PORT = os.getenv('RABBITMQ_RESULTS_PORT')

rabbit = mq.UDTRabbitMQ(RABBITMQ_HOST, RABBITMQ_RESULTS_PORT, RABBITMQ_CLIENTS_QUEUE, RABBITMQ_RESULTS_QUEUE)


def run_command(params):

    command_line = ''

    for i, x in enumerate(params):
        if i > 0:
            command_line = command_line + ' '
        command_line = command_line + x

    print(command_line)

    proc = subprocess.Popen(params, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    
    return proc.communicate()[0].decode("utf-8")



def onReceiveMessage(ch, method, properties, message):
    '''paho
    def onReceiveMessage(client, userdata, message):
        message = str(message.payload.decode("utf-8"))
    '''
    """
    ===================================================
    Message Received
    message.topic
    message.qos
    message.retain
    ===================================================
    """
    
    action = message.decode('UTF-8')
    print(" [x] Received %r" % action)

    if action == 'get_interface_data':
        results = run_command(['python3', 'get_interface_data.py'])
        results = "{'from':'get_interface_data', 'results':" + results + "}"
    else:
        [subcategoria, peso_ahorro, peso_productividad, peso_competitividad] = action.split(',')
        results = run_command(['python3', 'recommend.py',  '--subcat', str(subcategoria), '--pesos', str(peso_ahorro), str(peso_productividad), str(peso_competitividad), '--json'])
        results = "{'from':'get_recommendation', 'results':" + results + "}"


    """
    ===================================================
    Send Message
    ===================================================
    """
    results = results.replace('\\n', '').replace("'", '"')

    print(" [x] Sending %r" % results)
    rabbit.producer(results)


if __name__ == '__main__':
    try:
        while not rabbit.consumer(onReceiveMessage):
            print("Waiting RabbitMQ...")
            time.sleep(5)

    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
