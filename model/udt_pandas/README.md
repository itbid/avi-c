= UDT Pandas =

This library extends the Pandas' library.

== Requirements ==

Python 3
 
== Usage ==

Usage

df = pd2.read_csv('people.csv',
    [
        {'name': 'id', 'type': 'int'},
        {'name': 'consent', 'onlyTrue': True, 'remove': True},
        {'name': 'date of birth', 'type': 'sec', 'notnull': True}
    ])

In the previous example only three columns of the people.csv file are read.
The 'id' column is ensured to be an integer.
All records without 'consent' are removed.
The 'consent' column is used for the previous filtering and then it is removed.
The 'date of birdth' column is converted to 'sec' type.
All records without a 'date of birdth' are removed.
