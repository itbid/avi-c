#!/usr/bin/env python

import pandas as pd
from datetime import datetime
import numpy as np


# pd.set_option('display.max_columns', None)
pd.set_option('display.max_colwidth', None)
# pd.set_option('display.max_rows', None)
pd.set_option('display.expand_frame_repr', False)
pd.set_option('display.float_format', lambda x: '%.2f' % x)

def str2sec(df, column):
    """

    :param df:
    :param column:
    :return:
    """
    df[column] = pd.to_datetime(df[column]).apply(lambda timestamp: (timestamp.to_pydatetime() - datetime(1970, 1, 1)).total_seconds())


def sec2delta(df, column):
    """

    :param df:
    :param column:
    :return:
    """
    df[column] = pd.to_timedelta(df[column], unit='s').round('s')


def sec2datetime(df, column):
    """

    :param df:
    :param column:
    :return:
    """
    df[column] = pd.to_datetime(df[column], unit='s').round('s')


def rango2facturacion(string):
    """

    :param string:
    :return:
    """
    if string == '--':
        return 0
    elif string == '+20000':
        return 25000
    else:
        rango = string.split('-')
        return (float(rango[0]) + float(rango[1])) / 2


def normalize(df, exclude):
    """

    :param df:
    :param exclude:
    :return:
    """

    if str(df.name) not in exclude:
        max_value = df.max()
        min_value = df.min()
        sub_value = max_value - min_value
        return np.divide(np.subtract(df, min_value), sub_value)
    else:
        return df


def apply_restrictions(df, columns):
    """

    :param df
    :param columns
    :return:
    """

    index_columns = list()

    for c in columns:
        if 'notnull' in c and c['notnull']:
            df = df[df[c['name']].notnull()]

        if 'NaNTo0' in c and c['NaNTo0']:
            df[c['name']] = df[c['name']].fillna(0)

        if 'onlyTrue' in c and c['onlyTrue']:
            df = df[df[c['name']] == True]

        if 'notZero' in c and c['notZero']:
            df = df[df[c['name']] != 0]

        if 'remove' in c and c['remove']:
            df.drop(c['name'], axis=1, inplace=True)

        if 'type' in c:
            if c['type'] == 'sec':
                str2sec(df, c['name'])
            if c['type'] == 'delta':
                str2sec(df, c['name'])
            if c['type'] == 'datetime':
                str2sec(df, c['name'])
            if c['type'] == 'int':
                df = df.astype({c['name']: int})
            if c['type'] == 'float':
                df = df.astype({c['name']: float})
            if c['type'] == 'text' or c['type'] == 'string':
                df = df.astype({c['name']: object})

        if 'index' in c and c['index']:
            index_columns.append(c['name'])

    if len(index_columns) > 0:
        df.set_index(index_columns)
    
    return df


def read_csv(file_loc, columns):
    """

    :param file_loc:
    :param columns:
    :return:
    """

    df = pd.read_csv(file_loc, usecols=[c['name'] for c in columns])

    df = apply_restrictions(df, columns)

    return df


def read_postgres(sql, conn, columns):
    """
    
    :param sql
    :param conn
    :param columns:
    :return:
    """

    df = pd.read_sql_query(sql, conn)
    df = apply_restrictions(df, columns)

    return df
