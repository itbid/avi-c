= UDT RabbitMQ =

This library helps working with RabbitMQ.

== Requirements ==

Python 3
pika

```
pip3 install python3-pika
```

== Usage ==

Usage

Open a channel for listening and a channel for sending messages:

```
import udt_rabbitmq as mq

rabbit = mq.UDTRabbitMQ(RABBITMQ_HOST, RABBITMQ_CLIENTS_QUEUE, RABBITMQ_RESULTS_QUEUE)
```

This function will be called when a message arrives:

```
def onReceiveMessage(ch, method, properties, body):
    # The recibed message is in "body"

    # Send a message with:
    rabbit.producer("message")
```

Call this function to start listening for messages:

```
rabbit.consumer(onReceiveMessage)
```
