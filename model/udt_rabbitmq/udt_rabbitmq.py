#!/usr/bin/env python

# https://www.rabbitmq.com/tutorials/tutorial-one-python.html
import pika

'''paho
import paho.mqtt.client as mqtt
'''


'''paho
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("clients")
'''


class UDTRabbitMQ:
    def __init__(self, host, port, input_queue, output_queue):
        self.host = host
        self.port = int(port)
        self.input_queue = input_queue
        self.output_queue = output_queue

        '''paho
        # Create new client instance
        self.client = mqtt.Client()
        self.client.on_connect = on_connect
        '''

    def producer(self, message):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.host))
        channel = connection.channel()
        channel.queue_declare(queue=self.output_queue, durable=True)
        channel.confirm_delivery()

        try:
            channel.basic_publish(exchange='', routing_key=self.output_queue, body=str(message))
            print("Message sent to RabbitMQ (" + str(len(str(message))) + " bytes)")
        except pika.exceptions.UnroutableError:
            print('Message was returned.')

        connection.close()
        
        '''paho
        # Connect to broker
        self.client.connect(self.host, port=self.port)

        # Publish message
        self.client.publish(self.output_queue, message)

        print("*** message sent: " + message)
        '''


    def consumer(self, callback):
        ret = True
        # print("RabbitMQ url:" + self.host)

        try:
            connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.host))
            channel = connection.channel()
            channel.queue_declare(queue=self.input_queue, durable=True)
            channel.basic_consume(queue=self.input_queue, on_message_callback=callback, auto_ack=True)

            print('Connected to RabbitMQ.')
            print('To exit press CTRL+C...')
            channel.start_consuming()
        except:
            ret = False

        '''paho
        # Subscribe to topic
        self.client.on_message = callback

        # Connect to broker
        self.client.connect(self.host, port=self.port)

        self.client.loop_forever()
        '''

        return ret


        