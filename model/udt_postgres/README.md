= UDT RabbitMQ =

This library helps working with PostgreSQL databases.

== Requirements ==

Python 3
psycopg2

```
pip3 install psycopg2-binary
```

== Usage ==

Usage

Configure the database connection:

```
import psycopg2 
db = postgres.UDTPostgres(POSTGRESQL_DB, POSTGRESQL_USER, POSTGRESQL_PASS, POSTGRESQL_HOST, POSTGRESQL_PORT)
```

Get data from a table:

```
table_data = db.get_table_data('public.client', 'id')
```
