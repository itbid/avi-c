#!/usr/bin/env python

import psycopg2  # pip3 install psycopg2-binary
import sys
sys.path.append('../model/pandas')
import udt_pandas as pd2


class UDTPostgres:
    def __init__(self, database, user, password, host, port):
        self.database = database
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        # DEBUG:
        # print("DB:" + self.database + " USER:" + self.user + " PASS:" + self.password + " HOST:" + self.host + " PORT:" + str(self.port))

    def connect(self):
        return psycopg2.connect("host='{}' port={} dbname='{}' user={} password={}".format(self.host, self.port, self.database, self.user, self.password))

    def get_table_data(self, table_name, order_by):
       
        conn = self.connect()

        # Setting auto commit
        # conn.autocommit = True

        # Creating a cursor object using the cursor() method
        cursor = conn.cursor()

        # Retrieving data
        cursor.execute('select * from ' + table_name + ' order by ' + order_by)

        # Fetching all rows from the table
        results = cursor.fetchall()

        # Commit your changes in the database
        # conn.commit()

        # Closing the connection
        conn.close()
        conn = None

        return results


    def get_pandas_dataframe(self, table_name, use_cols):

        conn = self.connect()

        # Setting auto commit
        # conn.autocommit = True

        # Creating a cursor object using the cursor() method
        cursor = conn.cursor()

        # Retrieving data

        col_names = ''
        for i, col in enumerate(use_cols):
            if i > 0:
                col_names = col_names + ', '
            col_names = col_names + col['name']

        sql = 'select ' + col_names + ' from ' + table_name

        result = pd2.read_postgres(sql, conn, use_cols)

        # Closing the connection
        conn.close()
        conn = None

        return result
