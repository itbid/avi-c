#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""recommend.py: Este fichero contiene el algoritmo de recomendación. Desde la 
interface gráfica recibe una subcategoría y los pesos de los KPI y devuelve
el ranking de las mejores negociaciones ordenadas por confianza.
"""

__author__ = "IIIA-CSIC"
__license__ = ""
__version__ = "1.0.1"

import matplotlib.pyplot as plt
from scipy import stats
from tabulate import tabulate
import argparse as ap
import pandas as pd
import numpy as np
import csv
import os
import udt_pandas as pd2
import udt_postgres as postgres
import warnings

# For getting a clean output without warnings:
warnings.filterwarnings('ignore')

'''
Estas constantes se obtienen del sistema.
Las pone docker compose desde el fichero docker-compose.yml
'''
POSTGRESQL_HOST = os.getenv('DATABASE_ADDR')
POSTGRESQL_USER = os.getenv('POSTGRESQL_USER')
POSTGRESQL_PASS = os.getenv('POSTGRESQL_PASS')
POSTGRESQL_DB = os.getenv('POSTGRESQL_DB')  # + client_id
POSTGRESQL_PORT = os.getenv('POSTGRESQL_PORT')


def mercado(subasta, df_pro):
    '''
    Esta función recibe una subasta y devuelve sus proveedores.
    '''
    t = int(subasta['subsyscreado'])
    return df_pro[df_pro['profecha'] <= t].drop('profecha', axis=1).set_index('id_proveedor')


def mercado_base(subcategoria, df_pro, df_cat):
    '''
    Esta función devuelve los proveedores de una subcategoría.
    '''
    df_sub = df_cat[df_cat['id_subcategoria'] == subcategoria].drop('id_subcategoria', axis=1)
    df_pro_base = pd.merge(df_sub, df_pro, on='id_proveedor').drop('profecha', axis=1).set_index('id_proveedor')
    return df_pro_base


def representante(mercado):
    '''
    Esta función recibe el mercado (conjunto de proveedores) y devuelve el representante (el centroid o centro de masas).
    '''
    return mercado.mean()


def distancia(rep1, rep2, p=2):
    '''
    Esta función recibe dos representantes y devuelve la distancia entre ellos.
    '''
    return np.linalg.norm(rep1.values - rep2.values, ord=p)


def kpi(sub, weights):
    '''
    Esta función obtiene el kpi como la media entre el ahorro, la productividad y la competitividad.
    Utiliza el array weights de tres elementos (del 0 al 1) para dar pesos al ahorro, la productividad y la competitividad.
    '''
    return 100 * np.average(sub[['Ahorro', 'Productividad', 'Competitividad']].values, weights=weights)


def segundos_a_dias(segundos):
    '''
    Esta función convierte un número de segundos a su equivalente en días.
    '''
    return segundos / 60 / 60 / 24


def main():
    # ==================================================================================================================
    # Parser
    # ==================================================================================================================
    parser = ap.ArgumentParser()

    parser.add_argument('--cliente', type=str, default='157')  # 157 is Carglass
    parser.add_argument('--provedores', type=str, default='proveedores')  # proveedores.csv
    parser.add_argument('--subastas', type=str, default='subastas')  # subastas.csv
    parser.add_argument('--variantes', type=str, default='variantes')  # variantes.csv
    parser.add_argument('--ofertas', type=str, default='hofertas')  # hofertas.csv
    parser.add_argument('--relsubpro', type=str, default='relsubpro')  # relsubpro.csv
    parser.add_argument('--categorias', type=str, default='cat_proveedores')  # cat_proveedores.csv
    parser.add_argument('--ofecriterios', type=str, default='ofecriterios')  # ofecriterios.csv
    parser.add_argument('--criterios', type=str, default='criterios')  # criterios.csv
    parser.add_argument('--plot', action='store_true')
    parser.add_argument('--top', type=int, default=3)
    parser.add_argument('--export-csv', type=str)
    parser.add_argument('--data', type=str, default='data')
    parser.add_argument('--json', action='store_true')
    parser.add_argument('--url', type=str, default='testavi3')
    parser.add_argument('--pesos', nargs=3, type=float, metavar=('AHORRO', 'PROD', 'COMPET'), default=[1.0, 1.0, 1.0])
    mand = parser.add_argument_group(title='required arguments (mutually exclusive)')
    mutex = mand.add_mutually_exclusive_group(required=True)
    mutex.add_argument('--export-idx', type=str)
    mutex.add_argument('--subcat', type=int)

    args = parser.parse_args()

    # ==================================================================================================================
    # Lectura de los datos de entrada + Limpieza previa de los mismos
    # ==================================================================================================================

    db = postgres.UDTPostgres(POSTGRESQL_DB + str(args.cliente), POSTGRESQL_USER, POSTGRESQL_PASS, POSTGRESQL_HOST, POSTGRESQL_PORT)

    # Cargar la tabla de proveedores
    # Eliminamos los registros de proveedores que no tienen "profacturacion" o que no tienen "distance".
    df_pro = db.get_pandas_dataframe(args.provedores, [
        {'name': 'id_proveedor', 'index': True},
        {'name': 'profecha', 'type': 'sec'},
        {'name': 'profacturacion', 'type': 'text', 'notnull': True},
        {'name': 'distance', 'type': 'float', 'notnull': True}])

    # Cargar la tabla de subastas
    # Eliminamos los registros que no tienen "id_categoria"
    df_sub = db.get_pandas_dataframe(args.subastas, [
        {'name': 'id_subasta'},
        {'name': 'id_subcategoria', 'type': 'int', 'notnull': True},
        {'name': 'subtipo', 'type': 'int'},
        {'name': 'subsyscreado', 'type': 'sec'},
        {'name': 'subsyslanzado', 'type': 'sec', 'notnull': True}])

    # Cargar la tabla de variantes
    # Eliminamos los registros que no tienen "vriadjudicado".
    # Después de esto, esta columna ya no nos sirve y la eliminamos.
    # Eliminamos los registros que no tienen "baseline" o que el "importe" es cero.
    df_var = db.get_pandas_dataframe(args.variantes, [
        {'name': 'id_subasta'},
        {'name': 'id_variante'},
        {'name': 'vriadjudicado', 'onlyTrue': True, 'remove': True},
        {'name': 'baseline', 'notnull': True},
        {'name': 'importe', 'notZero': True},
        {'name': 'vriadjudicadofecha', 'type': 'sec'}])

    # Cargar la tabla de ofertas
    # Eliminamos los registros que no tienen "id_variante"
    df_ofe = db.get_pandas_dataframe(args.ofertas, [
        {'name': 'id_subasta'},
        {'name': 'id_variante', 'type': 'int', 'notnull': True},
        {'name': 'id_oferta'},
        {'name': 'ofefecha', 'type': 'sec'},
        {'name': 'id_proveedor'}])

    # Cargamos la tabla que relaciona subastas con productos
    df_relsubpro = db.get_pandas_dataframe(args.relsubpro, [
        {'name': 'id_subasta'},
        {'name': 'id_proveedor'}])

    # Cargar la tabla de categorías
    # Eliminamos los registros en los que "asignada" es False y borramos la columna.
    df_cat = db.get_pandas_dataframe(args.categorias, [
        {'name': 'id_subcategoria'},
        {'name': 'id_proveedor'},
        {'name': 'asignada', 'onlyTrue': True, 'remove': True}])

    # Cargamos la tabla que relaciona ofertas con criterios
    df_ofecri = db.get_pandas_dataframe(args.ofecriterios, [
        {'name': 'id_oferta'},
        {'name': 'id_criterio'},
        {'name': 'ofecri'},
        {'name': 'id_subasta'}])

    # Cargamos la tabla de criterios
    df_cri = db.get_pandas_dataframe(args.criterios, [
        {'name': 'id_criterio', 'index': True},
        {'name': 'critipo'},
        {'name': 'precio_referencia'},
        {'name': 'cridesc'},
        {'name': 'es_moneda'},
        {'name': 'peso', 'NaNTo0': True}])


    # ==================================================================================================================
    # Tratamiento de los datos
    # ==================================================================================================================

    # Obtenemos los criterios de cada subasta
    df_criterios_subasta = pd.merge(df_ofecri, df_cri, on='id_criterio')
    df_criterios_subasta = df_criterios_subasta[df_criterios_subasta['critipo'] == 14]
    # Quito las columnas que he necesitado para obtener los criterios de cada subasta
    df_cri.drop('cridesc', axis=1, inplace=True)
    df_cri.drop('es_moneda', axis=1, inplace=True)
    df_cri.drop('peso', axis=1, inplace=True)
    df_ofecri.drop('id_subasta', axis=1, inplace=True)
    # Quito las columnas que no necesito para saber qué criterios tienen cada subasta:
    df_criterios_subasta.drop('id_oferta', axis=1, inplace=True)
    df_criterios_subasta.drop('ofecri', axis=1, inplace=True)
    df_criterios_subasta.drop('critipo', axis=1, inplace=True)
    df_criterios_subasta.drop('precio_referencia', axis=1, inplace=True)
    # Quito repeticiones en los criterios para que quede solamente uno de cada:
    df_criterios_subasta = df_criterios_subasta.drop_duplicates()

    # Variante con ahorro maximo
    df_var['ahorro'] = df_var['baseline'] - df_var['importe']
    df_var = df_var.sort_values('ahorro', ascending=False).drop_duplicates(['id_subasta'])

    # Borrar ofertas con tiempo < de inicio o > de adjudicación <---- TO DO: Update comment
    df_ofe = pd.merge(df_ofe, df_sub, on='id_subasta')
    df_ofe = pd.merge(df_ofe, df_var, on=['id_subasta'])
    df_ofe = df_ofe[df_ofe['ofefecha'] > df_ofe['subsyscreado']]

    # Provedores invitados para cada subasta
    df_relsubpro_count = df_relsubpro.groupby(['id_subasta']).size().reset_index(name='invitados')

    # Provedores activos para cada subasta
    df_relsubpro_activos_count = df_ofe.groupby(['id_subasta'])['id_proveedor'].nunique().reset_index(name='activos')

    # Hofertas para cada subasta
    df_ofe_count = df_ofe.groupby(['id_subasta']).size().reset_index(name='hofertas')

    # Primera hoferta para cada subasta
    df_ofe_min = df_ofe[['id_subasta', 'ofefecha']].groupby(['id_subasta'], as_index=False).min().rename(
        columns={'ofefecha': 'min_ofefecha'})

    # Provedores para cada subcategoria
    df_relsubpro_cat_count = df_cat.groupby(['id_subcategoria']).size().reset_index(name='aceptados')

    # Hofertas con criterios de precio (by Albert)
    df_cri.drop('precio_referencia', axis=1, inplace=True)
    df_cri = df_cri[df_cri['critipo'] == 14]
    df_cri.drop('critipo', axis=1, inplace=True)
    df_ofecri = pd.merge(df_ofecri, df_cri, on='id_criterio')

    df_ofecri.drop('id_criterio', axis=1, inplace=True)
    df_ofecri = df_ofecri.groupby(['id_oferta']).sum()
    df_ofe_precio = pd.merge(df_ofe, df_ofecri, on='id_oferta')

    # Desviacion para cada subasta
    df_ofe_precio_groupby = df_ofe_precio[['id_subasta', 'ofecri']].groupby(['id_subasta'], as_index=False)
    std_ofe_precio = df_ofe_precio_groupby.std(ddof=0).rename(columns={'ofecri': 'Desviacion Ofertas'})

    # Agrupar columnas
    df = pd.merge(df_sub, df_var, on='id_subasta')
    for d in [df_relsubpro_count, df_ofe_count, df_ofe_min, std_ofe_precio, df_relsubpro_activos_count]:
        df = pd.merge(df, d, on='id_subasta')
    for d in [df_relsubpro_cat_count]:
        df = pd.merge(df, d, on='id_subcategoria')

    # ==================================================================================================================
    # Cálculo de los KPIs secundarios
    # ==================================================================================================================

    df['Duraccion'] = df['vriadjudicadofecha'] - df['subsyscreado']

    # KPIs de ahorro:
    df['Ahorro / Baseline (%)'] = 100 * df['ahorro'] / df['baseline']

    #  KPIs de competitividad:
    df['1 - Primera Hoferta / Duraccion (%)'] = 100 * (1 - (df['min_ofefecha'] - df['subsyscreado']) / df['Duraccion'])
    df['Invitados / Aceptados (%)'] = 100 * df['invitados'] / df['aceptados']
    df['Activos / Invitados (%)'] = 100 * df['activos'] / df['invitados']
    df['Hofertas / Activos'] = df['hofertas'] / df['activos']
    df['Similitud Ofertas'] = -df['Desviacion Ofertas']

    #  KPIs de productividad:
    df['1 - Tempo Lanzamiento / Duraccion (%)'] = 100 * (
                1 - (df['subsyslanzado'] - df['subsyscreado']) / df['Duraccion'])
    df['Ahorro / Dia (€)'] = df['ahorro'] * 24 * 3600 / (df['Duraccion'])

    # Borrar negociaciones con ahorro muy negativo
    df = df[df['Ahorro / Baseline (%)'] > -100]

    # Quitamos las columnas de "df" que no necesitamos
    for c in ['id_variante', 'ahorro', 'hofertas', 'vriadjudicadofecha',
              'min_ofefecha', 'aceptados', 'Desviacion Ofertas', 'activos']:
        df.drop(c, axis=1, inplace=True)

    kpi_ahorro = ['Ahorro / Baseline (%)']
    kpi_productividad = ['Ahorro / Dia (€)', '1 - Tempo Lanzamiento / Duraccion (%)']
    kpi_competitividad = ['1 - Primera Hoferta / Duraccion (%)', 'Similitud Ofertas', 'Invitados / Aceptados (%)',
                          'Activos / Invitados (%)', 'Hofertas / Activos']

    columns = []
    for col in [kpi_ahorro, kpi_productividad, kpi_competitividad]:
        columns.extend(col)

    # ==================================================================================================================
    # Eliminación de los outliers
    # ==================================================================================================================

    # Borrar outliers (https://www.kite.com/python/answers/how-to-remove-outliers-from-a-pandas-dataframe-in-python)
    z_scores = stats.zscore(df)
    abs_z_scores = np.abs(z_scores)
    filtered_entries = (abs_z_scores < 3).all(axis=1)
    df = df[filtered_entries]

    # ==================================================================================================================
    # Exportación de los IDs (funcionalidad necesaria para los tests de validación)
    # ==================================================================================================================

    if args.export_idx:
        df.to_csv(args.export_idx, columns=['id_subasta'], index=False, header=False)
        quit()

    # ==================================================================================================================
    # Normalización de los datos
    # ==================================================================================================================

    exclude = ['id_subasta', 'id_subcategoria', 'subtipo', 'Duraccion', 'Negociaciones', 'Ultima Negociacion',
               'subsyscreado', 'subsyslanzado', 'Duracion', 'invitados', 'baseline']
    subastas = df.apply(lambda x: pd2.normalize(x, exclude)).set_index(['id_subasta']).sort_index()

    # ==================================================================================================================
    # Agrupación de los KPIs secundarios en los KPIs principales (Ahorro, Productividad y Competitividad)
    # ==================================================================================================================

    subastas['Ahorro'] = 0
    for c in kpi_ahorro:
        subastas['Ahorro'] = subastas['Ahorro'] + subastas[c]
        subastas.drop(c, axis=1, inplace=True)
    subastas['Ahorro'] = subastas['Ahorro'] / len(kpi_ahorro)

    subastas['Productividad'] = 0
    for c in kpi_productividad:
        subastas['Productividad'] = subastas['Productividad'] + subastas[c]
        subastas.drop(c, axis=1, inplace=True)
    subastas['Productividad'] = subastas['Productividad'] / len(kpi_productividad)

    subastas['Competitividad'] = 0
    for c in kpi_competitividad:
        subastas['Competitividad'] = subastas['Competitividad'] + subastas[c]
        subastas.drop(c, axis=1, inplace=True)
    subastas['Competitividad'] = subastas['Competitividad'] / len(kpi_competitividad)

    # Preprocesar facturacion
    df_pro['profacturacion'] = df_pro['profacturacion'].apply(lambda x: pd2.rango2facturacion(x))

    # ==================================================================================================================
    # Para cada subasta, calcular su KPI final (función entre Ahorro, Productividad y Competitividad) y
    # la distancia entre la subasta y la base.
    # ==================================================================================================================

    output = dict()
    output['labels'] = list()
    output['k'] = list()
    output['d'] = list()
    output['rank'] = dict()
    output['num_negociaciones'] = len(subastas)
    output['num_proveedores'] = len(df_pro)

    # Se obtiene el representante del mercado base
    rb = representante(mercado_base(args.subcat, df_pro, df_cat))

    for idx, subasta in subastas.iterrows():
        output['labels'].append(idx)
        k_val = kpi(subasta, args.pesos)

        # Se obtiene el representante de la negociación
        rs = representante(mercado(subasta, df_pro))

        # Se obtiene la distancia entre los dos representantes
        d_val = distancia(rb, rs)

        output['k'].append(k_val)
        output['d'].append(d_val)
        output['rank'][idx] = {
            'd': d_val,
            'k': k_val,
        }

    output['k'] = np.array(output['k'])
    output['d'] = np.array(output['d'])
    d_max = output['d'].max()
    output['d'] = 100 * np.divide(output['d'], d_max)

    # Se ordena el ranking de forma jerárquica: primero por la distancia y si las distancias coinciden entonces por el kpi
    output['rank'] = sorted(output['rank'].items(), key=lambda x: (x[1]['d'], -x[1]['k']))

    # ==================================================================================================================
    # Mostrar outputs
    # ==================================================================================================================

    if args.plot:
        fig, ax = plt.subplots()
        colors = ['r', '#ff8000', '#ffff00', '#80ff00', 'g']
        for i, c in enumerate(colors):
            ax.add_patch(plt.Circle((100, 0), 100 - 20 * i, color=c))
        ax.plot(output['k'], output['d'], '.', color='k')
        ax.set_xlabel('KPI score (%)')
        ax.set_xlim([0, 100])
        ax.set_ylabel('Distance (%)')
        ax.set_ylim([0, 100])
        for i, idx in enumerate(output['labels']):
            plt.annotate(idx, (output['k'][i], output['d'][i]))
        plt.show()


    # ==================================================================================================================
    # Exportar la salida a formato CSV
    # ==================================================================================================================

    if args.export_csv:
        with open(args.export_csv, 'w') as f:
            write = csv.writer(f)
            headers.insert(0, '')
            headers.insert(-1, '')
            write.writerow(headers)
            for row in table:
                row.insert(0, '')
                row.insert(-1, '')
                write.writerow(row)
            write.writerow([])

    if not args.json:
        print('Top {} recomendaciones'.format(args.top))


    nego_types = {
        1: 'RFQ',
        3: 'RFP',
        4: 'e-Tender MRO',
        5: 'Sealed Bid Auction',
        8: 'Dutch Auction',
    }

    headers = ['#', 'ID', 'Tipo', 'Subcategoria', 'KPI (%)', 'Ahorro (%)', 'Productividad (%)', 'Competitividad (%)', 'Distancia', 'URL']
    table = []
    k = 1
    for idx, dk in output['rank'][:args.top]:
        table.append([
            k,
            idx,
            nego_types[int(subastas.loc[idx]['subtipo'])],
            int(subastas.loc[idx]['id_subcategoria']),
            dk['k'],
            100 * subastas.loc[idx]['Ahorro'],
            100 * subastas.loc[idx]['Productividad'],
            100 * subastas.loc[idx]['Competitividad'],
            100 * dk['d'] / d_max,
            'https://{}.itbid.org/negociaciones/ver/{}'.format(args.url, idx)
        ])
        k += 1

    if not args.json:
        print(tabulate(table, headers, tablefmt='rst', stralign='left', numalign='right', floatfmt='.3f'))

    if args.export_csv:
        with open(args.export_csv, 'a') as f:
            write = csv.writer(f)
            write.writerow(headers)
            write.writerows(table)


    # ==================================================================================================================
    # Mostrar salida en formato JSON.
    # Se usa principalmente para devolver los resultados en un formato estructurado y que otra aplicación pueda
    # tratarlos (model.py) 
    # ==================================================================================================================

    if args.json:
        to_interface = dict()
        to_interface['recomendaciones'] = list()

        for idx, dk in output['rank'][:args.top]:

            criterios_economicos = list()
            criterios_tecnicos = list()

            for _, row in df_criterios_subasta.iterrows():
                if row['id_subasta'] == idx:
                    if row['es_moneda']:
                        criterios_economicos.append({
                            'nombre': row['cridesc'],
                            'peso': str(row['peso'])
                        })
                    else:
                        criterios_tecnicos.append({
                            'nombre': row['cridesc'],
                            'peso': str(row['peso'])
                        })
                                        
            to_interface['recomendaciones'].append({
                'id': idx,
                'tipo': nego_types[int(subastas.loc[idx]['subtipo'])],
                'duracion': round(segundos_a_dias(subastas.loc[idx]['Duraccion']), 2),
                'tiempo_lanzamiento': round(segundos_a_dias(subastas.loc[idx]['subsyslanzado'] - subastas.loc[idx]['subsyscreado']), 2),
                'proveedores_participantes': round(subastas.loc[idx]['invitados'], 2),
                'criterios_economicos': criterios_economicos,
                'criterios_tecnicos': criterios_tecnicos,
                'baseline': round(subastas.loc[idx]['baseline'], 2),
                'kpi': round(dk['k'], 2),
                'ahorro': round(100 * subastas.loc[idx]['Ahorro'], 2),
                'confianza': round(100 - (100 * dk['d'] / d_max), 2),
                'competitividad': round(100 * subastas.loc[idx]['Competitividad'], 2),
                'productividad': round(100 * subastas.loc[idx]['Productividad'], 2),
                'url': 'https://{}.itbid.org/negociaciones/ver/{}'.format(args.url, idx)
            })

        to_interface['total_proveedores'] = len(df_pro)
        print(to_interface)



if __name__ == "__main__":
    main()
