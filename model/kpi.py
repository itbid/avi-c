import matplotlib.pyplot as plt
from datetime import datetime
from scipy import stats
import argparse as ap
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from yellowbrick.cluster import KElbowVisualizer
from yellowbrick.cluster.elbow import kelbow_visualizer
import seaborn as sns
import os

pd.set_option('max_columns', None)
pd.set_option('max_colwidth', None)
pd.set_option('max_rows', None)
pd.set_option('expand_frame_repr', False)
pd.set_option('display.float_format', lambda x: '%.2f' % x)

def str2sec(df, column):
    df[column] = pd.to_datetime(df[column]).apply(lambda timestamp: (timestamp.to_pydatetime() - datetime(1970, 1, 1)).total_seconds())

def sec2delta(df, column):
    df[column] = pd.to_timedelta(df[column], unit='s').round('s')

def sec2datetime(df, column):
    df[column] = pd.to_datetime(df[column], unit='s').round('s')

parser = ap.ArgumentParser()
parser.add_argument('--cliente', type=str, default='cajamar_217')
parser.add_argument('--subastas', type=str, default='subastas.csv')
parser.add_argument('--variantes', type=str, default='variantes.csv')
parser.add_argument('--ofertas', type=str, default='hofertas.csv')
parser.add_argument('--provedores', type=str, default='relsubpro.csv')
parser.add_argument('--categorias', type=str, default='cat_proveedores.csv')
parser.add_argument('--ofecriterios', type=str, default='ofecriterios.csv')
parser.add_argument('--criterios', type=str, default='criterios.csv')
parser.add_argument('--see', type=int)
parser.add_argument('--clusters', type=int)
parser.add_argument('--compradores', action='store_true')
parser.add_argument('--datos', action='store_true')
parser.add_argument('--silhouette', action='store_true')
parser.add_argument('--ahorro', action='store_true')
args = parser.parse_args()

# Leer tablas
df_sub = pd.read_csv(os.path.join(args.cliente, args.subastas), usecols=['id_subasta', 'id_subcategoria', 'subsyscreado', 'subsyslanzado', 'id_comprador'])
str2sec(df_sub, 'subsyscreado')
df_var = pd.read_csv(os.path.join(args.cliente, args.variantes), usecols=['id_subasta', 'id_variante', 'vriadjudicado', 'baseline', 'importe', 'vriadjudicadofecha'])
str2sec(df_var, 'vriadjudicadofecha')
df_ofe = pd.read_csv(os.path.join(args.cliente, args.ofertas), usecols=['id_subasta', 'id_variante', 'id_oferta', 'ofefecha', 'id_proveedor'])
str2sec(df_ofe, 'ofefecha')
df_pro = pd.read_csv(os.path.join(args.cliente, args.provedores), usecols=['id_subasta', 'id_proveedor'])
df_cat = pd.read_csv(os.path.join(args.cliente, args.categorias), usecols=['id_subcategoria', 'id_proveedor', 'asignada'])
df_ofecri = pd.read_csv(os.path.join(args.cliente, args.ofecriterios), usecols=['id_oferta', 'id_criterio', 'ofecri'])
df_cri = pd.read_csv(os.path.join(args.cliente, args.criterios), usecols=['id_criterio', 'critipo', 'precio_referencia'])

# Borrar negociaciones sin subcategoria 
df_sub = df_sub[df_sub['id_subcategoria'].notnull()]
df_sub = df_sub.astype({'id_subcategoria': int})

# Borrar hofertas sin variante
df_ofe = df_ofe[df_ofe['id_variante'].notnull()]
df_ofe = df_ofe.astype({'id_variante': int})

# Solo variantes adjudicadas
df_var = df_var[df_var['vriadjudicado'] == True]
df_var.drop('vriadjudicado', axis=1, inplace=True)

# Solo provedores aceptados
df_cat = df_cat[df_cat['asignada'] == True]
df_cat.drop('asignada', axis=1, inplace=True)

# Borrar variantes sin baseline o con importe == 0
#df_var = df_var[df_var['baseline'].notnull()]
df_var = df_var[df_var['importe'] != 0]

# Borrar ofertas con tiempo < de inicio o > de adjudicación
df_ofe = pd.merge(df_ofe, df_sub, on='id_subasta')
df_ofe = pd.merge(df_ofe, df_var, on=['id_subasta'])
df_ofe = df_ofe[df_ofe['ofefecha'] > df_ofe['subsyscreado']]
#df_ofe = df_ofe[df_ofe['ofefecha'] < df_ofe['vriadjudicadofecha']]

# Borrar subastas sin fecha de lanzamiento
df_sub = df_sub[df_sub['subsyslanzado'].notnull()]
str2sec(df_sub, 'subsyslanzado')

# Provedores invitados para cada subasta
df_pro_count = df_pro.groupby(['id_subasta']).size().reset_index(name='invitados')

# Provedores activos para cada subasta
df_pro_activos_count = df_ofe.groupby(['id_subasta'])['id_proveedor'].nunique().reset_index(name='activos')

# Hofertas para cada subasta
df_ofe_count = df_ofe.groupby(['id_subasta']).size().reset_index(name='hofertas')

# Primera hoferta para cada subasta
df_ofe_min = df_ofe[['id_subasta', 'ofefecha']].groupby(['id_subasta'], as_index=False).min().rename(columns={'ofefecha':'min_ofefecha'})

# Provedores para cada subcategoria
df_pro_cat_count = df_cat.groupby(['id_subcategoria']).size().reset_index(name='aceptados')

# Ultima negociacion para cada comprador
df_last_neg = df_sub[['id_comprador', 'subsyscreado']].groupby(['id_comprador']).max().rename(columns={'subsyscreado':'Ultima Negociacion'})
sec2datetime(df_last_neg, 'Ultima Negociacion') 

# Hofertas con criterios de precio (by Albert)
#df_cri = df_cri[df_cri['precio_referencia'] == True]
df_cri.drop('precio_referencia', axis=1, inplace=True)
df_cri = df_cri[df_cri['critipo'] == 14]
df_cri.drop('critipo', axis=1, inplace=True)
df_ofecri = pd.merge(df_ofecri, df_cri, on='id_criterio')
df_ofecri.drop('id_criterio', axis=1, inplace=True)
df_ofecri = df_ofecri.groupby(['id_oferta']).sum()
df_ofe_precio = pd.merge(df_ofe, df_ofecri, on='id_oferta')

# Promedio y desviacion para cada subasta
df_ofe_precio_groupby = df_ofe_precio[['id_subasta', 'ofecri']].groupby(['id_subasta'], as_index=False)
avg_ofe_precio = df_ofe_precio_groupby.mean().rename(columns={'ofecri':'Promedio Ofertas'})
std_ofe_precio = df_ofe_precio_groupby.std(ddof=0).rename(columns={'ofecri':'Desviacion Ofertas'})

# Agrupar columnas
df = pd.merge(df_sub, df_var, on='id_subasta')
for d in [df_pro_count, df_ofe_count, df_ofe_min, std_ofe_precio, df_pro_activos_count]:
    df = pd.merge(df, d, on='id_subasta')
for d in [df_pro_cat_count]:
    df = pd.merge(df, d, on='id_subcategoria')

# Calcular KPIs
df['Ahorro (€)'] = df['baseline'] - df['importe']
df['Ahorro / Baseline (%)'] = 100 * df['Ahorro (€)'] / df['baseline']
df['Duraccion'] = df['vriadjudicadofecha'] - df['subsyscreado']
df['Ahorro / Dia (€)'] = df['Ahorro (€)'] * 24 * 3600 / (df['Duraccion'])
df['1 - Primera Hoferta / Duraccion (%)'] = 100 * (1 - (df['min_ofefecha'] - df['subsyscreado']) / df['Duraccion'])
df['Invitados / Aceptados (%)'] = 100 * df['invitados'] / df['aceptados']
df['Activos / Invitados (%)'] = 100 * df['activos'] / df['invitados']
df['Hofertas / Activos'] = df['hofertas'] / df['activos']
df['1 - Tempo Lanzamiento / Duraccion (%)'] = 100 * (1 - (df['subsyslanzado'] - df['subsyscreado']) / df['Duraccion'])
df['Similitud Ofertas'] = -df['Desviacion Ofertas']

# Borrar negociaciones con ahorro muy negativo
df = df[df['Ahorro / Baseline (%)'] > -100]

if args.ahorro:
    count, bins = np.histogram(df['Ahorro / Baseline (%)'].values, bins=np.linspace(-100, 100, num=21))
    ax = sns.barplot(x=['[{}-{}]'.format(bins[i], bins[i + 1]) for i in range(len(bins) - 1)], y=count)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
    plt.show()

n_neg = len(df)
n_com = len(np.unique(df['id_comprador'].values))

for c in ['subsyscreado', 'subsyslanzado', 'id_variante', 'vriadjudicadofecha', 'baseline', 'Duraccion',
          'importe', 'invitados', 'hofertas', 'min_ofefecha', 'aceptados', 'Desviacion Ofertas', 'Ahorro (€)', 'activos']:
    df.drop(c, axis=1, inplace=True)

if args.see:
    print(df[df['id_comprador'] == args.see].drop('id_comprador', axis=1).set_index(['id_subasta']))

kpi_ahorro = ['Ahorro / Baseline (%)']
kpi_productividad = ['Ahorro / Dia (€)', '1 - Tempo Lanzamiento / Duraccion (%)']
kpi_competitividad = ['1 - Primera Hoferta / Duraccion (%)', 'Similitud Ofertas', 'Invitados / Aceptados (%)', 'Activos / Invitados (%)', 'Hofertas / Activos']

columns = [ 'id_comprador']
for col in [kpi_ahorro, kpi_productividad, kpi_competitividad]:
    columns.extend(col)

if args.compradores:
    group_by = df[columns].groupby(['id_comprador'])
    neg_count = group_by.size().reset_index(name='Negociaciones')
    median = group_by.median()
else:
    median = df
    median.set_index(['id_subasta'], inplace=True)

if args.datos:
    print(median.sort_values(by=['Ahorro / Baseline (%)']))
    print()
    print('{} negociaciones'.format(n_neg))
    print('{} compradores'.format(n_com))
    quit()

# Borrar outliers (https://www.kite.com/python/answers/how-to-remove-outliers-from-a-pandas-dataframe-in-python)
z_scores = stats.zscore(median)
abs_z_scores = np.abs(z_scores)
filtered_entries = (abs_z_scores < 3).all(axis=1)
median = median[filtered_entries]

def normalize(df_col):
    exclude = ['id_subasta', 'id_comprador', 'id_subcategoria', 'Duraccion', 'Negociaciones', 'Ultima Negociacion']
    if str(df_col.name) not in exclude:
        max_value = df_col.max()
        min_value = df_col.min()
        sub_value = max_value - min_value
        return np.divide(np.subtract(df_col, min_value), sub_value)
    else:
        return df_col
ranking = median.apply(lambda x: normalize(x))

ranking['Ahorro'] = 0
for c in kpi_ahorro:
    ranking['Ahorro'] = ranking['Ahorro'] + ranking[c]
    ranking.drop(c, axis=1, inplace=True)
ranking['Ahorro'] = ranking['Ahorro'] / len(kpi_ahorro)

ranking['Productividad'] = 0
for c in kpi_productividad:
    ranking['Productividad'] = ranking['Productividad'] + ranking[c]
    ranking.drop(c, axis=1, inplace=True)
ranking['Productividad'] = ranking['Productividad'] / len(kpi_productividad)

ranking['Competitividad'] = 0
for c in kpi_competitividad:
    ranking['Competitividad'] = ranking['Competitividad'] + ranking[c]
    ranking.drop(c, axis=1, inplace=True)
ranking['Competitividad'] = ranking['Competitividad'] / len(kpi_competitividad)

if args.compradores:
    ranking = pd.merge(ranking, neg_count, on='id_comprador')
    ranking = pd.merge(ranking, df_last_neg, on='id_comprador')
    ranking.set_index(['id_comprador'], inplace=True)

print(ranking.sort_values(by=['Ahorro']))
x = ranking[['Ahorro', 'Productividad', 'Competitividad']].values

if args.silhouette:
    elbow = False
    if elbow:
        WCSS = []
        range_n_clusters = range(1,11)
        for i in range_n_clusters:
            model = KMeans(n_clusters = i,init = 'k-means++')
            model.fit(x)
            WCSS.append(model.inertia_)
        fig = plt.figure(figsize = (7,7))
        plt.plot(range_n_clusters,WCSS, linewidth=4, markersize=12,marker='o',color = 'red')
        plt.xticks(np.arange(11))
        plt.xlabel("Number of clusters")
        plt.ylabel("WCSS")
        plt.show()

    elbow_alternative = False
    if elbow_alternative:
        # based on https://www.scikit-yb.org/en/latest/api/cluster/elbow.html
        #model = KMeans()
        #visualizer = KElbowVisualizer(model, k=(4,12))
        #visualizer.fit(x)        # Fit the data to the visualizer
        #visualizer.show()        # Finalize and render the figure
        kelbow_visualizer(KMeans(random_state=4), x, k=(2,10))

    ## Find the optimal number of clusters using silhouette score
    silhouette_avg = []
    range_n_clusters = range(2,11)
    for num_clusters in range_n_clusters:
       # initialise kmeans
       model = KMeans(n_clusters=num_clusters, init='k-means++')
       model.fit(x)
       cluster_labels = model.fit_predict(x)
       # silhouette score
       silhouette_avg.append(silhouette_score(x, cluster_labels))

    plt.plot(range_n_clusters, silhouette_avg)
    plt.xlabel('Numero de clusters')
    plt.ylabel('Silhouette score')
    plt.show()

if args.clusters:
    # finding the clusters based on input matrix "x"
    model = KMeans(n_clusters=args.clusters, init="k-means++", max_iter=300, n_init=10, random_state=0)
    y_clusters = model.fit_predict(x)

    # countplot to check the number of clusters and number of buyers in each cluster
    count_clusters = True
    if count_clusters:
        sns.countplot(x=y_clusters, palette='bright').set(ylabel='', title='{} en cada cluster'.format('Compradores' if args.compradores else 'Negociaciones'))
        plt.show()

    # https://github.com/mwaskom/seaborn/blob/e04b07eb3df135511e71e556c2bd34ef59ba08ba/seaborn/palettes.py#L32
    bright_palette=["#023EFF", "#FF7C00", "#1AC938", "#E8000B", "#8B2BE2", "#9F4800", "#F14CC1", "#A3A3A3", "#FFC400", "#00D7FF"]

    # 3d scatterplot using matplotlib
    fig = plt.figure(figsize = (15, 15))
    ax = fig.add_subplot(111, projection='3d')
    for c in range(args.clusters):
        ax.scatter(x[y_clusters == c,0], x[y_clusters == c,1], x[y_clusters == c,2], s=40, color=bright_palette[c])
    ax.text2D(0.05, 0.95, '{} de {}'.format('Compradores' if args.compradores else 'Negociaciones', args.cliente.split('_')[0].capitalize()), transform=ax.transAxes)
    ax.set_xlabel('Ahorro')
    ax.set_ylabel('Productividad')
    ax.set_zlabel('Competitividad')
    ax.set_xlim3d(0, 1)
    ax.set_ylim3d(0, 1)
    ax.set_zlim3d(0, 1)
    #ax.legend()
    plt.show()
