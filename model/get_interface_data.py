#!/usr/bin/env python

import json
import os
import argparse as ap
import pandas as pd
import udt_postgres as postgres
import warnings

# For getting a clean output without warnings:
warnings.filterwarnings('ignore')

POSTGRESQL_HOST = os.getenv('DATABASE_ADDR')
POSTGRESQL_USER = os.getenv('POSTGRESQL_USER')
POSTGRESQL_PASS = os.getenv('POSTGRESQL_PASS')
POSTGRESQL_DB = os.getenv('POSTGRESQL_DB')
POSTGRESQL_PORT = os.getenv('POSTGRESQL_PORT')


def main():
    interface_inputs = dict()
    
    # ==================================================================================================================
    # Parser
    # ==================================================================================================================

    parser = ap.ArgumentParser()

    parser.add_argument('--cliente', type=str, default='157')  # 157 is Carglass
    parser.add_argument('--subastas', type=str, default='subastas')
    parser.add_argument('--variantes', type=str, default='variantes')
    parser.add_argument('--categorias', type=str, default='cat_proveedores')

    args = parser.parse_args()

    # ======================================================================================
    # Acceso a la base de datos
    # ======================================================================================

    db = postgres.UDTPostgres(POSTGRESQL_DB + str(args.cliente), POSTGRESQL_USER, POSTGRESQL_PASS, POSTGRESQL_HOST, POSTGRESQL_PORT)

    # ======================================================================================
    # Lectura de los datos
    # ======================================================================================

    # Cargar la tabla de subastas
    # Eliminamos los registros que no tienen "id_categoria"
    df_sub = db.get_pandas_dataframe(args.subastas, [
        {'name': 'id_subasta'},
        {'name': 'id_subcategoria', 'type': 'int', 'notnull': True},
        {'name': 'id_comprador', 'type': 'int'},
        {'name': 'subtipo', 'type': 'int'},
        {'name': 'subsyscreado', 'type': 'sec'},
        {'name': 'subsyslanzado', 'type': 'sec', 'notnull': True}])

    # Cargar la tabla de variantes
    # Eliminamos los registros que no tienen "vriadjudicado".
    # Después de esto, esta columna ya no nos sirve y la eliminamos.
    # Eliminamos los registros que no tienen "baseline" o que el "importe" es cero.
    df_var = db.get_pandas_dataframe(args.variantes, [
        {'name': 'id_subasta'},
        {'name': 'id_variante'},
        {'name': 'vriadjudicado', 'onlyTrue': True, 'remove': True},
        {'name': 'baseline', 'notnull': True},
        {'name': 'importe', 'notZero': True},
        {'name': 'vriadjudicadofecha', 'type': 'sec'}])

    # Cargar la tabla de categorías
    # Eliminamos los registros en los que "asignada" es False y borramos la columna.
    df_cat = db.get_pandas_dataframe(args.categorias, [
        {'name': 'id_subcategoria'},
        {'name': 'id_proveedor'},
        {'name': 'asignada', 'onlyTrue': True, 'remove': True}])

    # ======================================================================================
    # Tratamiento de los datos
    # ======================================================================================

    # Variante con ahorro maximo
    df_var['ahorro'] = df_var['baseline'] - df_var['importe']
    df_var = df_var.sort_values('ahorro', ascending=False).drop_duplicates(['id_subasta'])


    # Obtenemos los compradores de cada negociación
    df = pd.merge(df_sub, df_var, on='id_subasta')
    neg_count = df.groupby(['id_comprador']).size().reset_index(name='negociaciones')

    # Obtenemos los proveedores invitados de cada subcategoría
    df_pro_cat_count = df_cat.groupby(['id_subcategoria']).size().reset_index(name='aceptados')

    # ======================================================================================
    # Generaos la salida
    # ======================================================================================

    interface_inputs["compradores"] = list()

    for index, row in neg_count.iterrows():
        interface_inputs["compradores"].append({
            'id': int(row['id_comprador']),
            'nombre': 'Comprador ' + str(int(row['id_comprador'])),
            'negociaciones': int(row['negociaciones'])
        })

    interface_inputs["subcategorias"] = []

    for index, row in df_pro_cat_count.iterrows():
        interface_inputs["subcategorias"].append({
            'id': int(row['id_subcategoria']),
            'nombre': 'Categoría ' + str(int(row['id_subcategoria'])),
            'proveedores_invitados': int(row['aceptados'])
        })

    interface_inputs['baseline'] = 1000
    interface_inputs['peso_ahorro'] = 1.0
    interface_inputs['peso_productividad'] = 1.0
    interface_inputs['peso_competitividad'] = 1.0

    print(json.dumps(interface_inputs, indent = 4))


if __name__ == '__main__':
    main()