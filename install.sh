#!/bin/bash

# Proactively stop and remove the volumes
docker-compose down --volumes

# Remove exited containers
docker rm -v $(docker ps -a -q -f status=exited)

# Create containers
# - d: -detach Detached mode: Run containers in the background.
docker-compose up --build -d --remove-orphans

# List containers
docker ps -a

