--
-- PostgreSQL database dump
--

-- Dumped from database version 10.19
-- Dumped by pg_dump version 10.19

-- Started on 2022-03-14 14:36:56 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--- Database
\c rhcitquot_157;

--
-- TOC entry 618 (class 1259 OID 203405)
-- Name: subastas; Type: TABLE; Schema: public; Owner: rhc
--

CREATE TABLE public.subastas (
    id_subasta integer NOT NULL,
    id_empresa integer,
    id_usuario integer,
    id_comprador integer,
    subtitulo text,
    subdesc text,
    subini timestamp without time zone,
    subfin timestamp without time zone,
    subvisible boolean DEFAULT false,
    subautorizado boolean DEFAULT false,
    subadjudicado boolean DEFAULT false,
    subdesierto boolean DEFAULT false,
    id_subcategoria integer,
    subfinalizado boolean DEFAULT false,
    subfecentregamax timestamp without time zone,
    subfecentregamin timestamp without time zone,
    subsuministracli text,
    subsuministrapro text,
    subgastosenvio boolean,
    subproverprecio boolean DEFAULT false,
    id_unidad integer,
    id_divisa integer,
    subdescdetalle text,
    subborrador boolean DEFAULT false,
    subunidad text,
    subobs text,
    subsyscreado timestamp without time zone DEFAULT now(),
    subsyslanzado timestamp without time zone,
    subsysadjudicado timestamp without time zone,
    subsyscreadopor integer,
    subsyslanzadopor integer,
    subsysadjudicadopor integer,
    subprecioobjetivo numeric,
    subsubasta boolean,
    subvalida boolean DEFAULT false,
    id_formapago integer,
    subformapago text,
    subtipo integer,
    subdirentrega text,
    subdivisa text,
    id_condenv integer,
    subcondenv text,
    subsubcat text,
    subplantilla boolean DEFAULT false,
    subabortada boolean DEFAULT false,
    obsoperacion text DEFAULT ''::text,
    subobsadjudicado text,
    subconfirmaadj boolean DEFAULT false,
    subsysconfadjpor integer,
    subsysconfadj timestamp without time zone,
    subsysabortado timestamp without time zone,
    subsysabortadopor integer,
    submejora numeric,
    id_mejora integer,
    subcie timestamp without time zone,
    subobsprv text,
    submaximunextension integer,
    subfechahoralimite timestamp without time zone,
    duracion_max integer,
    precioreserva numeric,
    numganadores integer,
    id_desempate integer DEFAULT 0,
    subdesempate double precision DEFAULT 0,
    visibilidad integer DEFAULT 0,
    mejorarprecio boolean DEFAULT true,
    mejorarporproducto boolean DEFAULT true,
    preciocierre numeric,
    tipopreciocierre integer DEFAULT 0,
    submejoramaxima numeric,
    id_mejoramaxima integer,
    submostrarcantidades boolean DEFAULT false,
    submostrarpreciototal boolean DEFAULT false,
    subsuperarposicion integer DEFAULT 0,
    id_sede integer,
    autorizadalanzamiento boolean,
    autorizadaanalisis boolean,
    autorizadaadjudicacion boolean,
    id_proyecto integer,
    faseiniciacion boolean DEFAULT false,
    fasenegociacion boolean DEFAULT false,
    fasecierre boolean DEFAULT false,
    sublanzado boolean DEFAULT false,
    aprobada_lanzamiento boolean DEFAULT false,
    aprobada_analisis boolean DEFAULT false,
    modalidad text DEFAULT 'avanzada'::text,
    estado text,
    multidivisa boolean DEFAULT false NOT NULL,
    peso_subtotal_mro numeric,
    tipo_mro text,
    cantidades_visibles boolean DEFAULT true,
    minutosparaextender integer,
    minutosextension integer,
    segundostramoholandesa integer,
    subtipomejora text,
    tipomejorarprecio numeric,
    negociacion_padre integer,
    fase integer DEFAULT 1
);


--- ALTER TABLE public.subastas OWNER TO rhc;

--
-- TOC entry 7485 (class 0 OID 0)
-- Dependencies: 618
-- Name: COLUMN subastas.tipomejorarprecio; Type: COMMENT; Schema: public; Owner: rhc
--

COMMENT ON COLUMN public.subastas.tipomejorarprecio IS 'Tipo de mejora mejorar precio';


--
-- TOC entry 1022 (class 1259 OID 205157)
-- Name: subastas_id_subasta_seq; Type: SEQUENCE; Schema: public; Owner: rhc
--

--- CREATE SEQUENCE public.subastas_id_subasta_seq
---     START WITH 1
---     INCREMENT BY 1
---     NO MINVALUE
---     NO MAXVALUE
---     CACHE 1;


--- ALTER TABLE public.subastas_id_subasta_seq OWNER TO rhc;

--
-- TOC entry 7487 (class 0 OID 0)
-- Dependencies: 1022
-- Name: subastas_id_subasta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rhc
--

--- ALTER SEQUENCE public.subastas_id_subasta_seq OWNED BY public.subastas.id_subasta;


--
-- TOC entry 7288 (class 2604 OID 207772)
-- Name: subastas id_subasta; Type: DEFAULT; Schema: public; Owner: rhc
--

--- ALTER TABLE ONLY public.subastas ALTER COLUMN id_subasta SET DEFAULT nextval('public.subastas_id_subasta_seq'::regclass);


--
-- TOC entry 7478 (class 0 OID 203405)
-- Dependencies: 618
-- Data for Name: subastas; Type: TABLE DATA; Schema: public; Owner: rhc
--

INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (138, 1, 49, 7, '138', NULL, '2018-06-05 20:52:02', '2018-06-05 20:52:02', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2018-06-05 20:52:02', NULL, NULL, 7, 7, NULL, NULL, NULL, false, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (139, 1, 49, 7, 'CFC - 033 - Alquiler Plataformas Elevadores y Grupos Electrógenos', 'RFP - Alquiler Plataformas Elevadoras y Grupos Electrógenos', '2018-06-07 14:06:31', '2018-06-15 18:00:00', false, true, true, false, 24, true, NULL, NULL, NULL, NULL, false, false, 1, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la
prestación de los ALQUILER DE PLATAFORMAS ELEVADORAS Y GRUPOS ELECTRÓGENOS para CARGLASS® durante el periodo de 1 de Julio de 2018 a 30 de Junio de 2019.
Con el fin de asegurar la correcta estabilización y homologación del servicio, se establece
periodo de prueba de tres meses, siendo avisados con anticipación de un mes.
La licitación se configurará en sucesivas rondas, siendo esta inicial una Petición de Propuestas.

CARGLASS® garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociación y concurrencia de ofertas en igualdad de condiciones.', false, NULL, NULL, '2018-06-06 14:14:20', '2018-06-07 14:06:31', NULL, 7, 7, NULL, NULL, NULL, true, NULL, NULL, 3, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, NULL, NULL, 'Alquiler Maquinaria', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-21 14:51:00', NULL, NULL, '2018-06-15 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (39, 1, 2, 2, '39', 'TRANSPORTE Y DISTRIBUCIÓN ESPAÑA  - P3', '2013-11-14 09:00:00', '2013-11-14 14:00:00', false, true, true, false, 9, true, NULL, NULL, NULL, NULL, NULL, false, 2, 1, 'FASE FINAL DE MEJORA EN LICITACIÓN DE SOBRE CERRADO

Los Sres Licitadores deben presentar su PROPUESTA DE FACTOR K (BASE100), MULTIPLICADOR LINEAL, a aplicar sobre todos los precios unitarios suministrados por CADA UNO de los 4 tipos de servicio:

K1 - FACTOR "ENTREGAS PROGRAMADAS"
K2 - FACTOR "ZONA HUBS-CENTROS"
K3 - FACTOR "INTERCENTROS"
K4 - FACTOR "BAJO DEMANDA"

Es decir, un K = 100 indica mantener los precios suministrados en fase RFP II para ese servicio, mientras K = 90 indica aplicar 10% de descuento a los precios de dicho servicio. (Descuento sobre cada tipologia de servicio = 100 - Ki)


***

DTO ESPECIAL POR INCREMENTO SUSTANCIAL DE VOLUMEN

Se solicita un DESCUENTO EN PORCENTAJE (%)  a aplicar sobre los precios en caso de que el volumen total del negocio (excepto efecto gasoil), sea superior EN UN 20% al previsto para el cálculo de la oferta.

Ejemplo: Dto (%) = 1,5 --> 1,5% de descuento adicional

***

ATENCION: LICITACION EN SOBRE CERRADO: LOS SRES LICITADORES SOLO PODRÁN ENTRAR SUS OFERTAS ENTRE LAS 09:00 Y LAS 14:00H DEL JUEVES, 14 DE NOVIEMBRE

Ni el personal de ITBID ni el de CARGLASS tienen acceso a la licitación durante la ventana de tiempo de cotoización de los proveedores.

', false, NULL, 'En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:


SR. D. JOAN CASTELLÀ
Móvil 659 060 561
Telf: 933 218 614
jcastella@itbid.org
', '2013-11-11 18:22:10.520145', '2013-11-11 18:22:48.714769', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 5, '
', 'EUR', 999, '--', 'Logistica', false, false, '', NULL, true, 2, '2014-01-09 12:13:17.767011', NULL, NULL, NULL, NULL, '2014-01-09 12:13:17.767011', NULL, NULL, '2013-11-14 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (23, 1, 2, 2, 'CFC005-B', 'CAJAS CIEMPOZUELOS - AJUSTE', '2013-04-12 12:43:52.333405', '2013-04-16 23:55:00', false, false, false, true, 5, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, NULL, false, NULL, 'ATENCION: PRECIO UNITARIOS CON PUNTO (".") COMO SEPARADOR DECIMAL.

NO CONTESTEN ESTA NUEVA PETICION SI NO MEJORAN SUS PRECIOS', '2013-04-12 12:42:56.01002', '2013-04-12 12:43:52.333405', '2013-04-19 13:55:48.19496', 2, 2, 2, NULL, NULL, true, 0, '30 días Transferencia', 4, 'CIEMPOZUELOS
', 'EUR', 0, 'DDP - Ciempozuelos', 'Carton ondulado', false, false, '[ 15-Abril-2013 20:33:27 ]  Jose Manuel Iáñez :   Añado Saica Carton
', NULL, false, 2, '2013-04-19 13:55:48.19496', NULL, NULL, NULL, NULL, '2013-04-19 13:55:48.19496', NULL, NULL, '2013-04-16 23:55:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'desierta', false, 80, 'sumatorio', true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (140, 1, 49, 7, '140', NULL, '2018-06-22 16:01:56', '2018-06-22 16:01:56', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2018-06-22 16:01:56', NULL, NULL, 7, 7, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (145, 1, 49, 7, '145', NULL, '2018-07-17 16:01:13', '2018-07-17 16:01:13', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2018-07-17 16:01:13', NULL, NULL, 7, 7, NULL, NULL, NULL, false, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (148, 1, 818, 16, '148', 'Licitación Servicio Calibración Sistemas ADAS', '2018-11-05 14:18:22', '2018-11-05 14:18:00', false, false, false, false, 21, false, NULL, NULL, NULL, NULL, false, false, 1, 1, NULL, true, NULL, NULL, '2018-11-05 14:18:22', NULL, NULL, 16, 16, NULL, NULL, NULL, false, NULL, NULL, 1, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, NULL, NULL, 'Consulting / Consultoría', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-05 14:18:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'borrador', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (141, 1, 49, 7, '141', NULL, '2018-06-22 16:17:18', '2018-06-22 16:17:18', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2018-06-22 16:17:18', NULL, NULL, 7, 7, NULL, NULL, NULL, false, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, 'ponderado', true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (25, 1, 19, 6, 'CFC005-S', 'CAJAS Y PIEZAS DE CARTON - FASE DE MEJORA', '2013-04-19 13:00:00', '2013-04-19 13:25:00', false, true, true, false, 5, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'Fase de Mejora, mediante SUBASTA DE PRECIO VISIBLE.

Ver anexo "Reglas_subasta_CFC005.PDF"

Resumen:

1 único criterio: el TOTAL estimado (suma de precios x cantidades)

El mejor precio es visible

Solo se admiten ofertas que mejoren el mejor precio

La mejora del precio sólo podrá ser entre el 0,25 y el 2%

NO se muestra la posición

La oferta inicial NO es editable hasta el inicio de la subasta', false, NULL, 'Esta fase de mejora de ofertas perfecciona el Pliego de Condiciones de la fase de petición de ofertas precedente, pero no lo sustituye ni lo anula. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

SR. D. JOSE MANUEL IAÑEZ
ITBID TECHNOLOGIES
Móvil 676 953 333
jmianez@itbid.org

', '2013-04-17 18:01:45.602422', '2013-04-17 18:02:19.555872', NULL, 6, 6, NULL, NULL, NULL, true, 0, '30 días Transferencia', 10, 'CIEMPOZUELOS
', NULL, 0, 'DDP - Ciempozuelos', 'Carton ondulado', false, false, '', NULL, true, 2, '2013-04-19 13:57:33.633338', NULL, NULL, 0.25, 8, '2013-04-19 13:57:33.633338', NULL, 0, '2013-04-19 13:15:00', 15, NULL, NULL, NULL, 0, 1, true, true, 0, 2, 2, 6, false, true, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (146, 1, 770, 13, '146', 'SOFTWARE PEOPLE HR CARGLASS - UPDATED RFI', '2018-09-26 17:05:23', '2018-10-17 17:00:00', false, false, false, true, 23, true, NULL, NULL, 'All documents and actual business induction will be deliver to the selected companies. The B2B interviews will be arranged by CARGLASS®.', 'See Briefing.pdf attached.', false, false, 1, 1, 'O presente documento de pedido de informação (RFI) forma parte da investigação de mercado que vem realizando CARGLASS® a respeito das tendências, soluções e potenciais fornecedores para suas necessidades no âmbito dos Serviços de Recursos Humanos, com a finalidade de dimensionar e configurar um projeto para a seleção do fornecedor idoneo para a prestação do serviço.

Please, read carrefully attached Briefing and RFI Terms.', false, NULL, NULL, '2018-09-26 16:58:25', '2018-09-26 17:05:23', NULL, 13, 13, NULL, NULL, NULL, true, 0, NULL, 3, NULL, NULL, NULL, 'DDP - Ciempozuelos', 'Software', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-12 15:30:50', NULL, NULL, '2017-09-29 12:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, false, false, false, 17, true, true, false, false, false, false, 'avanzada', 'desierta', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (83, 1, 2, 2, 'CFC025-3', 'RENTING EQUIPOS INFORMATICOS 2015 - FASE FINAL', '2015-07-03 13:00:00', '2015-07-03 14:00:00', false, true, true, false, 11, true, NULL, NULL, NULL, NULL, true, false, 1, 1, 'ENTRAR UNA UNICA Y DEFINITIVA OFERTA, SOLO ENTRE LAS 13:00 Y LAS 14:00H DEL VIERNES 3 JULIO.

TODOS LOS PRECIOS UNITARIOS, SIN IVA Y REFERIDOS EXCLUSIVAMENTE A LOS PRODUCTOS COTIZADOS ANTERIORMENTE

', false, NULL, 'En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

JOSE MANUEL IAÑEZ
ITBID ESOURCING SOLUTIONS
Móvil +34 676 953 333
jmianez@itbid.com', '2015-07-02 18:33:59.051683', '2015-07-02 18:35:18.80222', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'SEGUN PLIEGO DE CONDICIONES', 5, 'SEGUN PLIEGO DE CONDICIONES
', 'EUR', 999, 'SEGUN PLIEGO DE CONDICIONES', 'PC, Monitores y Laptops', false, false, '', NULL, true, 2, '2015-07-09 13:11:42.545897', NULL, NULL, NULL, NULL, '2015-07-09 13:11:42.545897', NULL, NULL, '2015-07-03 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, 8, false, false, true, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (193, 1, 771, 14, '193', NULL, '2020-07-02 11:18:48', '2020-07-02 11:18:48', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-07-02 11:18:48', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (147, 1, 818, 16, 'CFC-036', 'LICITACION SERVICIO DE LIMPIEZA HUBS Y RED DE CENTROS', '2018-10-31 17:37:32', '2018-11-14 11:05:00', false, true, true, false, 19, true, NULL, NULL, NULL, NULL, false, false, 2, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación de diversos servicios de LIMPIEZA de los diferentes Hubs y Red de Centros de Carglass para el periodo  diciembre de 2018 a diciembre  2020 (24 meses).

Ver adjunto PLIEGO DE CONDICIONES.
Ver adjunto TABLA DE CONDICIONES

Para cotizar, se debe descargar, cumplimentar y devolver el Excel "Tabla Cotización Limpieza".', false, NULL, 'En caso de dudas sobre las condiciones del servicio o este pliego, los Sres. Proveedores deben comunicarlas a través del “Centro de Mensajes” de la propia negociación electrónica. Las dudas serán atendidas a la mayor brevedad posible.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa al uso de la plataforma de negociación ITBID, por favor contactar con:

Mario Lázaro
Móvil 676 953 333
Telf. 933 218 614
mlazaro@itbid.com', '2018-10-30 18:21:31', '2018-10-31 17:37:32', NULL, 16, 16, NULL, NULL, NULL, true, 0, NULL, 1, 'No aplica - El servicio se realizará en cada centro.', NULL, NULL, NULL, 'Limpieza', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-09 10:56:42', NULL, NULL, '2018-11-08 18:21:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (142, 1, 49, 7, '142', NULL, '2018-06-22 16:17:57', '2018-06-22 16:17:57', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2018-06-22 16:17:57', NULL, NULL, 7, 7, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (151, 1, 818, 16, '151', NULL, '2018-11-23 12:34:59', '2018-11-23 12:34:59', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2018-11-23 12:34:59', NULL, NULL, 16, 16, NULL, NULL, NULL, false, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (22, 1, 2, 2, 'CFC005', 'CAJAS Y PIEZAS DE CARTON PARA CIEMPOZUELOS', '2013-03-25 16:49:34.011426', '2013-04-08 21:00:00', false, false, false, true, 5, true, '2014-04-30 00:00:00', '2013-05-01 00:00:00', NULL, NULL, true, false, 0, 1, NULL, false, NULL, 'ENTRAR PRECIOS EN FORMATO "123.45", CON EL PUNTO COMO SEPARADOR DECIMAL


Si algún licitador experimentara dificultades técnicas para la entrada de la oferta o la subida de ficheros, deberá contactar con:

Sr. Jose Manuel Iáñez
ITbid Technologies
e-mail: jmianez@itbid.org
Móvil 676 953 333
', '2013-03-25 16:49:13.312646', '2013-03-25 16:49:34.011426', '2013-04-19 13:55:30.273493', 2, 2, 2, NULL, NULL, true, 0, '30 días Transferencia', 4, 'HUB CIEMPOZUELOS - MADRID
', 'EUR', 0, 'DDP - Ciempozuelos', 'Carton ondulado', false, false, '[ 03-Abril-2013 12:02:40 ]  Jose Manuel Iáñez :   CAMBIO FECHA HORA FINAL
[ 08-Abril-2013 15:35:50 ]  Jose Manuel Iáñez :   mod hora aliaga
[ 08-Abril-2013 18:05:10 ]  Jose Manuel Iáñez :   Edicion precios faltantes por decimal
', NULL, false, 2, '2013-04-19 13:55:30.273493', NULL, NULL, NULL, NULL, '2013-04-19 13:55:30.273493', NULL, NULL, '2013-04-08 21:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'desierta', false, 80, 'sumatorio', true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (143, 1, 49, 7, 'CFC-033 - Alquiler Plataformas Elevadoras', 'RFQ - Alquiler Plataformas Elevadoras y Grupos Electrógenos', '2018-06-22 17:23:55', '2018-06-28 15:00:00', false, true, true, false, 24, true, NULL, NULL, NULL, NULL, false, false, 1, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación de los ALQUILER DE PLATAFORMAS ELEVADORAS Y GRUPOS ELECTRÓGENOS para CARGLASS® durante el periodo de 1 de Julio de 2018 a 30 de Junio de 2019.
Con el fin de asegurar la correcta estabilización y homologación del servicio, se establece periodo de prueba de tres meses, siendo avisados con anticipación de un mes. 
La licitación se configurará en sucesivas rondas, siendo esta inicial una Petición de Propuestas.
CARGLASS®® garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociación y concurrencia de ofertas en igualdad de condiciones.', false, NULL, NULL, '2018-06-22 16:34:43', '2018-06-22 17:23:55', NULL, 7, 7, NULL, NULL, NULL, true, NULL, NULL, 3, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, NULL, NULL, 'Alquiler Maquinaria', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-04 17:14:04', NULL, NULL, '2018-06-28 12:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (38, 1, 2, 2, 'CFC004-P2', 'TRANSPORTE Y DISTRIBUCIÓN ESPAÑA Y PORTUGAL - RFQ2', '2013-10-25 14:02:05.609237', '2013-11-06 20:00:00', false, true, true, false, 9, true, NULL, '2014-02-01 00:00:00', NULL, NULL, NULL, false, 1, 1, 'Determinación de los proveedores y condiciones óptimas para la prestación del servicio de TRANSPORTE Y DISTRIBUCIÓN DE LOS ALMACENES DE CARGLASS A SUS HUB’S, CENTROS Y CLIENTES EXTERNOS EN ESPAÑA Y PORTUGAL

Vean en Ficheros Adjuntos:

PLIEGO DE CONDICIONES (PDF) de esta licitación
ANEXOS I a III, Excel por tipo de servicio y PAIS
ANEXO VI, Relación de Tallleres Carglass

Los Sres Licitadores deben presentar su propuesta para todos los tipos de servicio solicitados.

Para cumplimentar una propuesta:

* Rellene los casilleros de la propia web (2 por cada versión o tipo de servicio)
* Adjunte cumplimentado los Anexo I, II y III de ambos paises

FECHA DE INICIO DEL SERVICIO: 1 FEBRERO 2014
', false, NULL, 'En caso de dudas sobre las condiciones del servicio, los Srs. Proveedores deben entrarlas a través del “Centro de Mensajes” de la propia negociación electrónica.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:


SR. D. JOAN CASTELLÀ

Móvil 659 060 561
Telf: 933 218 614
jcastella@itbid.org
', '2013-10-25 13:59:51.401923', '2013-10-25 14:02:05.609237', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 1, '--', 'EUR', 999, 'Ver Pliego de Condciones', 'Logistica', false, false, '[ 30-Octubre-2013 15:42:58 ]  Jose Manuel Iáñez :   Ampliación Plazoos consulta y recepción ofertas ( comunicacion via centro mensajes)
[ 19--2013 17:46:26 ]  Equipo ITbid :   No se Adjudica, se pasa a fase final de licitacion de Sobre cerrado LSC3 con los 3 proveedores identficados  
', NULL, true, 2, '2013-11-19 17:55:32.316033', NULL, NULL, NULL, NULL, '2013-11-19 17:55:32.316033', NULL, NULL, '2013-11-06 20:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (153, 1, 818, 16, '153', NULL, '2018-12-14 11:43:42', '2018-12-14 11:43:42', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2018-12-14 11:43:42', NULL, NULL, 16, 16, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'simple', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (197, 1, 2, 2, '197', NULL, '2020-11-04 22:36:45', '2020-11-04 22:36:45', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-11-04 22:36:45', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (84, 1, 2, 2, 'FCP005P-4', 'SERVIÇO TRANSPORTE - LONGA DISTÂNCIA', '2015-07-07 10:00:00', '2015-07-07 15:00:00', false, true, true, false, 9, true, '2015-07-27 00:00:00', '2015-07-20 00:00:00', NULL, NULL, NULL, false, 2, 1, 'SERVIÇO DE TRANSPORTE LONGA DISTÂNCIA - 
PERÍODO DE 01 DE JUNHO DE 2015 E 31 DE MAIO DE 2017 (24 MESES).

Faça o download da documentação e Condiçoes do concurso e o Excel de Cotacão.

O serviço de transporte consiste no traslado de vidro para automóveis e materiais auxiliares para montagem e condicionamiento a partir: 

* Dos armazéns centrais da CARGLASS em Ciempozuelos (Madrid-Espanha) para  Armazém centrai (HUB’s) em Portugal - Lisboa 



', false, NULL, 'Os fornecedores enviaram suas perguntas através do &quot;Centro de Mensagens&quot; da própia negociação. O prazo para envio de consultas através da plataforma ITBID será o indicado no documento de Condiçoes.

ITbid irá responder a todas as perguntas, por escrito, por meio do &quot;Centro de Mensagens&quot; para todos os fornecedores, na data indicada no calendário. As respostas que sejam identificadas de interesse geral serão enviadas para todos os fornecedores convidados ao processo, sem indicar quem o fornecedor que fez a consulta. 

Em caso de dúvidas sobre os códigos de acesso, a introdução de licitações ou qualquer assunto relacionado com a plataforma de negociação ITbid, por favor, entre em contacto com:

SR. JOSEP PUIG
Telf: + 34 933 218 614
jpuig@itbid.com
Cel: + 34 655347607


', '2015-07-06 12:46:51.536508', '2015-07-06 12:47:36.690912', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 5, 'LISBOA - VIALONGA
', 'EUR', 999, 'Segun Condições e Regras de Negociação Sealed Bid Auction IV', 'Logistica', false, false, '', NULL, true, 2, '2015-07-14 09:13:41.210312', NULL, NULL, NULL, NULL, '2015-07-14 09:13:41.210312', NULL, NULL, '2015-07-07 15:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (116, 1, 2, 2, '116', NULL, '2017-05-18 18:16:33', '2017-05-18 18:16:33', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2017-05-18 18:16:33', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (154, 1, 818, 16, '154', 'Ejemplo', '2018-12-14 11:43:56', '2018-12-25 11:43:00', false, false, false, false, 6, false, NULL, NULL, NULL, NULL, false, false, 1, 1, 'Ejemplo', true, NULL, NULL, '2018-12-14 11:43:56', NULL, NULL, 16, 16, NULL, NULL, NULL, false, NULL, NULL, 1, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, NULL, NULL, 'Electricidad', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-14 11:43:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'borrador', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (76, 1, 2, 2, 'CFC025', 'RENTING EQUIPOS INFORMATICOS 2015 (ES) y (PT)', '2015-05-19 18:49:50.38696', '2015-06-04 19:00:00', false, true, true, false, 11, true, NULL, NULL, NULL, NULL, true, false, 1, 1, 'TENDER PARA RENTING DE EQUIPOSINFORMATICOS 2015 - ESPAÑA Y PORTUGAL

NO ES NECESARIO COTIZAR AMBOS PAISES

RENTING A 4 AÑOS - 48 CUOTAS - TODOS LOS PRECIOS UNITARIOS SIN IVA

SE SOLICITA ADEMAS PRECIO AL CONTADO

SE DEBE ANEXAR LA FICHA TECNICA DE CADA PRODUCTO JUNTO CON CADA OFERTA

AUNQUE EL SISTEMA ADMITE ILIMITADAS OFERTAS, POR FAVOR, LIMITENLAS A 2 POR VERSION (COMBINACION DE PRODUCTO-PAIS)

ALGUNA DE LAS OFERTAS DEBE INCLUIR SU OFERTA TECNICA Y ECONÓMICA COMPLETA.
', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre características de los equipos, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

SR. D. JOSE MANUEL IÁÑEZ
ITbid Technologies
Móvil 676 953 333
jmianez@itbid.com', '2015-05-19 18:48:19.481086', '2015-05-19 18:49:50.38696', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'CONTRATO RENTING', 1, 'VER PLIEGO DE CONDICIONES', 'EUR', 999, 'VER PLIEGO DE CONDICIONES', 'PC, Monitores y Laptops', false, false, '', NULL, true, 2, '2015-06-22 10:20:28.152578', NULL, NULL, NULL, NULL, '2015-06-22 10:20:28.152578', NULL, NULL, '2015-06-04 19:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, 8, false, true, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (103, 1, 2, 2, '103', 'kjlh', '2016-08-30 16:06:08', '2016-08-30 17:15:00', false, false, false, false, 16, false, NULL, NULL, NULL, NULL, false, false, 1, 1, NULL, false, NULL, NULL, '2016-08-30 16:06:08', NULL, NULL, 2, 2, NULL, NULL, NULL, false, 0, NULL, 4, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 0, NULL, 'Alquiler Turismos - Car Rental', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, 80, 'sumatorio', true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (32, 1, 91, 8, 'CFC004-P', 'SERVICIO DE TRANSPORTE Y DISTRIBUCIÓN', '2013-07-10 14:07:02.542714', '2013-09-02 23:55:00', false, true, true, false, 9, true, NULL, '2013-11-01 00:00:00', NULL, NULL, NULL, false, 0, 1, 'Determinación de los proveedores y condiciones óptimas para la prestación del servicio de TRANSPORTE Y DISTRIBUCIÓN DE LOS ALMACENES DE CARGLASS A SUS HUB’S, CENTROS Y CLIENTES EXTERNOS.

Vean en Ficheros Adjuntos:

PLIEGO DE CONDICIONES (PDF) de esta licitación
ANEXOS I a III, Excel por tipo de servicio
ANEXO IV, Relación de Tallleres Carglass

Los Sres Licitadores pueden seleccionar presentar su propuesta para uno o varios de tipos de servicio solicitados.

Para cumplimentar una propuesta:

* Rellene los casilleros de la propia web
* Adjunte cumplimentado el Anexo I, II o III, según corresponda
* Adjunte el resto de documentos e información solicitados

FECHA DE INICIO DEL SERVICIO: 1 NOVIEMBRE 2013
', false, NULL, 'En caso de dudas sobre las condiciones del servicio, los Srs. Proveedores deben entrarlas a través del “Centro de Mensajes” de la propia negociación electrónica.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:


SR. D. JOAN CASTELLÀ

Móvil 659 060 561
Telf: 933 218 614
jcastella@itbid.org
', '2013-07-10 14:06:32.90083', '2013-07-10 14:07:02.542714', NULL, 8, 8, NULL, NULL, NULL, true, 0, '30 días Transferencia', 3, 'NO APLICA
', NULL, 999, NULL, 'Logistica', false, false, '[ 11-Julio-2013 13:22:25 ]  Joan Castella :   Añadidos 9 proveedores - MAIL2
[ 15-Julio-2013 10:05:37 ]  Joan Castella :   Añadidos 2 proveedores
[ 15-Julio-2013 10:22:02 ]  Jose Manuel Iáñez :   mail3
[ 15-Julio-2013 11:07:02 ]  Jose Manuel Iáñez :   mail3
[ 18-Julio-2013 16:48:42 ]  Jose Manuel Iáñez :   Añadido Truck 
[ 18-Julio-2013 19:28:42 ]  Jose Manuel Iáñez :   añadido DHL
[ 02-Agosto-2013 11:38:32 ]  Jose Manuel Iáñez :   Añadido Barbera
', NULL, true, 2, '2013-10-25 14:05:30.022491', NULL, NULL, NULL, NULL, '2013-10-25 14:05:30.022491', NULL, NULL, '2013-09-02 23:55:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (144, 1, 770, 13, '144', 'SOFTWARE PEOPLE HR CARGLASS', '2018-06-26 19:02:42', '2018-07-13 21:00:00', false, true, true, false, 23, true, NULL, NULL, 'All documents and actual business induction will be deliver to the selected companies. The B2B interviews will be arranged by CARGLASS®.', 'See Briefing.pdf attached.', false, false, 1, 1, 'O presente documento de pedido de informação (RFI) forma parte da investigação de mercado que vem realizando CARGLASS® a respeito das tendências, soluções e potenciais fornecedores para suas necessidades no âmbito dos Serviços de Recursos Humanos, com a finalidade de dimensionar e configurar um projeto para a seleção do fornecedor idoneo para a prestação do serviço.

Please, read carrefully attached Briefing and RFI Terms.', false, NULL, NULL, '2018-06-26 15:05:51', '2018-06-26 19:02:42', NULL, 13, 13, NULL, NULL, NULL, true, 0, NULL, 3, NULL, NULL, NULL, 'DDP - Ciempozuelos', 'Software', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-09 10:27:12', NULL, NULL, '2017-09-29 12:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, true, true, false, 17, true, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (198, 1, 2, 2, '198', NULL, '2020-11-04 22:42:57', '2020-11-04 22:42:57', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-11-04 22:42:57', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (42, 1, 2, 2, 'CFC012-P', 'Renovacion Equipos 2014-1', '2013-11-13 12:12:55.726347', '2013-11-21 14:00:00', false, true, true, false, 11, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'Proceso para la adquisición de 80 PC, 80 monitores y 50 Laptops para renovación de equipos de Centros y Call Center CARCLASS.

VER FICHERO ADJUNTO: 

Reglas_y_condiciones.PDF

RESUMEN

* Cotizar 2 versiones (Básica y Superior)
* En caso de ser declarado inicialmente finalista, se deberá depositar un equipo para ser homologado por CARGLASS.
* Los modelos se deben considerar con 3 años de garantía
* Junto con las ofertas se han de anexar los documentos indicados en el pliego de condiciones.
', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre características de los equipos, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de
negociación ITbid, por favor contactar con:

SR. D. JOSE MANUEL IÁÑEZ
ITbid Technologies
Móvil 676 953 333
jmianez@itbid.org', '2013-11-13 12:11:29.096793', '2013-11-13 12:12:55.726347', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 3, 'FACUNDO BACARDI I MASSO 7- 11
08100 MOLLET DEL VALLES
(Barcelona)
SPAIN
', NULL, 999, NULL, 'PC, Monitores y Laptops', false, false, '', NULL, true, 2, '2014-05-12 17:47:04.823755', NULL, NULL, NULL, NULL, '2014-05-12 17:47:04.823755', NULL, NULL, '2013-11-21 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (102, 1, 2, 2, '102', 'CFC030 LICITACION SERVICIO DE LIMPIEZA HUBS Y RED DE CENTROS', '2016-08-05 21:15:00', '2016-09-14 14:00:00', false, true, true, false, 19, true, NULL, NULL, NULL, NULL, false, false, 1, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación de diversos servicios de LIMPIEZA de los diferentes Hubs y Red de Centros de Carglass para el periodo  Agosto de 2016 a de Agosto de 2018 (24 meses).

Ver adjunto PLIEGO DE CONDICIONES.

Para cotizar, se debe descargar, cumplimentar y devolver el EXCEL DE COTIZACION.', false, NULL, 'En caso de dudas sobre las condiciones del servicio o este pliego, los Sres. Proveedores deben entrarlas a través del “Centro de Mensajes” de la propia negociación electrónica. Las dudas serán atendidas a la mayor brevedad posible.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa al uso de la plataforma de negociación ITBID, por favor contactar con:

Maricarmen Luzon
Móvil 636 109 345
Telf. 936 345 009
mcluzon@itbid.com', '2016-08-05 20:40:25', '2016-08-05 21:05:56', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 5, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 0, NULL, 'Limpieza', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2016-09-29 16:12:38', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (106, 1, 2, 2, '106', NULL, '2016-11-14 13:12:58', '2016-11-14 13:12:58', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2016-11-14 13:12:58', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (52, 1, 2, 2, 'CFC014-2', 'FORMACION Y DESARROLLO DEL PERSONAL-FINAL', '2014-05-07 18:39:34.141292', '2014-05-15 14:00:00', false, true, true, false, 14, true, '2016-06-30 00:00:00', '2014-07-01 00:00:00', NULL, NULL, NULL, false, 0, 1, 'Fase final del proceso de selección del proveedor (o proveedores) idóneo que actué como partner-colaborador en el diseño del plan formativo y en la prestación de los servicios de formación y desarrollo del personal de Carglass en España, para el periodo Julio 2014 – Junio 2016 (2 años).

Esta fase final continúa el proceso iniciado en la fase precedente, de petición de propuestas. En tanto no se modifiquen aquí, o como consecuencia de lo aquí solicitado, son de aplicación y válidas las informaciones, documentos y propuestas previamente realizadas por los licitadores.

VER FICHEROS ADJUNTOS:
*Pliego_Condiciones_Fase2_CFC014.PDF --> Pliego de condiciones
Programa_Formativo_PARA_RFQ_2A FASE.XLSX --> Excel a ser descargado, cumplimentado y devuelto a la plataforma.

El alcance de los servicios requeridos ha sido precisado en el PLIEGO.
', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

JOSE MANUEL IAÑEZ 
ITbid Technologies
Telf: 933 218 614
Móvil 676 953 333
jmianez@itbid.org

', '2014-05-07 18:38:47.449688', '2014-05-07 18:39:34.141292', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 3, 'NO PROCEDE ', NULL, 999, NULL, 'Formacion', false, false, '', NULL, true, 2, '2014-06-05 11:56:48.975252', NULL, NULL, NULL, NULL, '2014-06-05 11:56:48.975252', NULL, NULL, '2014-05-15 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (156, 1, 818, 16, '156', NULL, '2019-01-16 09:01:14', '2019-01-16 09:01:14', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2019-01-16 09:01:14', NULL, NULL, 16, 16, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (157, 1, 818, 16, '157', NULL, '2019-01-16 09:15:31', '2019-01-16 09:15:31', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2019-01-16 09:15:31', NULL, NULL, 16, 16, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (101, 1, 2, 2, '101', NULL, '2016-08-05 20:36:08', '2016-08-05 20:36:08', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2016-08-05 20:36:08', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (57, 1, 2, 2, 'CFC015-P2', 'IMPRESORAS PORTATILES - 2ª RONDA', '2014-06-06 12:45:01.847088', '2014-06-12 14:00:00', false, true, true, false, 15, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'ESTA RFP ES UNA SEGREGACION DE LA ANTERIOR, SOLO PARA IMPRESORAS. LEAN CON DETENIMIENTO LO QUE SIGUE:

Deben considerar una impresora similar a la cotizada previamente, DE 3 PULGADAS, con los accesorios que se solicitan. El precio es un criterio importante.

* Conectividad: Bluetooth

Si alguno de los accesorios indicados viene incluido por defecto, entren "0" en su precio.

TODFOS LOS PRECIOS, SIN IVA.

ACCESORIOS A INCLUIR:

* Cargador CA
* Adaptador alimentación vehículo (a encendedor)
* Funda Protectora (silicona o similar)
* Estuche Nylon o similar, con pinza para cinturón 

PLAZOS DEL PROYECTO:

- 1 Unidad, a ser entregada y testeada en cuanto se comunique adjudicación
- 14 Unidades para piloto, ultima semana junio- 1ª julio
- 105 Unidades a entregar en Setiembre

ANEXAR FICHA TÉCNICA DE LA IMPRESORA Y ACCESORIOS, SI FUERA POSIBLE', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre características de los equipos, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica.   

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:  

SR. D. JOSE MANUEL IÁÑEZ 
ITbid Technologies 
Móvil 676 953 333 
jmianez@itbid.org
', '2014-06-06 12:43:15.552626', '2014-06-06 12:45:01.847088', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 3, 'FACUNDO BACARDI I MASSO 7- 11
08100 MOLLET DEL VALLES
(Barcelona)
SPAIN
', NULL, 999, NULL, 'Smartphones y dispositivos móviles', false, false, '[ 12-Junio-2014 12:17:34 ]  Equipo ITbid :   Ventana J Rioja
', NULL, true, 2, '2014-07-25 11:14:34.30856', NULL, NULL, NULL, NULL, '2014-07-25 11:14:34.30856', NULL, NULL, '2014-06-12 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (155, 1, 818, 16, 'CFC-039', 'CFC-039 - Suministro de maquinaria almacen', '2019-01-29 22:36:54', '2019-02-13 12:05:00', false, false, false, true, 2, true, NULL, NULL, NULL, NULL, false, false, 1, 1, 'El objeto de esta negociación es determinar el proveedor y condiciones óptimas para el alquiler, con todos los servicios incluidos, de carretillas y vehículos para manipulación y almacenaje para el almacén de CARGLASS ubicado en Ciempozuelos (Madrid).

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa al uso de la plataforma de negociación ITBID, por favor contactar con:

Mario Lázaro Cerván
Móvil 676 953 333
Telf. 933 218 614
mlazaro@itbid.com', false, NULL, NULL, '2019-01-09 16:31:48', '2019-01-29 22:36:54', NULL, 16, 16, NULL, NULL, NULL, true, NULL, NULL, 1, 'Hub Ciempozuelos: Calle de las Moreras, 28350 Ciempozuelos, Madrid.', NULL, NULL, NULL, 'Carretillas', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-26 16:03:36', NULL, NULL, '2019-01-31 16:31:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'desierta', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (160, 1, 818, 16, '160', 'CFC-039 - Suministro de maquinaria almacen', '2019-02-11 14:02:30', '2019-02-11 14:02:30', false, false, false, false, 2, false, NULL, NULL, NULL, NULL, false, false, 1, 1, 'El objeto de esta negociación es determinar el proveedor y condiciones óptimas para el alquiler, con todos los servicios incluidos, de carretillas y vehículos para manipulación y almacenaje para el almacén de CARGLASS ubicado en Ciempozuelos (Madrid).

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa al uso de la plataforma de negociación ITBID, por favor contactar con:

Mario Lázaro Cerván
Móvil 676 953 333
Telf. 933 218 614
mlazaro@itbid.com', false, NULL, NULL, '2019-02-11 14:02:30', '2019-01-29 22:36:54', NULL, 16, 16, NULL, NULL, NULL, false, NULL, NULL, 1, 'Hub Ciempozuelos: Calle de las Moreras, 28350 Ciempozuelos, Madrid.', NULL, NULL, NULL, 'Carretillas', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-31 16:31:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (199, 1, 2, 2, '199', NULL, '2020-11-04 22:43:13', '2020-11-04 22:43:13', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-11-04 22:43:13', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (159, 1, 818, 16, 'CFC037 FASE II', 'CFC037 - Central de Alarmas FASE II', '2019-01-17 12:37:47', '2019-01-22 20:00:00', false, true, true, false, 18, true, '2019-01-21 00:00:00', '2019-01-17 00:00:00', NULL, NULL, NULL, false, 1, 1, 'El objeto de esta negociación es la prestación del servicio de Central de Alarmas y otros servicios, para la Red de Centros y Hubs de Carglass. CARGLASS garantiza a los proveedores seleccionados la transparencia de la negociación y concurrencia de ofertas en igualdad de condiciones.

CARGLASS  garantiza a los proveedores seleccionados la transparencia de la negociación y concurrencia de ofertas en igualdad de condiciones.

Ver Documentación  Adjunta:
*Guia_Rápida_RFQ_RFP_Proveedor_ES.pdf
*Tabla_Cotización_FASE_II
*Pliego_Condiciones_FASE_II

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

MARIO LÁZARO
ITBID
Móvil +34 676 953 333
mlazaro@itbid.com', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre características de los equipos, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

MARIO LÁZARO 
ITBID 
Móvil +34 676 953 333
mlazaro@itbid.com', '2019-01-17 10:54:38', '2019-01-17 12:37:47', NULL, 16, 16, NULL, NULL, NULL, true, NULL, '30 días Transferencia', 1, 'NO PROCEDE - VER PLEIGO DE CONDICIONES', NULL, 999, NULL, 'Serv Seguridad / Central de Alarmas', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-15 12:10:32', NULL, NULL, '2018-12-17 12:10:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (131, 1, 49, 7, '131', NULL, '2017-11-10 20:48:45', '2017-11-10 20:48:45', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2017-11-10 20:48:45', NULL, NULL, 7, 7, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (117, 1, 2, 2, '117', NULL, '2017-05-18 18:16:34', '2017-05-18 18:16:34', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2017-05-18 18:16:34', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (12, 1, 19, 6, 'ALM-02B', 'ALQUILER DE CARRETILLAS - RFQ2', '2012-10-29 02:35:06.928827', '2012-11-02 23:55:00', false, true, true, false, 2, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'ALQUILER DE DE CARRETILLAS Y VEHICULOS PARA
ALMACENAJE PARA LOS ALMACENES CARGLASS DE
CIEMPOZUELOS 

Carglass ha decidido que la adquisición de equipo para el nuevo centro de distribución de Ciempozuelos y la renovación del parque de Mollet sea de manera gradual. Durante la primera fase se adquirirá sólo el equipo nuevo, y conforme se vaya necesitando se irán sustituyendo las máquinas existentes. 

Por esta razón se valorará positivamente que los precios que se ofrezcan tengan una vigencia de hasta 2 años. 

Los plazos solicitados pasan a ser de 72 y 84 meses.

También se han cambiado algunas especificaciones para el equipo, las cuales se detallan EN FONDO AZUL en el Anexo I.

', false, NULL, ' 	

En caso de dudas sobre la plataforma, entrada de
ofertas o anexar ficheros, por favor, contactar
con:

JOSE MANUEL IAÑEZ
Mov. 676 953 333
email jmianez@itbid.org

', '2012-10-29 02:34:30.624235', '2012-10-29 02:35:06.928827', NULL, 6, 6, NULL, NULL, NULL, true, 999, 'Segun contrato de alquiler - cuota mensual', 3, 'CIEMPOZUELOS
Madrid

', NULL, 999, NULL, 'Carretillas', false, false, '[ 02-Noviembre-2012 22:24:00 ]  Jose Manuel Iáñez :   Editada hora para añadir fichero enviado por proveedor
', NULL, true, 2, '2012-11-07 19:12:19.054733', NULL, NULL, NULL, NULL, '2012-11-07 19:12:19.054733', NULL, NULL, '2012-11-02 23:55:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (86, 1, 2, 2, 'CFC022-1', 'RENTING EQUIPOS MULTIFUNCION', '2015-07-09 19:07:23.983694', '2015-10-07 14:00:00', false, true, true, false, 11, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'RENTING TECNOLOGICO (PORPOPIEDAD FINAL DEL PROVEEDOR) - 48 MESES

100 EQUIPOS (98 X HASTA 3.000 COPIAS + 2 HASTA 6.000 COPIAS)

VER FICHERO ADJUNTO &quot;REGLAS_Y_CONDICIONES.PDF&quot;

Los licitadores deberán suministrar en depósito, URGENTEMENTE, para que CARGLASS proceda a su homologación, 1 unidad del equipo ESTANDAR.

Ver en Pliego Condiciones de factura por copia / mantenimiento.

', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre características de los equipos, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

JOSE MANUEL IAÑEZ
ITBID ESOURCING SOLUTIONS
Móvil +34 676 953 333
jmianez@itbid.com
', '2015-07-09 19:00:35.064004', '2015-07-09 19:07:23.983694', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'VER PLIEGO DE CONDICIONES', 3, 'VER PLIEGO DE CONDICIONES
', NULL, 999, NULL, 'PC, Monitores y Laptops', false, false, '', NULL, true, 2, '2015-11-16 10:52:01.145312', NULL, NULL, NULL, NULL, '2015-11-16 10:52:01.145312', NULL, NULL, '2015-10-07 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, 6, false, true, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (94, 1, 2, 2, 'CFP014', 'SERVIÇO DE CÓPIA/IMPRESSÃO', '2015-12-02 17:12:43.459809', '2015-12-18 19:00:00', false, true, true, false, 11, true, '2019-12-31 00:00:00', '2016-01-01 00:00:00', NULL, NULL, NULL, false, 0, 1, 'Contrato (arrendamento ou similar) a duração de 4 anos.

Veja o arquivo anexado  &quot;Terms &amp; Conditions.PDF&quot;  (EN).

Use o &quot;Centro de Mensagens&quot; para a pergunta sobre este processo. Idiomas PT, ES ou EN', false, NULL, 'Use o &quot;Centro de Mensagens&quot; para a pergunta sobre este processo. (Idiomas PT, ES ou EN)

Em caso de dúvidas ou problemas para acessar ou gerenciar suas ofertas através do Sistema de eSourcing, entre em contato conosco entre 8:30-17:30 h (Lisboa).

JOSE MANUEL IAÑEZ
ITBID TECHNOLOGIES
Phone (+34) 676 953 333
Email: jmianez@itbid.com
', '2015-12-02 17:08:08.474859', '2015-12-02 17:12:43.459809', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'Veja Terms & Conditions', 3, 'Veja "Terms & Conditions"
', NULL, 999, NULL, 'PC, Monitores y Laptops', false, false, '', NULL, true, 2, '2016-01-20 09:04:40.171312', NULL, NULL, NULL, NULL, '2016-01-20 09:04:40.171312', NULL, NULL, '2015-12-18 19:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 4, true, true, false, 10, false, true, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (206, 1, 771, 14, '206', NULL, '2021-05-26 12:40:03', '2021-05-26 12:40:03', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2021-05-26 12:40:03', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (9, 1, 19, 6, 'ALM-02', 'ALQUILER DE CARRETILLAS', '2012-10-11 16:46:58.794823', '2012-10-19 14:00:00', false, true, true, false, 2, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'ALQUILER DE DE CARRETILLAS Y VEHICULOS PARA ALMACENAJE PARA LOS ALMACENES CARGLASS DE CIEMPOZUELOS Y MOLLET

Ver ficheros adjuntos:

* Reglas_y_condiciones.PDF
* Anexo 1 Descripcion técnica y Cotizacion.XLS
* Anexo 3 Compra Activos.XLS

Trasladar a los casilleros habilitados en la plataforma los resultados de los casilleros ROSA de los adjuntos.

SE HAN DE ADJUNTAR A LA OFERTA:

* Anexo1, cumplimentado
* Anexo 3, cumplimentado
* Folletos de los productos, con características técnicas
* Borrador de contrato tipo
* Documento explicativo de penalizaciones por resolución anticipada de contrato de alquiler (si no incluido en borrador de contrato)
* Imp Sociedades 2011

', false, 'carretillas / diversos tipos', 'En caso de dudas sobre la plataforma, entrada de
ofertas o anexar ficheros, por favor, contactar
con:

JOSE MANUEL IAÑEZ
Mov. 676 953 333
email jmianez@itbid.org

', '2012-10-11 16:46:40.628447', '2012-10-11 16:46:58.794823', NULL, 6, 6, NULL, NULL, NULL, true, 999, 'Cuota Mensual', 3, 'Ciempozuelos - Madrid
Mollet - Barcelona
', NULL, 999, NULL, 'Carretillas', false, false, '', NULL, true, 2, '2012-10-27 12:10:14.967997', NULL, NULL, NULL, NULL, '2012-10-27 12:10:14.967997', NULL, NULL, '2012-10-19 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (17, 1, 2, 2, 'CFC001-1', 'VESTUARIO LABORAL 2013 - RFQ', '2013-02-12 19:16:10.585738', '2013-02-21 14:00:00', false, false, false, true, 3, true, NULL, NULL, NULL, NULL, NULL, false, 1, 1, 'El objeto de esta negociación es determinar el proveedor y condiciones óptimas para el suministro de vestuario laboral del personal de los centros Carglass. 

VER EN ANEXOS:

* Reglas_y_condiciones_RFQ.PDF
* Anexo1._Especificaciones _y_alternativas.DOC
* Anexo2._Fichero_Cotizacion.XLSX

INSTRUCCIONES RESUMIDAS

Descargar y cumplimentar el anexo 2; copiar los 5 valores inferiores en lso casilleros de la web; adjuntar el Excel cumplimentado a la oferta de la web.

El Anexo 1 puede ser editado, si se desean cotizar prendas alternativas a las especificadas; en ese caso, tambien se debe adjuntar a la oferta.
', false, NULL, 'En caso de dudas o dificultades técnicas para la entrada de la oferta o la subida de ficheros, contactar con:

Sr. Jose Manuel Iáñez
ITbid Technologies
e-mail: jmianez@itbid.org
Móvil 676 953 333

', '2013-02-12 19:15:14.216484', '2013-02-12 19:16:10.585738', '2013-03-14 11:01:26.409399', 2, 2, 2, NULL, NULL, true, 999, 'A SER NEGOCIADA', 1, 'VER INDICADO EN EL PLIEGO
', 'EUR', 0, 'DDP - Ciempozuelos', 'Vestuario laboral y textiles', false, false, '[ 12-Febrero-2013 19:21:02 ]  Jose Manuel Iáñez :   Añadido Uniformat
[ 15-Febrero-2013 15:51:52 ]  Jose Manuel Iáñez :   Añadido FDS
', NULL, false, 2, '2013-03-14 11:01:26.409399', NULL, NULL, NULL, NULL, '2013-03-14 11:01:26.409399', NULL, NULL, '2013-02-21 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'simple', 'desierta', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (71, 1, 2, 2, 'CFC024', 'Suministro energía electrica 2015-2017', '2015-05-25 11:01:19.640379', '2015-06-04 14:00:00', false, true, true, false, 6, true, '2017-08-31 00:00:00', '2015-08-01 00:00:00', NULL, NULL, NULL, false, 2, 1, 'Suministro de energía eléctrica en baja tensión hasta Agosto 2017, para los centros y Hubs de CARGLASS que se detallan en el Anexo 1- Excel de Cotización - 

1.114 MWh anuales, aprox - 175 Contratos afectando a 170 centros.

DESCARGAR FICHEROS ADJUNTOS:

* Reglas_y_Condiciones_RFQ_CFC024.PDF --&gt; Pliego condiciones de esta licitación
* Anexo1_Excel_Cotizacion.XLSX  --&gt; datos de los centros y consumos, A SER CUMPLIMENTADO Y DEVUELTO EN LA PLATAFORMA
* Anexo2_Consumos_13_15.XLSX: datos mensualizados de los consumos por Centro

SE DEBEN ANEXAR A LA OFERTA OBLIGATORIAMENTE LOS SIGUIENTES FICHEROS:

* El Excel de Cotización completado 
* Oferta técnica y económica completa, incluyendo mejoras

Opcionalmente, los lictadores podrán incluir cuantos ficheros deseen.
', false, NULL, 'Para dudas sobre la licitación, usen el &quot;Centro de Mensajes&quot;.

Si algún licitador experimentara dificultades técnicas para la entrada de la oferta o la subida de ficheros, deberá contactar con:

Sr. Jose Manuel Iáñez
ITbid Technologies
e-mail: jmianez@itbid.org
Móv. 676 953 333
Tel. 933 218 614
', '2015-05-25 11:00:19.998457', '2015-05-25 11:01:19.640379', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'Ver pliego', 1, 'N/A
', 'EUR', 999, 'N/A', 'Electricidad', false, false, '', NULL, true, 2, '2015-06-18 18:55:35.245135', NULL, NULL, NULL, NULL, '2015-06-18 18:55:35.245135', NULL, NULL, '2015-06-04 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, 7, false, true, true, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (15, 1, 19, 6, 'S0918', 'ALQUILER DE CARRETILLAS - MEJORA', '2012-11-09 12:00:00', '2012-11-09 12:30:00', false, true, true, false, 2, true, NULL, NULL, NULL, NULL, true, false, 1, 1, 'Fase final, de mejora a partir de las ofertas introducidas en la fase precedente.

VER EN FICHEROS ANEXOS:

* Reglas de la Negociación
* Guía rápida de uso del Proveedor

La duración mínima de esta subasta es de 15 min, con un máximo de 30 min.

Las ofertas iniciales han sido introducidas, y están bloqueadas hasta el inicio de la subasta.', false, NULL, 'En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

SR. D. JOSE MANUEL IÁÑEZ
ITBID TECHNOLOGIES
Móvil 676 953 333
jmianez@itbid.org
', '2012-11-08 09:48:31.352588', '2012-11-08 09:51:09.126795', NULL, 6, 6, NULL, NULL, NULL, true, 999, 'Segun contrato de alquiler - cuota mensual', 22, 'Ciempozuelos - Madrid
', 'EUR', 0, 'DDP - Ciempozuelos', 'Carretillas', false, false, '', NULL, true, 2, '2013-02-12 19:00:47.771355', NULL, NULL, 0.25, 8, '2013-02-12 19:00:47.771355', NULL, 1, '2012-11-09 12:30:00', 15, NULL, NULL, 4, 2, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (122, 1, 2, 2, 'CFC-030', 'BUSINESS EXTENSION - CARGLASS SPAIN', '2017-09-18 15:40:53', '2017-09-25 22:00:00', false, true, true, false, 21, true, NULL, NULL, 'All documents and actual business induction will be deliver to the selected companies. The B2B interviews will be arranged by CARGLASS®.See Business_Extension_Briefing.pdf attached.', NULL, true, false, 2, 1, 'The purpose of this request is the selection of a partner / company who will support CARGLASS® SPAIN and BELRON® (from now on the ”Customer”) on their global investment strategy within the spanish market, including but not limited to the evaluation of the business potential and to assist on the development of the right strategy and optimal operating model for his business extension.

Please , read carrefully attached Briefing and RFP Terms.', false, NULL, NULL, '2017-09-15 16:16:00', '2017-09-18 15:40:53', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 3, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 999, NULL, 'Consultoría', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-23 16:19:24', NULL, NULL, '2017-09-29 12:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, 15, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (207, 1, 771, 14, 'CFC045', 'SERVICIO DE LIMPIEZA 2021', '2021-05-26 17:28:41', '2021-06-10 16:00:00', false, true, true, false, 19, true, NULL, NULL, NULL, NULL, NULL, false, 1, 1, '<p>El objeto de esta negociaci&oacute;n es determinar los proveedores y condiciones &oacute;ptimas para la prestaci&oacute;n de diversos servicios de LIMPIEZA para los diferentes centros de CARGLASS por el periodo de 2 a&ntilde;os, siendo de obligatorio cumplimiento solamente el primero a&ntilde;o del contrato y que incluya:</p>

<ul>
	<li>Servicios de limpieza convencional (pre-COVID19).</li>
	<li>Servicios de limpieza e higienizaci&oacute;n frente al COVID19.</li>
	<li>Servicios de limpieza de cristales y r&oacute;tulos (trimestral).</li>
	<li>Desinsectaci&oacute;n, Desinfecci&oacute;n y Desratizaci&oacute;n (DDD).</li>
	<li>Productos de limpieza e higienizaci&oacute;n frente al COVID19.</li>
	<li>Utilizaci&oacute;n de plataforma elevadora para limpieza en altura (cristales y r&oacute;tulos).</li>
	<li>Barredora mec&aacute;nica.</li>
	<li>Limpieza fin de obra con fregado mec&aacute;nico (M2).</li>
</ul>

<p><span style="text-autospace:none">CARGLASS garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociaci&oacute;n y concurrencia de ofertas en igualdad de condiciones.</span></p>

<p><strong>El proceso para la selecci&oacute;n del proveedor, y las fechas del mismo, es el siguiente:</strong></p>

<div align="center">
<table class="Table" style="border-collapse:collapse; border:none">
	<thead>
		<tr>
			<td style="width:346.35pt; border:solid #999999 1.5pt; background:#e6e6e6" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>ACTIVIDAD</b></p>
			</td>
			<td style="width:118.85pt; border:solid #999999 1.5pt; border-left:none; background:#e6e6e6" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>FECHA PREVISTA</b></p>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Env&iacute;o de la petici&oacute;n de oferta a los licitadores</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>26 Mayo 2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Plazo l&iacute;mite para visitar centros</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>07 Junio 2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Plazo l&iacute;mite para la recepci&oacute;n de consultas de los licitadores</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>07 Junio 2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><strong><span style="tab-stops:67.5pt">Plazo l&iacute;mite para la Recepci&oacute;n de ofertas in&iacute;ciales</span></strong></p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><u><strong>16:00h 10 Junio 2021</strong></u></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Revisi&oacute;n, valoraci&oacute;n y an&aacute;lisis ofertas</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>16 Junio 2021 </b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Convocatoria de licitadores pre-finalistas para defensa de su propuesta</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>21 Junio 2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Comunicaci&oacute;n de licitadores finalistas</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>23 Junio 2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Posible ronda final de negociaci&oacute;n</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>28 Junio 2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Confirmaci&oacute;n de la adjudicaci&oacute;n/firma del contrato</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>05 Julio al 09 Julio 2021 </b></p>
			</td>
		</tr>
	</tbody>
</table>
</div>', false, NULL, NULL, '2021-05-26 12:53:58', '2021-05-26 17:28:41', NULL, 14, 14, NULL, NULL, NULL, true, 999, NULL, 1, 'Centros Carglass en España.', NULL, 999, 'str_otras_condiciones_envio', 'C04. Housekeeping', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-21 17:06:02', NULL, NULL, '2021-06-10 16:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (65, 1, 2, 2, 'FCP006-P', 'FORNECIMENTO ELETRICIDADE 2015-2016', '2014-12-01 11:06:22.072437', '2014-12-11 14:00:00', false, false, false, true, 6, true, '2016-12-31 00:00:00', '2015-01-01 00:00:00', NULL, NULL, NULL, false, 2, 1, 'O objeto da negociação é determinar as condições ideais para comercialização e fornecimento de energia eléctrica em baixa tensão para os centros de AUTOGLASS - VIDROS DE VIATURAS LDA (CARGLASS) enumerados no anexo 1 Excel_Cotação.XLSX, com uma estimativa 497.000 kWh / ano.

ANEXOS:

TERMOS_E_REGRAS_RFQ_FCP006.PDF
ANEXO_1_EXCE-_COTAÇAO.XLS

A apresentação das propostas será EXCLUSIVAMENTE através da plataforma electrónica preparado para esse efeito, o endereço na internet (URL) https://carglass.itbid.org.

O proponente deve apresentar sua proposta, completando as caixas para o efeito do website negociação, e anexando concluída  Anexo 1 – Excel_Cotação.XLSX.

', false, NULL, 'Dúvidas sobre o conteúdo e alcance deste documento e/ou o processo de negociação deve geralmente ser canalizada exclusivamente através do “Centro de Mensagens” da plataforma ITbid. 

Qualquer concorrente tem dificuldades técnicas para a entrada da oferta ou upload de arquivos, você deve entrar em contato com:

Sr. Jose Manuel Iáñez
ITbid Technologies
e-mail: jmianez@itbid.org
Móv. (+34) 676 953 333
Tel. (+34) 933 218 614
', '2014-12-01 11:05:35.036722', '2014-12-01 11:06:22.072437', '2014-12-22 11:44:00.80474', 2, 2, 2, NULL, NULL, true, 999, 'VER TERMOS E REGRAS', 1, 'N/A
', 'EUR', 999, 'N/A', 'Electricidad', false, false, '[ 09-Diciembre-2014 14:05:13 ]  Equipo ITbid :   AUMENTO PLAZO 12 NOCHE
[ 10-Diciembre-2014 13:15:19 ]  Equipo ITbid :   mod fecha cierre - ampliacion plazo
', NULL, false, 2, '2014-12-22 11:44:00.80474', NULL, NULL, NULL, NULL, '2014-12-22 11:44:00.80474', NULL, NULL, '2014-12-11 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 4, false, false, false, 5, true, true, false, false, false, false, 'simple', 'desierta', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (126, 1, 2, 2, 'CF04-05-1', 'SERVICIO DE DISTRIBUCIÓN  CLIENTES EXTERNOS Y FURGONETAS CF04-05-1', '2017-10-10 21:41:08', '2017-10-30 14:00:00', false, true, true, false, 9, true, '2019-12-31 00:00:00', '2018-01-01 00:00:00', NULL, NULL, NULL, false, 0, 1, 'EEl objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación del servicio de TRANSPORTE Y DISTRIBUCIÓN DE PRODUCTOS Y ACCESORIOS DESDE LOS ALMACENES DE CARGLASS A SUS HUB’S, CENTROS Y CLIENTES EXTERNOS, a partir del día 1 de enero de 2018 hasta el 31 de diciembre de 2019.

El material a trasladar, será material propiedad de CARGLASS y su destino será  sus propios almacenes, centros y sus clientes externos.

CARGLASS garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociación y concurrencia de ofertas en igualdad de condiciones

Vean en Ficheros Adjuntos:

•	CLIENTES EXTERNOS: ANEXO III: origen Ciempozuelos, Mollet y Xirivella.
•	REPARTO EN ZONA HUB CON FURGONETAS: ANEXO VI.
•	TABLA CON LAS DIRECCIONES DE LOS CENTROS CARGLASS: ANEXO VII.

Los Sres Licitadores pueden seleccionar presentar su propuesta para uno o varios de tipos de servicio solicitados.

Para cumplimentar una propuesta:

* Rellene los casilleros de la propia web
* Adjunte cumplimentado los diferentes Anexos, según corresponda
* Adjunte el resto de documentos e información solicitados', false, NULL, 'En caso de dudas sobre las condiciones del servicio, los Srs. Proveedores deben enviarlas a través del “Centro de Mensajes” de la propia negociación electrónica.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

Josep Puig

Móvil 655347607
Telf.: 933 218 614
jpuig@itbid.com

Jose Manuel Iañez

Móvil 676953333
Telf.: 933 218 614
jpuig@itbid.com


Soporte Técnico - itbid

Telf.: 933 218 614
soporte@itbid.org', '2017-10-10 21:19:59', '2017-10-10 21:41:08', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 1, 'NO APLICA', NULL, 0, NULL, 'Logistica', false, false, '[ 11-Julio-2013 13:22:25 ]  Joan Castella :   Añadidos 9 proveedores - MAIL2
[ 15-Julio-2013 10:05:37 ]  Joan Castella :   Añadidos 2 proveedores
[ 15-Julio-2013 10:22:02 ]  Jose Manuel Iáñez :   mail3
[ 15-Julio-2013 11:07:02 ]  Jose Manuel Iáñez :   mail3
[ 18-Julio-2013 16:48:42 ]  Jose Manuel Iáñez :   Añadido Truck 
[ 18-Julio-2013 19:28:42 ]  Jose Manuel Iáñez :   añadido DHL
[ 02-Agosto-2013 11:38:32 ]  Jose Manuel Iáñez :   Añadido Barbera
', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-06 17:10:25', NULL, NULL, '2013-09-02 23:55:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, 13, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (128, 1, 2, 2, '128', NULL, '2017-10-24 10:23:37', '2017-10-24 10:23:37', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2017-10-24 10:23:37', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, 'ponderado', true, 3, 3, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (133, 1, 2, 2, '133', NULL, '2018-01-24 18:36:16', '2018-01-24 18:36:16', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2018-01-24 18:36:16', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (134, 1, 2, 2, '134', NULL, '2018-02-19 10:44:41', '2018-02-19 10:44:41', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2018-02-19 10:44:41', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (135, 1, 2, 2, '135', NULL, '2018-02-21 11:38:29', '2018-02-21 11:38:29', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2018-02-21 11:38:29', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'simple', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (46, 1, 2, 2, 'CFC014', 'SERVICIOS DE FORMACION Y DESARROLLO DEL PERSONAL', '2014-03-17 16:43:13.228347', '2014-04-08 14:00:00', false, true, true, false, 14, true, '2016-06-30 00:00:00', '2014-07-01 00:00:00', NULL, NULL, NULL, false, 0, 1, 'Proceso de selección del proveedor (o proveedores) idóneo que actue como partner-colaborador en el diseño del plan formativo y en la prestación de los servicios de formación y desarrollo del personal de Carglass en España, para el periodo Julio 2014 – Junio 2016 (2 años).

VER FICHEROS ADJUNTOS:
*Pliego_Condiciones_formacion_CFC014.PDF --> Pliego de condiciones
Programa_Formativo.XLSX --> Excel a ser descargado, cumplimentado y devuelto a la plataforma.

El alcance de los servicios requeridos es:

1.	Colaboración con los responsables de Carglass en la definición del plan formativo, de acuerdo con las necesidades y disponibilidades de Carglass
2.	Asesoramiento en las propuestas didácticas, objetivos y contenidos de cada curso
3.	Prestación de la formación a los asistentes, tanto para cursos presenciales como online. 
4.	Medición de la calidad del curso y satisfacción de los asistentes
5.	El asesoramiento, sin tramitación, en cuantas ayudas y subvenciones sean aplicables a la formación impartida
', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre características de los equipos, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

JOSE MANUEL IAÑEZ 
ITbid Technologies
Telf: 933 218 614
Móvil 676 953 333
jmianez@itbid.org

', '2014-03-17 16:41:51.262651', '2014-03-17 16:43:13.228347', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'A SER DETERMINADA', 3, 'NO PROCEDE - VER PLIEGO
', NULL, 999, NULL, 'Formacion', false, false, '[ 17-Marzo-2014 20:14:36 ]  Equipo ITbid :   Añadidos 7 proveedores
[ 18-Marzo-2014 19:50:00 ]  Equipo ITbid :   Añadidos 3 proveedores
[ 08-Abril-2014 11:57:10 ]  Equipo ITbid :   tea cegos
', NULL, true, 2, '2014-04-14 11:29:44.662639', NULL, NULL, NULL, NULL, '2014-04-14 11:29:44.662639', NULL, NULL, '2014-04-08 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (129, 1, 2, 2, 'CFC032', 'SERVICIOS DE CALL CENTER', '2017-11-10 19:43:07', '2017-11-10 19:43:00', false, false, false, false, 16, false, NULL, NULL, NULL, NULL, false, false, 1, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación de los SERVICIOS DE CALL CENTER DESBORDAMIENTO, FIN DE SEMANA, NOCTURNO Y FESTIVOS para CARGLASS durante el periodo de 1 enero 2018 a 31 diciembre 2020 (36 meses).', false, NULL, NULL, '2017-11-10 19:43:07', NULL, NULL, 2, 2, NULL, NULL, NULL, false, 0, NULL, 3, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 0, NULL, 'Alquiler Turismos - Car Rental', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-10 19:43:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (115, 1, 2, 2, '115', NULL, '2017-05-18 18:16:08', '2017-05-18 18:16:08', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2017-05-18 18:16:08', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (119, 1, 2, 2, 'CFC007-2-P', 'Suministro energía eléctrica 2017-2019', '2017-06-30 09:16:21', '2017-07-14 14:00:00', false, true, true, false, 6, true, '2019-07-31 00:00:00', '2017-09-01 00:00:00', NULL, NULL, NULL, false, 2, 1, 'El objeto de esta negociación es determinar las comercializadoras y condiciones óptimas para el suministro de energía eléctrica en baja tensión para los centros y Hubs de CARGLASS BV SUCURSAL EN ESPAÑA que se detallan en el Excel de Cotización, por un consumo anual estimado de 4,5 GWH.

DESCARGAR FICHEROS ADJUNTOS:

* Pliego_condiciones_RFQ_CFC007-2.pdf:  Pliego condiciones de esta licitación
* Excel_cotizacion_2017.xlsx: datos de los centros y consumos, A SER CUMPLIMENTADO Y DEVUELTO EN LA PLATAFORMA


SE DEBEN ANEXAR A LA OFERTA OBLIGATORIAMENTE LOS SIGUIENTES FICHEROS:

* El Excel de Cotización completado 
* Oferta técnica y económica completa, incluyendo mejoras si las proponen

Opcionalmente, los licitadores podrán incluir cuantos ficheros deseen.', false, NULL, 'Las dudas sobre los contenidos o alcance de este pliego y/o sobre el proceso de negociación en general, deberán canalizarse exclusivamente a través de plataforma ITBID.

Si algún licitador experimentara dificultades técnicas para el acceso a la plataforma, la entrada de la oferta o la subida de ficheros, deberá contactar con:

Maricarmen Luzon
ITBID Technologies
e-mail: mcluzon@itbid.com
Móv. 636 10 93 45
Tel. 933 218 614', '2017-06-29 11:15:39', '2017-06-30 09:16:21', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'Ver pliego', 1, 'N/A', NULL, 999, 'N/A', 'Electricidad', false, false, '', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-24 09:58:25', NULL, NULL, '2015-06-04 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (118, 1, 2, 2, 'CFC02-4 TRANSPORTE NACIONAL', 'SERVICIO DE TRANSPORTE Y DISTRIBUCIÓN NACIONAL', '2017-06-09 13:14:13', '2017-07-03 21:00:00', false, true, true, false, 9, true, '2017-09-01 00:00:00', '2017-06-12 00:00:00', NULL, NULL, NULL, false, 0, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación del servicio de TRANSPORTE Y DISTRIBUCIÓN DE LOS ALMACENES DE CARGLASS A SUS HUB’S, CENTROS Y CLIENTES EXTERNOS, a partir del día 1 de Septiembre de 2017 hasta el 31 de Agosto de 2019.

El material a trasladar, será material propiedad de CARGLASS y su destino será  sus propios almacenes y centros, o sus clientes externos.

Vean en Ficheros Adjuntos:

•	ENTREGA PROGRAMADA A CENTROS CONVENCIONALES: ANEXO I: 220 Centros entrega programada, origen Ciempozuelos, Mollet, destino talleres Carglass.
•	REPARTO HUBS ORIGEN CIEMPOZUELOS ANEXO II: destino talleres Zona Hub, entrega antes 9:00h. Y entregas puntuales palet promociones origen Ciempozuelos entregas talleres.
•	CLIENTES EXTERNOS: ANEXO III: origen Ciempozuelos, Mollet y Xirivella.
•	ENVÍOS CANARIAS: ANEXO IV.
•	ENTREGAS LARGA DISTANCIA ENTRE HUBS: ANEXO V
•	REPARTO EN ZONA HUB CON FURGONETAS: ANEXO VI.
•	TABLA CON LAS DIRECCIONES DE LOS CENTROS CARGLASS: ANEXO VII.

Los Sres Licitadores pueden seleccionar presentar su propuesta para uno o varios de tipos de servicio solicitados.

Para cumplimentar una propuesta:

* Rellene los casilleros de la propia web
* Adjunte cumplimentado los diferentes Anexos, según corresponda
* Adjunte el resto de documentos e información solicitados', false, NULL, 'En caso de dudas sobre las condiciones del servicio, los Srs. Proveedores deben enviarlas a través del “Centro de Mensajes” de la propia negociación electrónica.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:


Maricarmen Luzon

Móvil 636 10 93 45
Telf.: 933 218 614
mcluzon@itbid.org', '2017-06-09 13:14:13', '2017-06-12 16:09:40', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 1, 'NO APLICA', NULL, 0, NULL, 'Logistica', false, false, '[ 11-Julio-2013 13:22:25 ]  Joan Castella :   Añadidos 9 proveedores - MAIL2
[ 15-Julio-2013 10:05:37 ]  Joan Castella :   Añadidos 2 proveedores
[ 15-Julio-2013 10:22:02 ]  Jose Manuel Iáñez :   mail3
[ 15-Julio-2013 11:07:02 ]  Jose Manuel Iáñez :   mail3
[ 18-Julio-2013 16:48:42 ]  Jose Manuel Iáñez :   Añadido Truck 
[ 18-Julio-2013 19:28:42 ]  Jose Manuel Iáñez :   añadido DHL
[ 02-Agosto-2013 11:38:32 ]  Jose Manuel Iáñez :   Añadido Barbera
', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-10 10:33:19', NULL, NULL, '2013-09-02 23:55:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, 13, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (124, 1, 2, 2, 'CFC-030-02', 'BUSINESS EXTENSION - CARGLASS SPAIN - 02', '2017-10-10 10:00:00', '2017-10-16 14:00:00', false, true, true, false, 21, true, NULL, NULL, 'All documents and actual business induction will be deliver to the selected companies. The B2B interviews will be arranged by CARGLASS®.See Business_Extension_Briefing.pdf  and additional requirements document attached.', NULL, true, false, 2, 1, 'The purpose of this request is the selection of a partner / company who will support CARGLASS® SPAIN and BELRON® (from now on the ”Customer”) on their global investment strategy within the Spanish market, including but not limited to the evaluation of the business potential and to assist on the development of the right strategy and optimal operating model for his business extension.

In this 2ond  phase, you have been selected to provide your definitive proposal trough an Seled bid auction process . Please , read carrefully attached Briefing, SBA Terms and the additional REQUIREMENTS attached.', false, NULL, NULL, '2017-10-09 16:21:29', '2017-10-10 09:51:54', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 5, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 0, NULL, 'Consultoría', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-23 16:31:05', NULL, NULL, '2017-09-29 12:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, 15, false, false, true, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (28, 1, 49, 7, 'CFC-AH003S', 'COMPRA DE SUMINISTROS INDUSTRIALES 2013', '2013-06-28 11:00:00', '2013-06-28 11:45:00', false, true, true, false, 7, true, NULL, NULL, NULL, NULL, true, false, 1, 1, 'Fase de Mejora, mediante SUBASTA DE RANKING.

Resumen:
suministros industriales para hacer frente a las
necesidades de los distintos centros de la firma
CARGLASS B.V Surcursal en España.

VER FICHEROS ADJUNTOS:

*Reglas y Condiciodes (PDF) --> normas de esta
negociación

* Guía rápida de uso del Proveedor

La duración mínima de esta subasta es de 20 min,
con extensiones ilimitadas por cambio 1a posición.

La posición de inicio de subasta ha sido establecida en función de las ofertas iniciales obtenidas en fase RFQ.

', false, NULL, NULL, '2013-06-26 18:17:54.995594', '2013-06-26 18:18:57.555939', NULL, 7, 7, NULL, NULL, NULL, true, 0, '30 días Transferencia', 22, 'Moreras , P-2, Nave 1
Pol. Ind. Camporrosp
Ciempozuelos (MADRID)
SPAIN
', 'EUR', 0, 'DDP - Ciempozuelos', 'Suministros Industriales', false, false, '[ 24--2013 09:49:58 ]  Jose Manuel Iáñez :   Se Adjudica a ESIN . Reunion con ESIN en Carglass 12-07-13. Cierre precios definitivos 22-07-13 - ver informe final
', NULL, true, 2, '2013-07-24 09:53:25.867278', NULL, NULL, 0.25, 8, '2013-07-24 09:53:25.867278', NULL, 0, '2013-06-28 11:20:00', 20, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 1, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (208, 1, 771, 14, 'CFC045 - Fase 2', 'SERVICIO DE LIMPIEZA 2021 - FASE 2', '2021-06-29 13:10:00', '2021-07-05 16:00:00', false, true, true, false, 19, true, NULL, NULL, NULL, NULL, false, false, 1, 1, '<p>El objeto de esta negociaci&oacute;n es determinar los proveedores y condiciones &oacute;ptimas para la prestaci&oacute;n de diversos servicios de LIMPIEZA para los diferentes centros de CARGLASS por el periodo de 2 a&ntilde;os, siendo de obligatorio cumplimiento solamente el primero a&ntilde;o del contrato y que incluya:</p>

<ul>
	<li>Servicios de limpieza convencional (pre-COVID19).</li>
	<li>Servicios de limpieza e higienizaci&oacute;n frente al COVID19.</li>
	<li>Servicios de limpieza de cristales y r&oacute;tulos (trimestral).</li>
	<li>Desinsectaci&oacute;n, Desinfecci&oacute;n y Desratizaci&oacute;n (DDD).</li>
	<li>Productos de limpieza e higienizaci&oacute;n frente al COVID19.</li>
	<li>Utilizaci&oacute;n de plataforma elevadora para limpieza en altura (cristales y r&oacute;tulos).</li>
	<li>Barredora mec&aacute;nica.</li>
	<li>Limpieza fin de obra con fregado mec&aacute;nico (M2).</li>
</ul>

<p><span style="text-autospace:none">El presente pliego regula las normas que regir&aacute;n esta fase de <b>mejora de ofertas</b>, fase 2 de licitaci&oacute;n de sobre cerrado, perfeccionando y complementando el Pliego de Condiciones de la fase de petici&oacute;n de ofertas precedente, pero no lo sustituye ni lo anula.</span></p>

<p><span style="text-autospace:none">En esta modalidad de &ldquo;<b>Sobre Cerrado</b>&rdquo;, los licitadores deber&aacute;n presentar su oferta exclusivamente en la ventana de tiempo asignada. Dada la tipolog&iacute;a de &ldquo;Sobre Cerrado&rdquo; ning&uacute;n comprador de CARGLASS tiene acceso en ning&uacute;n caso a las ofertas, hasta finalizado el plazo de presentaci&oacute;n de ofertas por parte de todos los proveedores y cierre del periodo de oferta.</span></p>

<p><span style="text-autospace:none">CARGLASS garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociaci&oacute;n y concurrencia de ofertas en igualdad de condiciones.</span></p>

<p><strong>El proceso para la selecci&oacute;n del proveedor, y las fechas del mismo, es el siguiente:</strong></p>

<p>&nbsp;</p>

<div align="center">
<table class="Table" style="border-collapse:collapse; border:none">
	<thead>
		<tr>
			<td style="width:312.5pt; border:solid #999999 1.5pt; background:#e6e6e6" width="417">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>ACTIVIDAD</b></p>
			</td>
			<td style="width:111.2pt; border:solid #999999 1.5pt; border-left:none; background:#e6e6e6" width="148">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>FECHA PREVISTA</b></p>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width:312.5pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="417">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Env&iacute;o de la petici&oacute;n de oferta a los licitadores</p>
			</td>
			<td style="width:111.2pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="148">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>29 Junio 2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:312.5pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="417">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Plazo l&iacute;mite para la recepci&oacute;n de consultas de los licitadores</p>
			</td>
			<td style="width:111.2pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="148">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>01 Julio 2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:312.5pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="417">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><span style="tab-stops:67.5pt"><b>Plazo l&iacute;mite para la Recepci&oacute;n de ofertas in&iacute;ciales</b></span></p>
			</td>
			<td style="width:111.2pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt; background:#92d050" width="148">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>16:00h 05 Julio 2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:312.5pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="417">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Revisi&oacute;n, valoraci&oacute;n y an&aacute;lisis ofertas</p>
			</td>
			<td style="width:111.2pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="148">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>05 Julio al 09 Julio 2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:312.5pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.0pt" width="417">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Confirmaci&oacute;n de la adjudicaci&oacute;n/firma del contrato</p>
			</td>
			<td style="width:111.2pt; border-top:none; border-left:none; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.5pt" width="148">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>12 Julio al 14 Julio 2021 </b></p>
			</td>
		</tr>
	</tbody>
</table>
</div>', false, NULL, NULL, '2021-06-29 11:22:50', '2021-06-29 12:58:56', NULL, 14, 14, NULL, NULL, NULL, true, 999, 'Ver pliego.', 5, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, NULL, NULL, 'C04. Housekeeping', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-21 17:27:16', NULL, NULL, '2021-07-05 16:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, true, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (99, 1, 2, 2, 'CFC030P', 'LICITACION SERVICIO DE LIMPIEZA HUBS Y RED DE CENTROS', '2016-07-11 19:57:55', '2016-07-26 15:00:00', false, true, true, false, 19, true, NULL, NULL, NULL, NULL, false, false, 1, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación de diversos servicios de LIMPIEZA de los diferentes Hubs y Red de Centros de Carglass para el periodo  Agosto de 2016 a de Agosto de 2018 (24 meses).

Ver adjunto PLIEGO DE CONDICIONES.

Para cotizar, se debe descargar, cumplimentar y devolver el EXCEL DE COTIZACION.', false, NULL, 'En caso de dudas sobre las condiciones del servicio o este pliego, los Sres. Proveedores deben entrarlas a través del “Centro de Mensajes” de la propia negociación electrónica. Las dudas serán atendidas a la mayor brevedad posible.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa al uso de la plataforma de negociación ITBID, por favor contactar con:

Maricarmen Luzon
Móvil 636 109 345
Telf. 936 345 009
mcluzon@itbid.com', '2016-07-11 19:57:55', '2016-07-11 20:13:13', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 3, 'No aplica - ver pliego', NULL, 0, NULL, 'Limpieza', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2016-08-03 17:37:10', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, 12, true, true, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (100, 1, 2, 2, '100', 'CFC030 LICITACION SERVICIO DE LIMPIEZA HUBS Y RED DE CENTROS (SOBRE CERRADO)', '2016-08-05 18:00:00', '2016-09-01 13:10:00', false, false, false, false, 19, false, '2017-09-01 00:00:00', '2016-08-05 00:00:00', NULL, NULL, false, false, 1, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación de diversos servicios de LIMPIEZA de los diferentes Hubs y Red de Centros de Carglass para el periodo  Agosto de 2016 a de Agosto de 2018 (24 meses).

Ver adjunto PLIEGO DE CONDICIONES.

Para cotizar, se debe descargar, cumplimentar y devolver el EXCEL DE COTIZACION.', false, NULL, 'En caso de dudas sobre las condiciones del servicio o este pliego, los Sres. Proveedores deben entrarlas a través del “Centro de Mensajes” de la propia negociación electrónica. Las dudas serán atendidas a la mayor brevedad posible.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa al uso de la plataforma de negociación ITBID, por favor contactar con:

Maricarmen Luzon
Móvil 636 109 345
Telf. 936 345 009
mcluzon@itbid.com', '2016-08-05 13:10:03', '2016-07-11 20:13:13', NULL, 2, 2, NULL, NULL, NULL, false, 0, NULL, 5, 'No aplica - ver pliego', NULL, 0, NULL, 'Limpieza', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, 12, true, true, false, false, false, false, 'simple', 'nueva', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (107, 1, 2, 2, 'CFC010', 'SERVICIO MENSAJERÍA 2016-2017', '2016-11-29 12:13:13', '2016-11-29 12:13:00', false, false, false, false, 9, false, NULL, NULL, NULL, NULL, NULL, false, 1, 1, 'Licitación para la selección del proveedor de servicios de mensajería para hacer frente a las necesidades  de la firma CARGLASS B.V Surcursal en España.

VER FICHEROS ADJUNTOS:

Pliego Licitación Servicios de Mensajería (PDF) --> normas de esta negociación
Excel de Cotizacion (xls)

****', false, NULL, 'Si algún licitador experimentara dificultades técnicas para la entrada de la oferta o la subida de ficheros, deberá contactar con:

Sr. Josep Puig o David Amigo
ITbid Technologies
e-mail: jpuig@itbid.org / damigo@itbid.org
Telef: 933218614', '2016-11-29 12:13:13', '2013-07-24 22:51:25.963414', NULL, 2, 2, NULL, NULL, NULL, false, 0, '30 días Transferencia', 1, 'FACUNDO BACARDI I MASSO 7- 11
08100 MOLLET DEL VALLES
(Barcelona)
SPAIN', NULL, 0, 'Segun Servicio y Tipo entrega.', 'Logistica', false, false, '', NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-07-30 11:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (127, 1, 2, 2, 'CF004-5-3', 'SERVICIO DE TRANSPORTE Y DISTRIBUCIÓN DE LOS ALMACENES DE CARGLASS A CANARIAS CF004-5-3', '2017-10-10 22:01:17', '2017-10-30 14:00:00', false, true, true, false, 9, true, '2019-12-31 00:00:00', '2018-01-01 00:00:00', NULL, NULL, NULL, false, 0, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación del servicio de TRANSPORTE Y DISTRIBUCIÓN DE LOS ALMACENES DE CARGLASS A SUS CENTROS Y CLIENTES EXTERNOS EN CANARIAS, a partir del día 1 de Enero de 2018 hasta el 31 de Diciembre de 2019.

El material a trasladar, será material propiedad de CARGLASS y su destino será  sus propios almacenes y centros, o sus clientes externos para Canarias.

Vean en Ficheros Adjuntos:

•	ENVÍOS CANARIAS: ANEXO IV.
•	TABLA CON LAS DIRECCIONES DE LOS CENTROS CARGLASS: ANEXO VII.

Los Sres Licitadores pueden seleccionar presentar su propuesta para uno o varios de tipos de servicio solicitados.

Para cumplimentar una propuesta:

* Rellene los casilleros de la propia web
* Adjunte cumplimentado los diferentes Anexos, según corresponda
* Adjunte el resto de documentos e información solicitados', false, NULL, 'En caso de dudas sobre las condiciones del servicio, los Srs. Proveedores deben enviarlas a través del “Centro de Mensajes” de la propia negociación electrónica.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

Josep Puig

Móvil 655347607
Telf.: 933 218 614
jpuig@itbid.com

Jose Manuel Iañez

Móvil 676953333
Telf.: 933 218 614
jpuig@itbid.com


Soporte Técnico - itbid

Telf.: 933 218 614
soporte@itbid.org', '2017-10-10 21:42:08', '2017-10-10 22:01:17', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 1, 'NO APLICA', NULL, 0, NULL, 'Logistica', false, false, '[ 11-Julio-2013 13:22:25 ]  Joan Castella :   Añadidos 9 proveedores - MAIL2
[ 15-Julio-2013 10:05:37 ]  Joan Castella :   Añadidos 2 proveedores
[ 15-Julio-2013 10:22:02 ]  Jose Manuel Iáñez :   mail3
[ 15-Julio-2013 11:07:02 ]  Jose Manuel Iáñez :   mail3
[ 18-Julio-2013 16:48:42 ]  Jose Manuel Iáñez :   Añadido Truck 
[ 18-Julio-2013 19:28:42 ]  Jose Manuel Iáñez :   añadido DHL
[ 02-Agosto-2013 11:38:32 ]  Jose Manuel Iáñez :   Añadido Barbera
', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-26 09:34:27', NULL, NULL, '2013-09-02 23:55:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, 13, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (108, 1, 2, 2, 'CFC010', 'CFC010 Servicios de Mensajería', '2016-11-29 13:14:00', '2016-12-07 14:00:00', false, true, true, false, 10, true, '2018-12-01 00:00:00', '2018-11-30 00:00:00', NULL, NULL, NULL, false, 0, 1, 'LICITACIÓN PARA LA SELECCIÓN DEL PROVEEDOR DEL SERVICIO DE MENSAJERIA 

VER FICHEROS ADJUNTOS:

* Pliego Licitación Servicios de Mensajería. pdf
*Excel Cotización. xlsx 
LEER DETENIDAMENTE EL PLIEGO DE CONDICIONES ADJUNTO.
Las ofertas deben incluir, como archivos anexos:
* Documento con su oferta técnica y económica completa', false, NULL, 'En caso de dudas sobre las condiciones o servicios de esta licitación, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

Maricarmen Luzon
Project Manager 
mcluzon@itbid.com
636 10 93 45', '2016-11-29 13:14:00', '2016-11-29 17:51:07', NULL, 2, 2, NULL, NULL, NULL, true, 0, 'TBC', 3, 'N/A', NULL, 0, NULL, 'Mensajeria', false, false, '[ 16-Septiembre-2014 15:53:19 ]  Equipo ITbid :   añadido Sixt
', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2016-12-30 12:10:28', NULL, NULL, '2014-09-22 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, true, true, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (109, 1, 2, 2, 'CFC017- Alquiler vehiculos', 'Servicios de Alquiler de Turismos 2017', '2016-11-29 18:20:00', '2016-12-14 14:00:00', false, true, true, false, 16, true, '2018-12-01 00:00:00', '2018-11-30 00:00:00', NULL, NULL, NULL, false, 0, 1, 'LICITACIÓN PARA LA SELECCIÓN DEL PROVEEDOR DEL SERVICIO DE ALQUILER DE COCHES Y FURGONETAS.

VER FICHEROS ADJUNTOS:
* Pliego Licitación-pdf
*Excel Cotizacion. xlsx (Dicho excel consta de 2 pestañas, una para ofertar el alquiler de coches y otra para el alquiler de furgonetas)
LEER DETENIDAMENTE EL PLIEGO DE CONDICIONES ADJUNTO.

Las ofertas deben incluir, como archivos anexos:
* Documento con su oferta técnica y económica completa (incluyendo otras categorías de vehículos y mejoras propuestas sobre los servicios solicitados). 

Poner especial atención en la propuesta de cargo por deposito vacío y en los requerimientos del seguro de los vehículos.', false, NULL, 'En caso de dudas sobre las condiciones o servicios de esta licitación, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

Maricarmen Luzon
Project Manager 
mcluzon@itbid.com
636 10 93 45', '2016-11-29 17:55:46', '2016-11-29 18:16:23', NULL, 2, 2, NULL, NULL, NULL, true, 0, 'TBC', 5, 'N/A', NULL, 0, NULL, 'Alquiler Turismos - Car Rental', false, false, '[ 16-Septiembre-2014 15:53:19 ]  Equipo ITbid :   añadido Sixt
', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2016-12-16 12:56:05', NULL, NULL, '2014-09-22 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, true, true, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (209, 1, 771, 14, '209', NULL, '2021-07-21 11:09:58', '2021-07-21 11:09:58', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2021-07-21 11:09:58', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (89, 1, 2, 2, 'CFC022-2', 'RENTING EQUIPOS MULTIFUNCION - OFERTA FINAL', '2015-11-18 16:00:00', '2015-11-18 18:00:00', false, true, true, false, 11, true, NULL, NULL, NULL, NULL, true, false, 1, 1, 'FASE FINAL, DE MEJORA DE OFERTAS, MEDIANTE LICITACION EN &quot;SOBRE CERRADO&quot;

2 UNICOS PARTICIPANTES

* Han de entrar 1 UNICA Y DEFINITIVA OFERTA, relativa la CUOTA UNITARIA POR MES de cada equipo (100 equipos) y el COSTE X 1000 COPIAS

* La oferta ha de referirse al modelo cotizado anteriormente, y con los niveles de servicio ofrecidos

* La oferta ha de incliur la retirada de los equipos a sustituir

* La oferta no puede ser superior a la efectuada anteriormente; si lo fuera, será declarada nula

* El proveedor NO está obligado a mejorar su oferta precedente

LA COTIZACION DEBERÁ REALIZARSE EXCLUSIVAMENTE EL MIERCOLES 18 DE NOVIEMBRE, ENTRE LAS 16:00 Y LAS 18:00H.

NI EL PERSONAL DE CARGLASS NI EL DE ITBID TIENEN ACCESO A LAS OFERTAS DURANTE LA VENTANA DE TIEMPO EN QUE LOS PROVEEDORES PUEDEN COTIZAR.

ESTA HABILITADO EL &quot;CENTRO DE MENSAJES&quot; SI DESEAN PLANTEAR ALGUNA CUESTION.
', false, NULL, 'EN CASO DE DUDAS, USEN CENTRO DE MENSAJES', '2015-11-16 12:07:00.430929', '2015-11-16 12:07:44.642902', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 5, '--', 'EUR', 999, 'SEGUN PLIEGO DE CONDICIONES', 'PC, Monitores y Laptops', false, false, '', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2016-02-16 11:36:55', NULL, NULL, '2015-11-18 18:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, 6, false, false, true, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (45, 1, 2, 2, 'CFC013-P', 'TRANSPALETAS MANUALES ALFA', '2013-12-17 12:08:06.525083', '2013-12-19 14:00:00', false, true, true, false, 2, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'Licitación para la adquisición de 70 transpaletas manuales, clásicas. Requesitos aproximados, pueden ser modificados en función de características del licitador:

Carga: 2.000 kg
Longitud horquillas: 115 cm

* Las claves delante de los criterios de cotización remiten al esquema adjunto (Fichero "esquema.JPG")

ADJUNTAR FOLLETO TECNICO DE LA SOLUCION PROPUESTA

Cada transpaleta debe ser entregada (DDP) en diversos talleres de la antigua red de Guardian - Ver fichero adjunto.

ATENCION: COTIZAR EN LA PROPIA PLATAFORMA ANTES DE LAS 14:00H DEL JUEVES 19 DE DICIEMBRE', false, NULL, 'En caso de dudas sobre la plataforma, entrada de ofertas o anexar ficheros, por favor, contactar con:

JOSE MANUEL IAÑEZ
Mov. 676 953 333
email: jmianez@itbid.org', '2013-12-17 12:07:15.312677', '2013-12-17 12:08:06.525083', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 3, 'Se adjunta relación de puntos de entrega - ', NULL, 999, NULL, 'Carretillas', false, false, '[ 17-Diciembre-2013 12:53:55 ]  Equipo ITbid :   Añadido ESIN
', NULL, true, 2, '2013-12-20 13:25:31.011453', NULL, NULL, NULL, NULL, '2013-12-20 13:25:31.011453', NULL, NULL, '2013-12-19 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (30, 1, 2, 2, '30', 'Suministro energía electrica 2013 - FASE FINAL', '2013-07-11 10:00:00', '2013-07-11 14:00:00', false, true, true, false, 6, true, '2015-08-31 00:00:00', NULL, NULL, NULL, NULL, false, 2, 1, 'Fase Final, mediante Oferta en Sobre Cerrado.

Se debe entrar UNA UNICA Y DEFINITIVA OFERTA, 

La oferta sólo se podrá entrar en la plataforma electrónica ENTRE LAS 10:00 Y LAS 14:00 DEL JUEVES 11 DE JULIO. 

Durante dicha ventana de tiempo, ni el personal de CARGLASS ni el de ITBID  tienen acceso a las ofertas presentadas.

Se debe entrar exclusivamente el importe del "Total Cotizado" del Casillero M2 del Excel

Se debe adjuntar el Excel cumplimentado, de forma que el casillero M2 cuadre con el importe entrado en la plataforma.

', false, NULL, 'Si algún licitador experimentara dificultades técnicas para la entrada de la oferta o la subida de ficheros, deberá contactar con:

Sr. Jose Manuel Iáñez
ITbid Technologies
e-mail: jmianez@itbid.org
Móv. 676 953 333
Tel. 933 218 614
', '2013-07-08 12:47:09.92349', '2013-07-08 12:47:27.306186', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'La habitual - ver pliego', 5, '--', 'EUR', 999, 'N/A', 'Electricidad', false, false, '', NULL, true, 2, '2013-10-24 18:57:46.482606', NULL, NULL, NULL, NULL, '2013-10-24 18:57:46.482606', NULL, NULL, '2013-07-11 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (114, 1, 2, 2, 'CFC003', 'CFC003 SUMINISTRO MATERIAL DE OFICINA Y PAPEL FASE CIERRE EN SOBRE CERRADO', '2017-03-29 10:55:00', '2017-03-31 12:45:00', false, true, true, false, 20, true, '2018-12-01 00:00:00', '2018-11-30 00:00:00', NULL, NULL, NULL, false, 0, 1, 'El objeto de esta licitación es determinar los proveedores y condiciones óptimas para la prestación de diversos servicios de SUMINISTRO DE MATERIAL DE OFICINA Y PAPEL de CARGLASS, para el periodo 1 de Abril de 2017 a finales de Marzo 2019 (24 meses), con renovación anual.

LEER DETENIDAMENTE EL PLIEGO DE CONDICIONES ADJUNTO.

Para la oferta, se deberá descargar, cumplimentar según instrucciones y subir a la plataforma el Excel de Cotización.

IMPORTANTE:
EN ESTA FASE LA OFERTA PRESENTADA DEBERÁ SER A TRAVÉS DE CENTRO ESPECIAL DE EMPLEO.
DEBERÁN PRESENTAR:
-	Cual es su Pedido mínimo sin gastos de envío.
-       Gastos de envío para pedidos que no lleguen al mínimo solicitado.

Estos dos criterios serán considerados de forma positiva en esta fase de cierre.', false, NULL, 'En caso de dudas sobre las condiciones o servicios de esta licitación, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

Maricarmen Luzon
Project Manager 
mcluzon@itbid.com
636 10 93 45', '2017-03-29 10:36:05', '2017-03-29 10:46:33', NULL, 2, 2, NULL, NULL, NULL, true, 0, 'TBC', 5, 'N/A', NULL, 0, NULL, 'CF07-5 - Material de Oficina', false, false, '[ 16-Septiembre-2014 15:53:19 ]  Equipo ITbid :   añadido Sixt
', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-25 16:08:47', NULL, NULL, '2014-09-22 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, true, true, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (96, 1, 2, 2, 'CFC028-P', 'SERVICIO DE CENTRAL DE ALARMAS', '2016-02-15 20:41:14', '2016-03-02 20:00:00', false, true, true, false, 18, true, '2018-12-31 00:00:00', '2016-04-01 00:00:00', NULL, NULL, NULL, false, 1, 1, 'El objeto de esta negociación es la prestación del servicio de Central de Alarmas y otros conexos, para la Red de Centros y Hubs de Carglass en España hasta 31 de diciembre de 2018.

Ver Pliego de Condiciones y resto de documentos adjuntos', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre características de los equipos, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

JOSE MANUEL IAÑEZ
ITBID ESOURCING SOLUTIONS
Móvil +34 676 953 333
jmianez@itbid.com', '2016-02-15 20:41:14', '2016-02-15 21:11:31', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 3, 'NO PROCEDE - VER PLEIGO DE CONDICIONES', NULL, 999, NULL, 'Serv Seguridad / Central de Alarmas', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2016-03-11 13:44:29', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, 11, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (246, 1, 771, 14, '246', 'INSTALACIÓN DE PLACAS FOTOVOLTAICAS CARGLASS CIEMPOZUELOS', '2021-12-02 18:03:23', '2021-12-02 18:03:23', false, false, false, false, 6, false, NULL, NULL, NULL, NULL, false, false, 1, 1, '<p>El objeto de esta negociaci&oacute;n es identificar potenciales proveedores que puedan llevar a cabo un proyecto de suministro, instalaci&oacute;n y mantenimiento de placas fotovoltaicas en el HUB principal de Carglass en Ciempozuelos, Madrid.</p>

<p>&nbsp;</p>

<p><b>Consumo estimado Ciempozuelos 689 MWh/a&ntilde;o.</b></p>

<p><b>Cubierta en chapa de 2mm, lana y chapa. </b></p>

<p><strong>Calendario del proyecto:</strong></p>

<div align="center">
<table class="Table" style="border-collapse:collapse; border:none">
	<thead>
		<tr>
			<td style="width:346.35pt; border:solid #999999 1.5pt; background:#e6e6e6" width="462">
			<p class="CxSpFirst" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>ACTIVIDAD</b></p>
			</td>
			<td style="width:118.85pt; border:solid #999999 1.5pt; border-left:none; background:#e6e6e6" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>FECHA PREVISTA</b></p>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Env&iacute;o de la petici&oacute;n de informaci&oacute;n</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>10/11/2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Plazo l&iacute;mite para la recepci&oacute;n de consultas de los licitadores</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>15/11/21</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>Plazo l&iacute;mite para la Recepci&oacute;n de propuestas in&iacute;ciales</b></p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt; background:#a8d08d" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>19/11/21</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Revisi&oacute;n, valoraci&oacute;n y an&aacute;lisis de las propuestas</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>22 al 26/11/2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Convocatoria de licitadores seleccionados a participar en la RFQ</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>29/11/2021</b></p>
			</td>
		</tr>
	</tbody>
</table>
</div>

<p>&nbsp;</p>', false, NULL, NULL, '2021-12-02 18:03:23', '2021-11-10 13:46:47', NULL, 14, 14, NULL, NULL, NULL, false, 999, NULL, 5, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 999, 'str_otras_condiciones_envio', 'K02. Electricity', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-19 16:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, false, true, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (44, 1, 2, 2, 'CFC011-P', 'PETICION DE INFORMACION SERVICIOS GRAFICOS', '2013-11-26 17:22:14.375308', '2013-12-02 18:00:00', false, false, false, true, 13, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'El objeto de la presente solicitud de información (RFI) es obtener datos básicos que nos permitan conocer su empresa, de cara iniciar los trámites de homologación como PROVEEDORES DE SERVICIOS GRAFICOS. CARGLASS ha encargado a ITbid Technologies su asesoramiento en este proceso.

POR FAVOR, DESCARGAR, CUMPLIMENTAR Y DEVOLVER A TRAVÉS DE ESTA MISMA PLATAFORMA EL EXCEL ADJUNTO 
"Serv_Graficos_Carglass.XLSX"

Deben enviarlo, si es de su interes, antes las 14:00h del lunes 2 de diciembre.

Para cuestiones relativas a esta petición de información (RFI), usen el "Centro de Mensajes" de la propia negociación.
', false, NULL, 'Para cuestiones relativas a la plataforma o entrada de ofertas, contactar con:

ANTONIO DE HARO
Mov. 639 757 911
adeharo@itbid.org
', '2013-11-26 17:21:18.773651', '2013-11-26 17:22:14.375308', '2014-03-17 16:24:24.513502', 2, 2, 2, NULL, NULL, true, 999, 'Transferencia 30 días', 3, '--
', NULL, 999, NULL, 'Servicios Gráficos', false, false, '[ 27-Noviembre-2013 19:12:25 ]  Equipo ITbid :   Añadido Cuscó
[ 02-Diciembre-2013 16:46:58 ]  Equipo ITbid :   Se reabre para DIGITAL SCREEN hasta las 18:00
', NULL, false, 2, '2014-03-17 16:24:24.513502', NULL, NULL, NULL, NULL, '2014-03-17 16:24:24.513502', NULL, NULL, '2013-12-02 18:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'simple', 'desierta', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (69, 1, 2, 2, 'FCP005P', 'SERVIÇO DE TRANSPORTE E DISTRIBUIÇÃO', '2015-03-27 14:47:02.643346', '2015-04-17 20:30:00', false, true, true, false, 9, true, '2017-05-31 00:00:00', '2015-06-01 00:00:00', NULL, NULL, NULL, false, 0, 1, 'SERVIÇO DE TRANSPORTE E DISTRIBUIÇÃO DOS ARMAZÉMS DE CARGLASS AO SEUS HUB´S, CENTROS E CLIENTES EXTERNOS - 
PERÍODO DE 01 DE JUNHO DE 2015 E 31 DE MAIO DE 2017 (24 MESES).

Faça o download da documentação e Condiçoes do concurso e o Excel de Cotacão.

O serviço de transporte consiste no traslado de vidro para automóveis e materiais auxiliares para montagem e condicionamiento a partir: 

* Dos armazéns centrais da CARGLASS em Ciempozuelos (Madrid-Espanha) para 2 Armazéms centrais (HUB’s) em Portugal: Lisboa y Porto
* Dos armazéns centrais (HUB’s) em Portugal para 32 Centros entrega programada 
* 16 Centros em áreas de influência dos HUB’s 

OS SENHORES LICITANTES PODERÃO APRESENTAR A SUA PROPOSTA PARA CADA UM DOS TIPOS DE TRANSPORTE REQUERIDOS



', false, NULL, 'Os fornecedores enviaram suas perguntas através do &quot;Centro de Mensagens&quot; da própia negociação. O prazo para envio de consultas através da plataforma ITBID será o indicado no documento de Condiçoes.

ITbid irá responder a todas as perguntas, por escrito, por meio do &quot;Centro de Mensagens&quot; para todos os fornecedores, na data indicada no calendário. As respostas que sejam identificadas de interesse geral serão enviadas para todos os fornecedores convidados ao processo, sem indicar quem o fornecedor que fez a consulta. 

Em caso de dúvidas sobre os códigos de acesso, a introdução de licitações ou qualquer assunto relacionado com a plataforma de negociação ITbid, por favor, entre em contacto com:

SR. JOSEP PUIG
Telf: + 34 933 218 614
jpuig@itbid.com
Cel: + 34 655347607

SR. ANTONIO DE HARO
Telf: + 34 933 218 614
adharo@itbid.com 
Cel: + 34 639757911

', '2015-03-27 14:31:53.194833', '2015-03-27 14:47:02.643346', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 3, '--
', NULL, 999, NULL, 'Logistica', false, false, '', NULL, true, 2, '2015-05-06 09:49:54.618597', NULL, NULL, NULL, NULL, '2015-05-06 09:49:54.618597', NULL, NULL, '2015-04-17 20:30:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 4, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (81, 1, 2, 2, 'CFC025-S', 'RENTING EQUIPOS INFORMATICOS 2015 - FASE FINAL', '2015-07-03 09:45:00', '2015-07-03 10:00:00', false, false, false, false, 11, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'selección del proveedor para Renting de Equipos de Informática durante 2015. El número definitivo de unidades a suministrar es según lo indicado en la fase precedente.

* Ver adjunto REGLAS_SUBASTA_CFC025.PDF

Duración: 15 minutos + ilimitadas extensiones de 5 minutos si se producen ofertas que ocupen posición 1 en los últimos 3 minutos de cada periodo.

NO será visible la mejor oferta
SI será visible la POSICION que ocupa la oferta (1, 2, etc...)

Una vez finalizada la subasta, el ganador deberá entregar un nuevo desglose de precios unitarios mensuales. Se aplicarán los precios introducidos en la fase previa de petición de ofertas, reducidos en la misma proporción a la eventual bajada efectuada durante la fase de subasta.

', false, NULL, 'En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

SR. JOSE MANUEL IÁÑEZ
ITBID TECHNOLOGIES
Móvil 676 953 333
jmianez@itbid.org
', '2015-07-01 19:05:32.734199', '2015-07-01 19:06:20.365205', NULL, 2, 2, NULL, 0, NULL, true, 999, 'SEGUN PLIEGO DE CONDICIONES', 10, 'SEGUN PLIEGO DE CONDICIONES
', NULL, 999, 'SEGUN PLIEGO DE CONDICIONES', 'PC, Monitores y Laptops', false, true, '', NULL, false, NULL, NULL, '2015-07-02 18:34:27.190612', 2, 0.25, 8, NULL, NULL, 0, '2015-07-03 10:00:00', 15, 0, NULL, NULL, 0, 0, false, true, 0, 2, 2, 6, true, false, -1, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'abortada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (79, 1, 2, 2, 'CFC025-2', 'RENTING EQUIPOS INFORMATICOS 2015 (ES) y (PT)', '2015-06-22 12:11:33.281274', '2015-06-26 14:00:00', false, false, false, true, 11, true, '2015-08-14 00:00:00', '2015-07-27 00:00:00', NULL, NULL, true, false, 1, 1, 'TENDER PARA RENTING DE EQUIPOSINFORMATICOS 2015 - ESPAÑA Y PORTUGAL

ES OBLIGATORIO COTIZAR AMBOS PAISES

SE DEBE ENTRAR EXCLUSIVAMENTE LA CUOTA MENSUAL UNITARIA POR EQUIPO.

RENTING A 4 AÑOS - 48 CUOTAS - TODOS LOS PRECIOS UNITARIOS SIN IVA

LOS PRODUCTOS COTIZADOS HAN DE SER LOS DE LA FASE PRECEDENTE, EXCEPTO LO INDICADO. SI SE CAMBIA EL PRODUCTO, SE DEBE ADJUNTAR LA NUEVA FICHA TÉCNICA.', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre características de los equipos, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

JOSE MANUEL IAÑEZ
ITBID ESOURCING SOLUTIONS
Móvil +34 676 953 333
jmianez@itbid.com
', '2015-06-22 12:11:08.692474', '2015-06-22 12:11:33.281274', '2015-07-01 18:57:20.072309', 2, 2, 2, NULL, NULL, true, 999, 'CONTRATO RENTING', 1, 'VER PLIEGO DE CONDICIONES
', 'EUR', 999, 'VER PLIEGO DE CONDICIONES', 'PC, Monitores y Laptops', false, false, '', NULL, false, 2, '2015-07-01 18:57:20.072309', NULL, NULL, NULL, NULL, '2015-07-01 18:57:20.072309', NULL, NULL, '2015-06-26 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, 8, false, true, false, false, false, false, 'simple', 'desierta', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (20, 1, 49, 7, 'AH001', 'RFP Hoteles 2013', '2013-03-14 14:40:46.694987', '2013-03-22 12:00:00', false, true, true, false, 4, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'El objeto de esta negociación es determinar las condiciones económicas, servicios y ubicación que pueden ofrecer para formar parte como proveedor habitual y  preferente en la  contratación de pernoctaciones para el personal de los centros de CARGLASS.

VER EN ANEXOS:

* Reglas_y_condiciones_RFP_AH001.PDF
* Anexo1_Fichero_Cotizacion.XLS

INSTRUCCIONES RESUMIDAS

Descargar y cumplimentar el anexo 1; copiar los 5
valores inferiores en los casilleros de la web;
adjuntar el Excel cumplimentado a la oferta de la
web.

', false, NULL, 'Si algún licitador experimentara dificultades técnicas para la entrada de la oferta o la subida de ficheros, deberá contactar con:

Sr. Antonio de Haro
ITbid Technologies
e-mail: adeharo@itbid.org
Telf: 933218614 
Móvil :639757911
', '2013-03-14 14:40:16.17387', '2013-03-14 14:40:46.694987', NULL, 7, 2, NULL, NULL, NULL, true, 999, 'Ver pliego de condiciones', 3, 'N/A
', NULL, 999, NULL, 'Hoteles y Alojamientos', false, false, '[ 15-Marzo-2013 09:33:12 ]  Jose Manuel Iáñez :   Añadido Accord
[ 15-Marzo-2013 18:44:34 ]  Jose Manuel Iáñez :   aÑADIDO NH
[ 15-Marzo-2013 18:51:41 ]  Jose Manuel Iáñez :   añadido silken
[ 19-Marzo-2013 12:15:50 ]  Jose Manuel Iáñez :   nuevo proveedor AC Hoteles incorporado
[ 19-Marzo-2013 20:16:38 ]  Jose Manuel Iáñez :   cAMBIO FECHA CIERRE PARA NH - NO SE COMUNCIA RESTO
', NULL, true, 2, '2013-06-27 12:09:49.939828', NULL, NULL, NULL, NULL, '2013-06-27 12:09:49.939828', NULL, NULL, '2013-03-22 12:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (27, 1, 19, 6, 'CFC007', 'Suministro energía electrica 2013', '2013-06-25 18:21:31.896259', '2013-07-03 15:00:00', false, true, true, false, 6, true, '2015-07-31 00:00:00', '2015-06-15 00:00:00', NULL, NULL, NULL, false, 0, 1, 'Suministro de energía eléctrica en baja tensión hasta Julio 2015, para los centros y Hubs de CARGLASS que se detallan en el Anexo 1- Excel de Cotización - 3.733 Mwh aprox

DESCARGAR FICHEROS ADJUNTOS:

* Reglas_y_Condiciones_RFQ_CFC007.PDF --> Pliego condiciones de esta licitación
* Anexo1_Excel_Cotizacion.XLSX  --> datos de los centros y consumos, A SER CUMPLIMENTADO.

El Excel contiene 2 hojas: la de centros, a ser cumplimentada con los precios individuales y la de Descuentos por volumen.

Los posibles lotes serán comunicados en una fase posterior.

SE DEBEN ANEXAR A LA OFERTA LOS SIGUIENTES FICHEROS:

* El Excel de Cotización completado 
* Documento con las mejoras o servicios adicionales propuestos
* Enlace y claves de acceso demostración de la oficina virtual 
* Excel de muestra con la información de facturas por centros
* Certificado de origen de la electricidad ', false, NULL, 'Si algún licitador experimentara dificultades técnicas para la entrada de la oferta o la subida de ficheros, deberá contactar con:

Sr. Jose Manuel Iáñez
ITbid Technologies
e-mail: jmianez@itbid.org
Móv. 676 953 333
Tel. 933 218 614
', '2013-06-25 18:20:30.018423', '2013-06-25 18:21:31.896259', NULL, 6, 6, NULL, NULL, NULL, true, 999, 'La habitual - ver pliego', 3, 'N/A
', NULL, 999, NULL, 'Electricidad', false, false, '', NULL, true, 2, '2013-07-10 14:28:30.889164', NULL, NULL, NULL, NULL, '2013-07-10 14:28:30.889164', NULL, NULL, '2013-07-03 15:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (211, 1, 771, 14, '211', NULL, '2021-10-19 13:50:12', '2021-10-19 13:50:12', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2021-10-19 13:50:12', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (70, 1, 2, 2, 'FCP005P-VAN', 'TTE Y DISTRIBUCION AREA INFLUENCIA HUBS', '2015-04-22 13:53:44.30105', '2015-04-24 18:00:00', false, true, true, false, 9, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'NUEVA SOLICITUD DE PROPUESTA - 
AFECTA SOLO AL SERVICIO DE REPARTO EN AREA DE INFLUENCIA DE HUBS LISBOA Y OPORTO

Precisa y modifica la anterior solicitud POR FAVOR, COTIZAR DEVOLVIENDO EL NUEVO EXCEL CUMPLIMENTADO  ANTES DE LAS 18:00 DEL VIERNES 24.


•	Se manejan 2 opciones:
o	1 reparto diario
o	2 repartos diarios
•	Se recoge a las 08:00 (y en la opción 2 repartos, también a las 14:00h)
•	Se ha de entregar en los Centros antes 2 horas desde la recogida, es decir, antes de las 10:00h (y de las 16:00h en la opción 2 repartos)
o	Ello parece implicar que se deberían usar 2 furgonetas por cada Hub 
•	Se desea obtener 3 tipos de facturación posible (por cada opción):
o	Precio por punto de entrega (el Hub de origen – destino de la logística inversa NO se considera punto de entrega)
o	Precio por hora de furgoneta
o	Precio mensual por ruta
Los 3 precios deberían ser coherentes y dar un resultado similar en coste anual.

', false, NULL, 'En caso de dudas, usen el “Centro de Mensajes”. Para problemas con el uso de la plataforma, dirigirse a:

JOSE MANUEL IÁÑEZ
Telf: 933 218 614
Móvil: 676 953 333
jmianez@itbid.com
', '2015-04-22 13:51:02.131124', '2015-04-22 13:53:44.30105', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'A CONVENIR', 3, '--
', NULL, 999, NULL, 'Logistica', false, false, '', NULL, true, 2, '2015-05-06 09:51:02.437339', NULL, NULL, NULL, NULL, '2015-05-06 09:51:02.437339', NULL, NULL, '2015-04-24 18:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 4, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (168, 1, 818, 16, '168', NULL, '2019-03-20 21:12:07', '2019-03-20 21:12:07', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2019-03-20 21:12:07', NULL, NULL, 16, 16, NULL, NULL, NULL, false, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (59, 1, 2, 2, 'CFC016-P', 'Servicios de Alquiler de Turismos 2015', '2014-09-11 19:57:09.39465', '2014-09-22 14:00:00', false, true, true, false, 16, true, '2015-12-31 00:00:00', '2014-11-01 00:00:00', NULL, NULL, NULL, false, 0, 1, 'LICITACION PARA LA SELECCION DEL PROVEEDOR DEL SERVICIO DE ALQUILER DE TURISMOS.

VER FICHEROS ADJUNTOS:

* Reglas_y_condiciones.pdf
* Cobertura_geografica.xlsx
* MUESTREO_400_ALQUILERES.xlsx

Aunque el sistema admite ilimitadas ofertas, se sugiere se realicen un máximo de 2 por licitador (preferentemente, con seguro de daños con y sin franquicia). 

Una vez creadas las ofertas, pueden ser editadas libremente hasta la fecha/hora máxima de presentación de ofertas, pulsando el boton "EDITAR".

Las ofertas deben incluir, como archivos anexos:

* Documento con su oferta técnica y económica completa (incluyendo otras categorías de vehículos y mejoras propuestas sobre los servicios solicitados). 
* Fichero “Cobertura_geográfica.XLSX” debidamente cumplimentado
* Si fuera posible, un fichero Excel de muestra, similar al solicitado en el apartado “3.4. Administración y Control del Gasto” del pliego de condiciones.
', false, NULL, 'En caso de dudas sobre las condiciones o servicios de esta licitación, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

JOSE MANUEL IAÑEZ
ITBID Technologies
Móvil 676 953 333
jmianez@itbid.org
', '2014-09-11 19:55:53.513767', '2014-09-11 19:57:09.39465', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'TBC', 3, 'N/A
', NULL, 999, NULL, 'Alquiler Turismos - Car Rental', false, false, '[ 16-Septiembre-2014 15:53:19 ]  Equipo ITbid :   añadido Sixt
', NULL, true, 2, '2014-11-07 20:14:30.774099', NULL, NULL, NULL, NULL, '2014-11-07 20:14:30.774099', NULL, NULL, '2014-09-22 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, 1, false, true, true, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (55, 1, 2, 2, 'CFC015-P', 'IMPRESORAS PORTATILES Y SMARTPHONES', '2014-05-12 20:18:44.422286', '2014-05-23 14:00:00', false, true, true, false, 15, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'Licitación para la adquisición de 120 conjuntos de impresoras térmicas portátiles con conexión Bluetooth (+Wifi, preferentemente), y  Smartphone (Android), para equipar las unidades móviles de CARGLASS. La presentación de ofertas para el Smartphone es OPCIONAL, aunque Carglass valora positivamente agrupar el pedido en un único proveedor.

Ver fichero adjunto: REGLAS_Y_CONDICIONESV2.PDF

Junto con las ofertas se han de anexar los documentos indicados en el pliego de condiciones.

', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre características de los equipos, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica.   

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:  

SR. D. JOSE MANUEL IÁÑEZ 
ITbid Technologies 
Móvil 676 953 333 
jmianez@itbid.org
', '2014-05-12 20:17:55.313321', '2014-05-12 20:18:44.422286', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 3, 'FACUNDO BACARDI I MASSO 7- 11
08100 MOLLET DEL VALLES
(Barcelona)
SPAIN
', NULL, 999, NULL, 'Smartphones y dispositivos móviles', false, false, '[ 13-Mayo-2014 18:22:56 ]  Equipo ITbid :   Añado 2 proveedores
[ 19-Mayo-2014 13:50:37 ]  Equipo ITbid :   Prvo añadido FEWLAPS
[ 19-Mayo-2014 13:57:19 ]  Equipo ITbid :   fewlaps
', NULL, true, 2, '2014-06-06 12:25:17.04358', NULL, NULL, NULL, NULL, '2014-06-06 12:25:17.04358', NULL, NULL, '2014-05-23 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (34, 1, 2, 2, 'CFC010', 'SERVICIO MENSAJERIA 2013-2014', '2013-07-24 22:51:25.963414', '2013-07-30 11:00:00', false, true, true, false, 9, true, NULL, NULL, NULL, NULL, NULL, false, 1, 1, 'Licitación para la seleccion del proveedor de servicios de mensajeía para hacer frente a las necesidades  de la firma CARGLASS B.V Surcursal en España.

VER FICHEROS ADJUNTOS:

Reglas y Condiciones (PDF) --> normas de esta negociación
Guia Rapida de Ayuda para cotizar (PDF)

****

Por favor, cumplimenten los casilleros con los resultados (Crierios 1 al 5) que se definen en las reglas y condiciones de la negociación, así como adjuntar la información complementaria requerida.
', false, NULL, 'Si algún licitador experimentara dificultades técnicas para la entrada de la oferta o la subida de ficheros, deberá contactar con:

Sr. Josep Puig o David Amigo
ITbid Technologies
e-mail: jpuig@itbid.org / damigo@itbid.org
Telef: 933218614 

', '2013-07-24 22:47:00.714811', '2013-07-24 22:51:25.963414', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 1, 'FACUNDO BACARDI I MASSO 7- 11
08100 MOLLET DEL VALLES
(Barcelona)
SPAIN
', 'EUR', 999, 'Segun Servicio y Tipo entrega.', 'Logistica', false, false, '', NULL, true, 2, '2013-11-07 09:44:54.781604', NULL, NULL, NULL, NULL, '2013-11-07 09:44:54.781604', NULL, NULL, '2013-07-30 11:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (53, 1, 2, 2, 'CFC012-2-p', 'Renovacion Equipos 2014-2', '2014-05-12 19:08:58.53214', '2014-05-23 17:00:00', false, false, false, true, 11, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'Proceso para la adquisición de 70 PC (2 modelos), 20 laptop (2 modelos), 4 ultrabooks (2 modelos) y 100 monitores para renovación de equipos de Centros y Call Center CARCLASS.

ESTA NEGOCIACION SUSTITUYE A LA ANTERIOR INICIADA EN 2013

VER FICHERO ADJUNTO: 

Reglas_y_condiciones_V4.PDF

RESUMEN

* Cotizar 2 versiones (Básica y Superior) de PC, laptop y ultrabook
* En caso de ser declarado inicialmente finalista, se deberá depositar un equipo para ser homologado por CARGLASS.
* Los modelos se deben considerar con 3 años de garantía
* Junto con las ofertas se han de anexar los documentos indicados en el pliego de condiciones.
', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre características de los equipos, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

SR. D. JOSE MANUEL IÁÑEZ
ITbid Technologies
Móvil 676 953 333
jmianez@itbid.org
', '2014-05-12 19:04:36.94972', '2014-05-12 19:08:58.53214', '2014-05-28 18:49:22.86236', 2, 2, 2, NULL, NULL, true, 0, '30 días Transferencia', 3, 'FACUNDO BACARDI I MASSO 7- 11
08100 MOLLET DEL VALLES
(Barcelona)
SPAIN
', NULL, 999, NULL, 'PC, Monitores y Laptops', false, false, '[ 13-Mayo-2014 18:09:05 ]  Equipo ITbid :   añado 2 proveedores
[ 23-Mayo-2014 16:38:22 ]  Equipo ITbid :   Ventana MicroBlanc
', NULL, false, 2, '2014-05-28 18:49:22.86236', NULL, NULL, NULL, NULL, '2014-05-28 18:49:22.86236', NULL, NULL, '2014-05-23 17:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'simple', 'desierta', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (64, 1, 2, 2, 'CFC021', 'Renovacion Equipos 2015', '2014-11-24 20:26:48.873322', '2014-12-05 14:00:00', false, false, false, false, 11, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'Proceso para la adquisición de 70 PC básicos, 20 PC Superior, 90 monitores 23&quot;, 15 Monitores 19-20&quot;, 10 Laptops y 2 Laptops Superior, con destino a la renovación de equipos de Centros y Call Center CARCLASS.

VER FICHERO ADJUNTO: 

Reglas_y_condiciones_CFC021.PDF

RESUMEN

* Cotizar máximo 2 ofertas x proveedor
* Se solicitan equipos en depósito para homologación; ver fecha máxima de depósito
* Los modelos se deben considerar con 3 años de garantía (24h, centralizada en Mollet o In Situ)
* Junto con las ofertas se han de anexar los documentos indicados en el pliego de condiciones.
', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre características de los equipos, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

SR. D. JOSE MANUEL IÁÑEZ
ITbid Technologies
Móvil 676 953 333
jmianez@itbid.com
', '2014-11-24 20:25:44.449746', '2014-11-24 20:26:48.873322', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 3, 'FACUNDO BACARDI I MASSO 7- 11
08100 MOLLET DEL VALLES
(Barcelona)
SPAIN
', NULL, 999, NULL, 'PC, Monitores y Laptops', false, true, '[ 25-Noviembre-2014 09:58:06 ]  Equipo ITbid :   A&Ntilde;ADIDO PC COMPONENTES
[ 27-Noviembre-2014 10:56:38 ]  Equipo ITbid :   a&ntilde;adido THE HP SHOP
', NULL, false, NULL, NULL, '2014-12-03 10:15:29.943347', 2, NULL, NULL, NULL, NULL, NULL, '2014-12-05 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, 4, true, true, false, false, false, false, 'simple', 'abortada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (72, 1, 2, 2, 'FCP005P-2', 'SERVIÇO TRANSPORTE - LONGA DISTÂNCIA', '2015-05-05 18:09:28.215394', '2015-05-13 20:00:00', false, false, false, true, 9, true, '2017-05-31 00:00:00', '2015-06-15 00:00:00', NULL, NULL, NULL, false, 0, 1, 'SERVIÇO DE TRANSPORTE LONGA DISTÂNCIA - 
PERÍODO DE 01 DE JUNHO DE 2015 E 31 DE MAIO DE 2017 (24 MESES).

Faça o download da documentação e Condiçoes do concurso e o Excel de Cotacão.

O serviço de transporte consiste no traslado de vidro para automóveis e materiais auxiliares para montagem e condicionamiento a partir: 

* Dos armazéns centrais da CARGLASS em Ciempozuelos (Madrid-Espanha) para 2 Armazéms centrais (HUB’s) em Portugal: Lisboa y Porto



', false, NULL, 'Os fornecedores enviaram suas perguntas através do &quot;Centro de Mensagens&quot; da própia negociação. O prazo para envio de consultas através da plataforma ITBID será o indicado no documento de Condiçoes.

ITbid irá responder a todas as perguntas, por escrito, por meio do &quot;Centro de Mensagens&quot; para todos os fornecedores, na data indicada no calendário. As respostas que sejam identificadas de interesse geral serão enviadas para todos os fornecedores convidados ao processo, sem indicar quem o fornecedor que fez a consulta. 

Em caso de dúvidas sobre os códigos de acesso, a introdução de licitações ou qualquer assunto relacionado com a plataforma de negociação ITbid, por favor, entre em contacto com:

SR. JOSEP PUIG
Telf: + 34 933 218 614
jpuig@itbid.com
Cel: + 34 655347607

SR. ANTONIO DE HARO
Telf: + 34 933 218 614
adharo@itbid.com 
Cel: + 34 639757911

', '2015-05-05 18:08:43.391687', '2015-05-05 18:09:28.215394', '2015-05-27 10:22:10.102012', 2, 2, 2, NULL, NULL, true, 0, '30 días Transferencia', 3, '--
', NULL, 999, NULL, 'Logistica', false, false, '', NULL, false, 2, '2015-05-27 10:22:10.102012', NULL, NULL, NULL, NULL, '2015-05-27 10:22:10.102012', NULL, NULL, '2015-05-13 20:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'simple', 'desierta', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (74, 1, 2, 2, 'FCP005P-3', 'SERVIÇO TRANSPORTE - DISTRIBUÇÃO PROGRAMADA', '2015-05-06 17:29:28.214179', '2015-05-15 20:00:00', false, true, true, false, 9, true, '2017-05-31 00:00:00', '2015-06-15 00:00:00', NULL, NULL, NULL, false, 0, 1, 'SERVIÇO DE TRANSPORTE DISTRIBUÇÃO PROGRAMADA - 
PERÍODO DE 01 DE JUNHO DE 2015 E 31 DE MAIO DE 2017 (24 MESES).

Faça o download da documentação e Condiçoes do concurso e o Excel de Cotacão.

O serviço de transporte consiste no traslado de vidro para automóveis e materiais auxiliares para montagem e condicionamiento a partir: 

* DOS ARMAZÉMS DE CARGLASS PORTUGAL AO SEUS CENTROS NA PORTUGAL



', false, NULL, 'Os fornecedores enviaram suas perguntas através do &quot;Centro de Mensagens&quot; da própia negociação. O prazo para envio de consultas através da plataforma ITBID será o indicado no documento de Condiçoes.

ITbid irá responder a todas as perguntas, por escrito, por meio do &quot;Centro de Mensagens&quot; para todos os fornecedores, na data indicada no calendário. As respostas que sejam identificadas de interesse geral serão enviadas para todos os fornecedores convidados ao processo, sem indicar quem o fornecedor que fez a consulta. 

Em caso de dúvidas sobre os códigos de acesso, a introdução de licitações ou qualquer assunto relacionado com a plataforma de negociação ITbid, por favor, entre em contacto com:

SR. JOSEP PUIG
Telf: + 34 933 218 614
jpuig@itbid.com
Cel: + 34 655347607

SR. ANTONIO DE HARO
Telf: + 34 933 218 614
adharo@itbid.com 
Cel: + 34 639757911

', '2015-05-06 17:28:26.932754', '2015-05-06 17:29:28.214179', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 3, '--
', NULL, 999, NULL, 'Logistica', false, false, '', NULL, true, 2, '2015-07-14 09:15:14.284792', NULL, NULL, NULL, NULL, '2015-07-14 09:15:14.284792', NULL, NULL, '2015-05-15 20:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (88, 1, 2, 2, 'CFC027C', 'LICITACION RECOGIDA DE RESIDUOS - CENTROS', '2015-09-24 14:03:53.970811', '2015-10-09 14:00:00', false, true, true, false, 17, true, '2018-12-31 00:00:00', '2016-01-01 00:00:00', NULL, NULL, NULL, false, 0, 1, 'Licitación para la prestación del servicio de Recogida de Residuos Peligrosos de la red de Centros CARGLASS repartidos por toda España, para el periodo Enero 2016 a Diciembre 2018 (3 años).

En esta fase de la negociación, participan licitadores debidamente acreditados para prestar, incluso con limitaciones geográficas, los servicios descritos. Por tanto, NO será necesario cotizar por la totalidad del negocio, es decir, por todos y cada uno de centros CARGLASS. 

Los licitadores podrán subcontratar parcialmente los servicios solicitados, o presentarse en forma de UTE.


', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre el servicio, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

JOSE MANUEL IAÑEZ
ITBID ESOURCING SOLUTIONS
Móvil +34 676 953 333
jmianez@itbid.com

', '2015-09-24 14:02:49.316996', '2015-09-24 14:03:53.970811', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 3, 'FACUNDO BACARDI I MASSO 7- 11
08100 MOLLET DEL VALLES
(Barcelona)
SPAIN
', NULL, 999, NULL, 'Recogida Residuos', false, false, '', NULL, true, 2, '2015-11-06 17:31:38.471238', NULL, NULL, NULL, NULL, '2015-11-06 17:31:38.471238', NULL, NULL, '2015-10-09 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, 9, true, true, true, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (87, 1, 2, 2, 'CFC027H', 'LICITACION RECOGIDA DE RESIDUOS - HUBS', '2015-09-24 13:26:00.762163', '2015-10-09 14:00:00', false, true, true, false, 17, true, '2018-12-31 00:00:00', '2016-01-01 00:00:00', NULL, NULL, NULL, false, 0, 1, 'Licitación para servicio de Recogida de Residuos No Peligrosos y Banales en los Hubs de CARGLASS de Mollet (Barcelona) y Ciempozuelos (Madrid), para el periodo Enero 2016 a Diciembre 2018 (3 años).

En esta fase de la negociación, participan licitadores debidamente acreditados para prestar, al menos, uno de los servicios descritos. Por tanto, NO será necesario cotizar por la totalidad del negocio, es decir, todos y cada uno de los puntos y servicios descritos.

Los licitadores podrán subcontratar parcialmente los servicios solicitados, o presentarse en forma de UTE.

CARGLASS se reserva el derecho a modificar o precisar las especificaciones del servicio y a confeccionar lotes, una vez analizadas las propuestas iniciales. En la confección de lotes, CARGLASS priorizará la amplitud y cobertura geográfica de los mismos, y solicitará una nueva oferta a los licitadores.
', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre el servicio, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

JOSE MANUEL IAÑEZ
ITBID ESOURCING SOLUTIONS
Móvil +34 676 953 333
jmianez@itbid.com
', '2015-09-24 13:25:36.685743', '2015-09-24 13:26:00.762163', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 3, 'NO PROCEDE - VER PLIEGO DE CONDICIONES
', NULL, 999, NULL, 'Recogida Residuos', false, false, '', NULL, true, 2, '2015-11-06 16:07:35.247905', NULL, NULL, NULL, NULL, '2015-11-06 16:07:35.247905', NULL, NULL, '2015-10-09 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, 9, true, true, true, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (130, 1, 49, 7, 'CFC032', 'SERVICIOS DE CALL CENTER', '2017-11-10 19:52:34', '2017-11-23 16:00:00', false, false, false, false, 22, false, '2020-12-31 00:00:00', '2018-01-01 00:00:00', NULL, NULL, NULL, false, 1, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación de los SERVICIOS DE CALL CENTER DESBORDAMIENTO, FIN DE SEMANA, NOCTURNO Y FESTIVOS para CARGLASS durante el periodo de 1 enero 2018 a 31 diciembre 2020 (36 meses).

Se ruega descargar y leer el Plego de Condiciones, adjunto en PDF', true, NULL, 'Las dudas sobre los servicios a contratar, sobre este pliego y/o sobre el proceso de negociación en general, deberán canalizarse exclusivamente a través del Centro de Mensajes de plataforma ITBID. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa al uso de la plataforma de negociación ITBID, por favor contactar con:

Jose Manuel Iáñez
Móvil 676 953 333
jmianez@itbid.com', '2017-11-10 19:52:34', NULL, NULL, 7, 7, NULL, NULL, NULL, false, 0, NULL, 3, 'No procede', NULL, 999, NULL, 'Call Center', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-23 16:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'borrador', false, NULL, NULL, true, 3, 3, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (136, 1, 2, 2, 'CFC-032-2018', 'SERVICIOS DE CALL CENTER  DESBORDAMIENTO, FIN DE SEMANA, NOCHES Y FESTIVOS', '2018-02-23 13:11:17', '2018-03-05 15:00:00', false, true, true, false, 22, true, NULL, NULL, NULL, NULL, NULL, false, 2, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación de los SERVICIOS DE CONTACT CENTER DESBORDAMIENTO, FIN DE SEMANA, NOCTURNO Y FESTIVOS para CARGLASS durante el periodo de 1 abril 2018 a 1 de marzo 2019.
Con el fin de asegurar la correcta estabilización y homologación del servicio, se establece periodo de prueba de seis meses, siendo avisados con anticipación de un mes. 
La licitación se configurará en sucesivas rondas, siendo esta inicial una Petición de Propuestas.
CARGLASS® garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociación y concurrencia de ofertas en igualdad de condiciones.', false, NULL, NULL, '2018-02-21 11:39:05', '2018-02-23 13:11:17', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 3, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 999, NULL, 'Call Center', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-12 18:18:50', NULL, NULL, '2018-03-05 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, 3, 3, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (137, 1, 49, 7, 'CFC-032', 'SERVICIOS DE CALL CENTER  DESBORDAMIENTO, FIN DE SEMANA, NOCHES Y FESTIVOS', '2018-03-27 10:22:02', '2018-04-06 14:00:00', false, true, true, false, 22, true, NULL, NULL, NULL, NULL, NULL, false, 2, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación de los SERVICIOS DE CONTACT CENTER DESBORDAMIENTO, FIN DE SEMANA, NOCTURNO Y FESTIVOS para CARGLASS durante el periodo de 01 de mayo de 2018 a 30 de abril de 2019.
Con el fin de asegurar la correcta estabilización y homologación del servicio, se establece periodo de prueba de seis meses, siendo avisados con anticipación de un mes. 
La licitación se ha configurado en sucesivas rondas; la primera se compuso por una solicitud de propuestas más abiertas sobre la que seleccionamos una shortlist de proveedores. Estos proveedores fueron visitados y se aclaró el tipo de servicio en una reunión en las instalaciones de cada proveedor. Esta ronda es la definitiva para seleccionar el proveedor adjudicatario que saldrá de la propuesta más competitiva a nivel técnico, competencial y económico.
CARGLASS® garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociación y concurrencia de ofertas en igualdad de condiciones.', false, NULL, NULL, '2018-03-26 20:52:17', '2018-03-27 10:22:02', NULL, 7, 7, NULL, NULL, NULL, true, 0, NULL, 3, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 999, NULL, 'Call Center', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-09 18:23:43', NULL, NULL, '2018-03-05 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, 3, 3, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (132, 1, 2, 2, '132', 'TRANSPORTE PROGRAMADO', '2018-01-15 08:28:30', '2018-01-15 08:28:30', false, false, false, false, 9, false, '2020-02-29 00:00:00', '2018-03-01 00:00:00', NULL, NULL, NULL, false, 1, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación del servicio de TRANSPORTE Y DISTRIBUCIÓN PROGRAMADA DE PRODUCTOS Y ACCESORIOS DESDE LOS ALMACENES DE CARGLASS A SUS CENTROS, a partir del día 1 de marzo de 2018 hasta el 28 de Febrero de 2020.

Los Sres Licitadores pueden seleccionar presentar su propuesta para uno o varios de los lotes.

Leer detenidamente el Pliego de Condiciones adjunto.

Para presentar una propuesta:

* Puede hacerlo "a mano", aunque le sugerimos descargar y cumplimentar un Excel (Ver adjunto "INSTRUCTIVO DESCARGAR Y COTIZAR EXCEL.PDF"
* Adjunte el resto de documentos e información solicitados en el Pliego de Condiciones', false, NULL, 'Las dudas sobre los contenidos o alcance de este pliego y/o sobre el proceso de negociación en general, deberán canalizarse exclusivamente a través del CENTRO DE MENSAJES de la plataforma.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:


Josep Puig

Móvil 655347607
Telf.: 933 218 614
jpuig@itbid.com

Jose Manuel Iañez

Móvil 676953333
Telf.: 933 218 614
jpuig@itbid.com', '2018-01-15 08:28:30', '2017-10-10 10:16:04', NULL, 2, 2, NULL, NULL, NULL, false, 0, NULL, 4, 'NO PROCEDE --', NULL, 999, NULL, 'Logistica', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LAS CANTIDADES HAN SIDO MULTIPLICADAS POR UN FACTOR 2,27 PARA OBTENER EL SUBTOTAL A PARTIR DE PRECIOS SUMINISTRADOS', NULL, '2017-10-10 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, false, true, false, false, false, false, 'avanzada', 'nueva', false, NULL, 'ponderado', false, 3, 3, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (120, 1, 2, 2, 'CF07-2', 'Suministro energía eléctrica 2017-2019 - Fase de Cierre', '2017-07-24 16:00:00', '2017-07-27 15:10:00', false, true, true, false, 6, true, '2019-07-31 00:00:00', '2017-09-01 00:00:00', NULL, NULL, NULL, false, 2, 1, 'El objeto de esta negociación es determinar las comercializadoras y condiciones óptimas para el suministro de energía eléctrica en baja tensión para los centros y Hubs de CARGLASS BV SUCURSAL EN ESPAÑA que se detallan en el Excel de Cotización, por un consumo anual estimado de 4,5 GWH.

DESCARGAR FICHEROS ADJUNTOS:

* Pliego_Sobre_Cerrado_Fase_Cierre_.pdf:  Pliego condiciones de esta licitación
* Excel_cotizacion_2017.xlsx: datos de los centros y consumos, A SER CUMPLIMENTADO Y DEVUELTO EN LA PLATAFORMA


SE DEBEN COMPLETAR TODOS LOS CASILLEORS DE LA OFERTA, Y ANEXAR EL EXCEL DE COTIZACION COMPLETADO

Opcionalmente, los licitadores podrán incluir cuantos ficheros deseen.', false, NULL, 'Las dudas sobre los contenidos o alcance de este pliego y/o sobre el proceso de negociación en general, deberán canalizarse exclusivamente a través de plataforma ITBID.

Si algún licitador experimentara dificultades técnicas para el acceso a la plataforma, la entrada de la oferta o la subida de ficheros, deberá contactar con:

Maricarmen Luzon
ITBID Technologies
e-mail: mcluzon@itbid.com
Móv. 636 10 93 45
Tel. 933 218 614', '2017-07-24 09:33:32', '2017-07-24 15:38:01', NULL, 2, 2, NULL, NULL, NULL, true, 0, 'Ver pliego', 5, 'N/A', NULL, 999, 'N/A', 'Electricidad', false, false, '', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-04 11:54:23', NULL, NULL, '2015-06-04 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (104, 1, 2, 2, 'CFC017- Alquiler vehiculos', 'Servicios de Alquiler de Turismos 2016', '2016-11-07 11:35:12', '2016-11-17 14:00:00', false, false, false, false, 16, false, '2018-11-30 00:00:00', '2018-11-30 00:00:00', NULL, NULL, NULL, false, 0, 1, 'LICITACIÓN PARA LA SELECCIÓN DEL PROVEEDOR DEL SERVICIO DE ALQUILER DE COCHES Y FURGONETAS.

VER FICHEROS ADJUNTOS:

* Pliego Licitación Alquiler Furgonetas. pdf
* Pliego Licitación Alquiler vehículos. pdf
* Cobertura_geografica. xlsx
*Excel Cotizacion. xlsx (Dicho excel consta de 2 pestañas, una para ofertar el alquiler de coches y otra para el alquiler de furgonetas)
LEER DETENIDAMENTE EL PLIEGO DE CONDICIONES ADJUNTO.

Las ofertas deben incluir, como archivos anexos:

* Documento con su oferta técnica y económica completa (incluyendo otras categorías de vehículos y mejoras propuestas sobre los servicios solicitados). 
* Fichero “Cobertura_geográfica.XLSX” debidamente cumplimentado', false, NULL, 'En caso de dudas sobre las condiciones o servicios de esta licitación, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

Maricarmen Luzon
Project Manager 
mcluzon@itbid.com
636 10 93 45', '2016-11-07 11:35:12', '2014-09-11 19:57:09.39465', NULL, 2, 2, NULL, NULL, NULL, false, 0, 'TBC', 1, 'N/A', NULL, 0, NULL, 'Alquiler Turismos - Car Rental', false, false, '[ 16-Septiembre-2014 15:53:19 ]  Equipo ITbid :   añadido Sixt
', NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-09-22 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, NULL, false, true, true, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (105, 1, 2, 2, 'CFC017- Alquiler vehiculos', 'Servicios de Alquiler de Turismos 2017', '2016-11-07 12:54:33', '2016-11-24 14:00:00', false, true, true, false, 16, true, '2018-12-01 00:00:00', '2018-11-30 00:00:00', NULL, NULL, NULL, false, 0, 1, 'LICITACIÓN PARA LA SELECCIÓN DEL PROVEEDOR DEL SERVICIO DE ALQUILER DE COCHES Y FURGONETAS.

VER FICHEROS ADJUNTOS:

* Pliego Licitación Alquiler Furgonetas. pdf
* Pliego Licitación Alquiler vehículos. pdf
* Cobertura_geografica. xlsx
*Excel Cotizacion. xlsx (Dicho excel consta de 2 pestañas, una para ofertar el alquiler de coches y otra para el alquiler de furgonetas)
LEER DETENIDAMENTE EL PLIEGO DE CONDICIONES ADJUNTO.

Las ofertas deben incluir, como archivos anexos:

* Documento con su oferta técnica y económica completa (incluyendo otras categorías de vehículos y mejoras propuestas sobre los servicios solicitados). 
* Fichero “Cobertura_geográfica.XLSX” debidamente cumplimentado', false, NULL, 'En caso de dudas sobre las condiciones o servicios de esta licitación, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

Maricarmen Luzon
Project Manager 
mcluzon@itbid.com
636 10 93 45', '2016-11-07 12:54:33', '2016-11-08 12:08:26', NULL, 2, 2, NULL, NULL, NULL, true, 0, 'TBC', 3, 'N/A', NULL, 0, NULL, 'Alquiler Turismos - Car Rental', false, false, '[ 16-Septiembre-2014 15:53:19 ]  Equipo ITbid :   añadido Sixt
', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-29 15:32:50', NULL, NULL, '2014-09-22 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, true, true, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (121, 1, 2, 2, '121', 'PROFESIONAL SERVICES ASSISTANCE ON GLOBAL INVESTMENT STRATEGY - PORTUGAL', '2017-09-14 09:18:50', '2017-09-29 16:00:00', false, true, true, false, 21, true, NULL, NULL, 'All documents and actual business induction will be deliver to the selected companies. The B2B interviews will be arranged by CARGLASS®.', 'See Business_Extension_Briefing.pdf attached.', false, false, 1, 1, 'The purpose of this request is the selection of a partner / company who will  support CARGLASS® PORTUGAL and BELRON® (from now on the ”Customer”) on their global investment strategy within the Portuguese market, including but not limited to the evaluation of the business potential and to assist on the development of the right operating model for his business extension.

Please , read carrefully attached Briefing and RFP Terms.', false, NULL, NULL, '2017-09-08 10:05:14', '2017-09-14 09:18:50', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 3, NULL, NULL, 0, NULL, 'Consultoría', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-19 08:54:25', NULL, NULL, '2017-09-29 12:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, true, true, false, 14, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (110, 1, 2, 2, 'CFC010', 'CFC010 Servicios de Mensajería y Paqueteria', '2016-12-30 12:11:04', '2017-01-09 18:30:00', false, true, true, false, 10, true, '2018-12-01 00:00:00', '2018-11-30 00:00:00', NULL, NULL, NULL, false, 0, 1, 'LICITACIÓN PARA LA SELECCIÓN DEL PROVEEDOR DEL SERVICIO DE MENSAJERÍA Y PAQUETERIA.

VER FICHEROS ADJUNTOS:

* Pliego Licitación Servicios de Mensajería. pdf
*Excel Cotización. xlsx 
LEER DETENIDAMENTE EL PLIEGO DE CONDICIONES ADJUNTO.
Las ofertas deben incluir, como archivos anexos:
* Documento con su oferta técnica y económica completa', false, NULL, 'En caso de dudas sobre las condiciones o servicios de esta licitación, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

Maricarmen Luzon
Project Manager 
mcluzon@itbid.com
636 10 93 45', '2016-12-30 12:11:04', '2016-12-30 12:32:53', NULL, 2, 2, NULL, NULL, NULL, true, 0, 'TBC', 3, 'N/A', NULL, 0, NULL, 'Mensajeria', false, false, '[ 16-Septiembre-2014 15:53:19 ]  Equipo ITbid :   añadido Sixt
', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-25 16:47:23', NULL, NULL, '2014-09-22 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, true, true, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (113, 1, 2, 2, 'CFC003', 'CFC003 SUMINISTRO MATERIAL DE OFICINA Y PAPEL FASE II', '2017-03-21 17:44:38', '2017-03-23 21:00:00', false, true, true, false, 20, true, '2018-12-01 00:00:00', '2018-11-30 00:00:00', NULL, NULL, NULL, false, 0, 1, 'El objeto de esta licitación es determinar los proveedores y condiciones óptimas para la prestación de diversos servicios de SUMINISTRO DE MATERIAL DE OFICINA Y PAPEL de CARGLASS, para el periodo 1 de Abril de 2017 a finales de Marzo 2019 (24 meses), con renovación anual.

LEER DETENIDAMENTE EL PLIEGO DE CONDICIONES ADJUNTO.

Para la oferta, se deberá descargar, cumplimentar según instrucciones y subir a la plataforma el Excel de Cotización.

IMPORTANTE:
EN ESTA FASE LA OFERTA PRESENTADA DEBERÁ SER A TRAVÉS DE CENTRO ESPECIAL DE EMPLEO.
DEBERÁN PRESENTAR:
-	Cual es su Pedido mínimo sin gastos de envío.
-	Plataforma web para la solicitud de pedidos.
-	Plazo de entrega estimado.
-       Ultimo estado de cuentas.', false, NULL, 'En caso de dudas sobre las condiciones o servicios de esta licitación, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

Maricarmen Luzon
Project Manager 
mcluzon@itbid.com
636 10 93 45', '2017-03-21 17:44:38', '2017-03-21 18:25:25', NULL, 2, 2, NULL, NULL, NULL, true, 0, 'TBC', 1, 'N/A', NULL, 0, NULL, 'CF07-5 - Material de Oficina', false, false, '[ 16-Septiembre-2014 15:53:19 ]  Equipo ITbid :   añadido Sixt
', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-29 09:36:49', NULL, NULL, '2014-09-22 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, true, true, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (112, 1, 2, 2, '112', 'CFC003 SUMINISTRO MATERIAL DE OFICINA Y PAPEL', '2017-03-09 15:20:33', '2017-03-16 15:00:00', false, true, true, false, 20, true, '2018-12-01 00:00:00', '2018-11-30 00:00:00', NULL, NULL, NULL, false, 0, 1, 'El objeto de esta licitación es determinar los proveedores y condiciones óptimas para la prestación de diversos servicios de SUMINISTRO DE MATERIAL DE OFICINA Y PAPEL de CARGLASS, para el periodo 1 de Abril de 2017 a finales de Marzo 2019 (24 meses), con renovación anual.

LEER DETENIDAMENTE EL PLIEGO DE CONDICIONES ADJUNTO.

Para la oferta, se deberá descargar, cumplimentar según instrucciones y subir a la plataforma el Excel de Cotización.

IMPORTANTE:
IMPORTANTE: EL PROVEEDOR DEBERÁ OFERTAR EXACTAMENTE LO SOLICITADO EN EL EXCEL DE COTIZACIÓN.
Ejemplo: CAJA 12 LAPICES FORAY HB
EL PROVEEDOR DEBERÁ OFERTAR 12 LAPICES FORAY O SIMILAR, PUEDE SER MARCA BLANCA, PERO EN NINGÚN CASO PODRÁ OFERTAR EL PRECIO UNITARIO DE 1 LÁPIZ, O UN EMBALAJE DIFERENTE AL SOLICITADO
EN CASO DE QUE EL PROVEEDOR SUMINISTRE EL MATERIAL EN CAJAS DE 10, TENDRÁ QUE HACER EL CALCULO PARA 12 LAPICES Y EN LA COLUMNA “*UM ALTERNATIVA” INDICAR EL EMBALAJE DEL ARTÍCULO.', false, NULL, 'En caso de dudas sobre las condiciones o servicios de esta licitación, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

Maricarmen Luzon
Project Manager 
mcluzon@itbid.com
636 10 93 45', '2017-03-09 15:20:33', '2017-03-10 12:16:09', NULL, 2, 2, NULL, NULL, NULL, true, 0, 'TBC', 1, 'N/A', NULL, 0, NULL, 'CF07-5 - Material de Oficina', false, false, '[ 16-Septiembre-2014 15:53:19 ]  Equipo ITbid :   añadido Sixt
', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-21 17:43:38', NULL, NULL, '2014-09-22 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, true, true, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (111, 1, 2, 2, 'CFC003 - MATERIAL OFICINA', 'CFC003 - SUMINISTRO MATERIAL DE OFICINA Y PAPEL', '2017-02-15 10:51:38', '2017-03-15 10:51:00', false, false, false, false, 20, false, '2017-04-14 00:00:00', '2017-03-10 00:00:00', NULL, NULL, false, false, 1, 1, 'El objeto de esta licitación es determinar los proveedores y condiciones óptimas para la prestación de diversos servicios de SUMINISTRO DE MATERIAL DE OFICINA Y PAPEL de CARGLASS, para el periodo 1 de Abril de 2017 a finales de Abril de 2019 (24 meses), con renovación anual.

LEER DETENIDAMENTE EL PLIEGO DE CONDICIONES ADJUNTO.

Para la oferta, se deberá descargar, cumplimentar según instrucciones y subir a la plataforma el Excel de Cotización.

IMPORTANTE:
IMPORTANTE: EL PROVEEDOR DEBERÁ OFERTAR EXACTAMENTE LO SOLICITADO EN EL EXCEL DE COTIZACIÓN.
Ejemplo: CAJA 12 LAPICES FORAY HB
EL PROVEEDOR DEBERÁ OFERTAR 12 LAPICES FORAY O SIMILAR, PUEDE SER MARCA BLANCA, PERO EN NINGÚN CASO PODRÁ OFERTAR EL PRECIO UNITARIO DE 1 LÁPIZ, O UN EMBALAJE DIFERENTE AL SOLICITADO
EN CASO DE QUE EL PROVEEDOR SUMINISTRE EL MATERIAL EN CAJAS DE 10, TENDRÁ QUE HACER EL CALCULO PARA 12 LAPICES Y EN LA COLUMNA “*UM ALTERNATIVA” INDICAR EL EMBALAJE DEL ARTÍCULO.', false, NULL, 'En caso de dudas sobre las condiciones del servicio o este pliego, los Sres. Proveedores deben entrarlas a través del “Centro de Mensajes” de la propia negociación electrónica. Las dudas serán atendidas a la mayor brevedad posible.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa al uso de la plataforma de negociación ITBID, por favor contactar con:

Maricarmen Luzon
Móvil 636 109 345
Telf. 936 345 009
mcluzon@itbid.com', '2017-02-15 10:51:38', NULL, NULL, 2, 2, NULL, NULL, NULL, false, 0, NULL, 4, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 0, NULL, 'Material de Oficina', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-15 10:51:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, 80, 'sumatorio', true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (125, 1, 2, 2, 'CFC-02-4-4', 'SERVICIO DE TRANSPORTE ENTRE LOS ALMACENES CENTRALES CFC-02-4-4', '2017-10-10 17:18:42', '2017-10-31 14:00:00', false, true, true, false, 9, true, '2019-12-31 00:00:00', '2018-01-01 00:00:00', NULL, NULL, NULL, false, 0, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación del servicio de TRANSPORTE ENTRE LOS ALMACENES CENTRALES DE CARGLASS DE CIEMPOZUELOS (MADRID) Y MOLLET (BARCELONA), a partir del día 1 de Enero de 2018 hasta el 31 de Diciembre de 2019.

El material a trasladar, será material propiedad de CARGLASS y su destino será sus propios almacenes en su modalidad de larga distancia.

CARGLASS garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociación y concurrencia de ofertas en igualdad de condiciones.

Vean en Ficheros Adjuntos:
antes 9:00h. Y entregas puntuales palet promociones origen Ciempozuelos entregas talleres.

•	ENTREGAS LARGA DISTANCIA ENTRE HUBS: ANEXO V

Para cumplimentar una propuesta:

* Rellene los casilleros de la propia web
* Adjunte cumplimentado los diferentes Anexos, según corresponda
* Adjunte el resto de documentos e información solicitados', false, NULL, 'En caso de dudas sobre las condiciones del servicio, los Srs. Proveedores deben enviarlas a través del “Centro de Mensajes” de la propia negociación electrónica.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:


Maricarmen Luzon

Móvil 636 10 93 45
Telf.: 933 218 614
mcluzon@itbid.org', '2017-10-10 10:19:21', '2017-10-10 17:18:42', NULL, 2, 2, NULL, NULL, NULL, true, 0, '30 días Transferencia', 1, 'NO APLICA', NULL, 0, NULL, 'Logistica', false, false, '[ 11-Julio-2013 13:22:25 ]  Joan Castella :   Añadidos 9 proveedores - MAIL2
[ 15-Julio-2013 10:05:37 ]  Joan Castella :   Añadidos 2 proveedores
[ 15-Julio-2013 10:22:02 ]  Jose Manuel Iáñez :   mail3
[ 15-Julio-2013 11:07:02 ]  Jose Manuel Iáñez :   mail3
[ 18-Julio-2013 16:48:42 ]  Jose Manuel Iáñez :   Añadido Truck 
[ 18-Julio-2013 19:28:42 ]  Jose Manuel Iáñez :   añadido DHL
[ 02-Agosto-2013 11:38:32 ]  Jose Manuel Iáñez :   Añadido Barbera
', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-29 11:35:44', NULL, NULL, '2013-09-02 23:55:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, 13, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (161, 1, 818, 16, '161', 'CFC-039 II - Suministro de maquinaria almacen', '2019-02-11 15:46:05', '2019-03-01 12:10:00', false, true, true, false, 2, true, NULL, NULL, NULL, NULL, false, false, 1, 1, 'El objeto de esta negociación es determinar el proveedor y condiciones óptimas para el alquiler, con todos los servicios incluidos, de carretillas y vehículos para manipulación y almacenaje para el almacén de CARGLASS ubicado en Ciempozuelos (Madrid).

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa al uso de la plataforma de negociación ITBID, por favor contactar con:

Mario Lázaro Cerván
Móvil 676 953 333
Telf. 933 218 614
mlazaro@itbid.com', false, NULL, NULL, '2019-02-11 14:36:01', '2019-02-11 15:46:05', NULL, 16, 16, NULL, NULL, NULL, true, NULL, NULL, 1, 'Hub Ciempozuelos: Calle de las Moreras, 28350 Ciempozuelos, Madrid.', NULL, NULL, NULL, 'Carretillas', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-20 18:29:24', NULL, NULL, '2019-01-31 16:31:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (165, 1, 818, 16, '165', 'FASE CIERRE - LOTE 1 - CFC-039 II - Suministro de maquinaria almacen', '2019-03-20 20:07:35', '2019-03-20 20:07:35', false, false, false, false, 24, false, '2025-05-31 00:00:00', '2019-05-20 00:00:00', NULL, NULL, true, false, 2, 1, '<p>El objeto de esta negociaci&oacute;n es determinar el proveedor y condiciones &oacute;ptimas para el alquiler, con todos los servicios incluidos, de carretillas LOTE 1 - PREPARADOR DE PEDIDOS.</p>

<p>Estas Reglas regulan la fase final, de mejora de ofertas, del concurso iniciado mediante petici&oacute;n de propuestas (Ref. CARGLASS-FC039-II) y complementan, pero no anulan, el Pliego de Condiciones de la fase precedente (ver pliego adjunto&nbsp;REV01).</p>

<p>&nbsp;En caso de dudas sobre c&oacute;digos de acceso, introducci&oacute;n de ofertas o cualquier cuesti&oacute;n relativa al uso de la plataforma de negociaci&oacute;n ITBID, por favor contactar con: Mario L&aacute;zaro Cerv&aacute;n - M&oacute;vil 676 953 333 Telf. 933 218 614 mlazaro@itbid.com o Josep Puig ,&nbsp;M&oacute;vil 655347607 - jpuig@itbid.com</p>', false, NULL, NULL, '2019-03-20 20:07:35', '2019-03-20 19:51:55', NULL, 16, 16, NULL, 310000, NULL, false, 0, '- -', 8, 'Hub Ciempozuelos: Calle de las Moreras, 28350 Ciempozuelos, Madrid.', NULL, 0, 'DDP - Ciempozuelos', 'Alquiler Maquinaria', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, 0, 9, NULL, NULL, NULL, '2019-03-25 16:05:00', NULL, 492000, 3, NULL, 0, 0, false, false, 0, 0, 0, 3, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, 'cantidad', 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (163, 1, 818, 16, 'CFC-039 - FASE CIERRE - LOTE 1', 'FASE CIERRE - LOTE 1 - CFC-039 II - Suministro de maquinaria almacen', '2019-03-25 15:45:00', '2019-03-25 15:56:01', false, true, true, false, 24, true, '2025-05-31 00:00:00', '2019-05-20 00:00:00', NULL, NULL, true, false, 2, 1, '<p>El objeto de esta negociaci&oacute;n es determinar el proveedor y condiciones &oacute;ptimas para el alquiler, con todos los servicios incluidos, de carretillas LOTE 1 - PREPARADOR DE PEDIDOS.</p>

<p>Estas Reglas regulan la fase final, de mejora de ofertas, del concurso iniciado mediante petici&oacute;n de propuestas (Ref. CARGLASS-FC039-II) y complementan, pero no anulan, el Pliego de Condiciones de la fase precedente (ver pliego adjunto&nbsp;REV01).</p>

<p>&nbsp;En caso de dudas sobre c&oacute;digos de acceso, introducci&oacute;n de ofertas o cualquier cuesti&oacute;n relativa al uso de la plataforma de negociaci&oacute;n ITBID, por favor contactar con: Mario L&aacute;zaro Cerv&aacute;n - M&oacute;vil 676 953 333 Telf. 933 218 614 mlazaro@itbid.com o Josep Puig ,&nbsp;M&oacute;vil 655347607 - jpuig@itbid.com</p>', false, NULL, NULL, '2019-03-20 18:30:11', '2019-03-20 19:51:55', NULL, 16, 16, NULL, 310000, NULL, true, 0, '- -', 8, 'Hub Ciempozuelos: Calle de las Moreras, 28350 Ciempozuelos, Madrid.', NULL, 0, 'DDP - Ciempozuelos', 'Alquiler Maquinaria', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, 0, 9, '2019-05-10 12:18:15', NULL, NULL, '2019-03-25 16:05:00', NULL, 492000, 3, NULL, 0, 0, false, false, 0, 0, 0, 3, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, 'cantidad', 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (166, 1, 818, 16, '166', 'FASE CIERRE - LOTE 1 - CFC-039 II - Suministro de maquinaria almacen', '2019-03-20 20:08:19', '2019-03-20 20:08:19', false, false, false, false, 24, false, '2025-05-31 00:00:00', '2019-05-20 00:00:00', NULL, NULL, true, false, 2, 1, '<p>El objeto de esta negociaci&oacute;n es determinar el proveedor y condiciones &oacute;ptimas para el alquiler, con todos los servicios incluidos, de carretillas LOTE 1 - PREPARADOR DE PEDIDOS.</p>

<p>Estas Reglas regulan la fase final, de mejora de ofertas, del concurso iniciado mediante petici&oacute;n de propuestas (Ref. CARGLASS-FC039-II) y complementan, pero no anulan, el Pliego de Condiciones de la fase precedente (ver pliego adjunto&nbsp;REV01).</p>

<p>&nbsp;En caso de dudas sobre c&oacute;digos de acceso, introducci&oacute;n de ofertas o cualquier cuesti&oacute;n relativa al uso de la plataforma de negociaci&oacute;n ITBID, por favor contactar con: Mario L&aacute;zaro Cerv&aacute;n - M&oacute;vil 676 953 333 Telf. 933 218 614 mlazaro@itbid.com o Josep Puig ,&nbsp;M&oacute;vil 655347607 - jpuig@itbid.com</p>', false, NULL, NULL, '2019-03-20 20:08:19', '2019-03-20 19:51:55', NULL, 16, 16, NULL, 310000, NULL, false, 0, '- -', 8, 'Hub Ciempozuelos: Calle de las Moreras, 28350 Ciempozuelos, Madrid.', NULL, 0, 'DDP - Ciempozuelos', 'Alquiler Maquinaria', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, 0, 9, NULL, NULL, NULL, '2019-03-25 16:05:00', NULL, 492000, 3, NULL, 0, 0, false, false, 0, 0, 0, 3, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, 'cantidad', 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (167, 1, 818, 16, '167', NULL, '2019-03-20 21:12:03', '2019-03-20 21:12:03', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2019-03-20 21:12:03', NULL, NULL, 16, 16, NULL, NULL, NULL, false, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (162, 1, 818, 16, '162', 'CFC - 038A - Recogida y Gestión de Distintos Residuos.', '2019-02-19 15:37:40', '2019-02-28 20:00:00', false, true, true, false, 17, true, NULL, NULL, NULL, NULL, false, false, 1, 1, '<p>El objeto de esta negociaci&oacute;n es determinar los proveedores y condiciones &oacute;ptimas para la prestaci&oacute;n de diversos servicios de Recogida y Gesti&oacute;n de Residuos de los diferentes centros de Carglass&reg;.&nbsp;</p>

<p>Carglass&reg; garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociaci&oacute;n y concurrencia de ofertas en igualdad de condiciones.</p>

<p>Ver adjunto:&nbsp;</p>

<p>*Pliego_Condiciones_CFC038A</p>

<p>*Manual_Proveedor</p>

<p>*Lista_Informaci&oacute;n_Centros</p>

<p><u><strong>Lotes:</strong></u></p>

<ul>
	<li><u><strong>LOTE 1:</strong></u>&nbsp;Recogida y Gesti&oacute;n de Residuos Peligrosos. LER (08 04 09*, 15 01 10* y 15 02 02*)</li>
	<li><u><strong>LOTE 2:</strong></u>&nbsp;Envases de Madera. LER (15 01 03 y 20 01 38)</li>
	<li><u><strong>LOTE 3:</strong></u>&nbsp;Residuo de Vidrio. LER (16 01 20)</li>
	<li><u><strong>LOTE 4:</strong></u>&nbsp;Envases de Papel y Cart&oacute;n. LER (15 01 01)</li>
	<li><u><strong>LOTE 5:</strong></u>&nbsp;Residuos de Pl&aacute;stico. LER (20 01 39)</li>
	<li><u><strong>LOTE 6:</strong></u>&nbsp; Residuo Banal. LER (20 01 39)</li>
	<li><strong><u>SERVICIO ADICIONAL 1:</u></strong>&nbsp;Residuos de Pl&aacute;stico. LER (16 01 19).</li>
	<li><u><strong>SERVICIO ADICIONAL 2:</strong></u> Residuo de Toner de impresi&oacute;n. LER (08 03 18).</li>
</ul>

<p>No es obligatorio cotizar ni todos los lotes ni todos los centros de cada lote.</p>

<p class="MsoFooter"><span style="tab-stops:35.4pt">En caso de dudas sobre c&oacute;digos de acceso, introducci&oacute;n de ofertas o cualquier cuesti&oacute;n relativa al uso de la plataforma de negociaci&oacute;n ITBID, por favor contactar con:</span></p>

<p class="MsoFooter">&nbsp;</p>

<table class="Table" style="border-collapse:collapse; border:undefined">
	<tbody>
		<tr>
			<td style="width:235.05pt" width="313">
			<p class="MsoFooter" style="padding:0cm 5.4pt 0cm 5.4pt"><span style="tab-stops:171.0pt">Mario L&aacute;zaro&nbsp;</span></p>

			<p class="MsoFooter" style="padding:0cm 5.4pt 0cm 5.4pt"><span style="tab-stops:171.0pt">M&oacute;vil 676 953 333</span></p>

			<p class="MsoFooter" style="padding:0cm 5.4pt 0cm 5.4pt"><span style="tab-stops:171.0pt">Telf. 933 218 614</span></p>

			<p class="MsoFooter" style="padding:0cm 5.4pt 0cm 5.4pt"><span style="tab-stops:171.0pt"><a href="mailto:mlazaro@itbid.com">mlazaro@itbid.com</a></span></p>
			</td>
			<td style="width:235.1pt" width="313">
			<p class="MsoFooter" style="padding:0cm 5.4pt 0cm 5.4pt">&nbsp;</p>
			</td>
		</tr>
	</tbody>
</table>', false, NULL, NULL, '2019-02-19 12:31:46', '2019-02-19 15:37:40', NULL, 16, 16, NULL, NULL, NULL, true, NULL, NULL, 3, 'Ver lista de centros', NULL, NULL, NULL, 'Recogida Residuos', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-21 12:45:54', NULL, NULL, '2019-02-19 12:31:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (170, 1, 818, 16, '170', 'FASE CIERRE - LOTE 1 - CFC-039 II - Suministro de maquinaria almacen', '2019-03-21 18:02:22', '2019-03-21 18:02:22', false, false, false, false, 24, false, '2025-05-31 00:00:00', '2019-05-20 00:00:00', NULL, NULL, true, false, 2, 1, '<p>El objeto de esta negociaci&oacute;n es determinar el proveedor y condiciones &oacute;ptimas para el alquiler, con todos los servicios incluidos, de carretillas LOTE 1 - PREPARADOR DE PEDIDOS.</p>

<p>Estas Reglas regulan la fase final, de mejora de ofertas, del concurso iniciado mediante petici&oacute;n de propuestas (Ref. CARGLASS-FC039-II) y complementan, pero no anulan, el Pliego de Condiciones de la fase precedente (ver pliego adjunto&nbsp;REV01).</p>

<p>&nbsp;En caso de dudas sobre c&oacute;digos de acceso, introducci&oacute;n de ofertas o cualquier cuesti&oacute;n relativa al uso de la plataforma de negociaci&oacute;n ITBID, por favor contactar con: Mario L&aacute;zaro Cerv&aacute;n - M&oacute;vil 676 953 333 Telf. 933 218 614 mlazaro@itbid.com o Josep Puig ,&nbsp;M&oacute;vil 655347607 - jpuig@itbid.com</p>', false, NULL, NULL, '2019-03-21 18:02:22', '2019-03-20 19:51:55', NULL, 16, 16, NULL, 310000, NULL, false, 0, '- -', 8, 'Hub Ciempozuelos: Calle de las Moreras, 28350 Ciempozuelos, Madrid.', NULL, 0, 'DDP - Ciempozuelos', 'Alquiler Maquinaria', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, 0, 9, NULL, NULL, NULL, '2019-03-25 16:05:00', NULL, 492000, 3, NULL, 0, 0, false, false, 0, 0, 0, 3, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, 'cantidad', 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (171, 1, 818, 16, '171', 'FASE CIERRE - LOTE 1 - CFC-039 II - Suministro de maquinaria almacen', '2019-03-21 18:03:54', '2019-03-21 18:03:54', false, false, false, false, 24, false, '2025-05-31 00:00:00', '2019-05-20 00:00:00', NULL, NULL, true, false, 2, 1, '<p>El objeto de esta negociaci&oacute;n es determinar el proveedor y condiciones &oacute;ptimas para el alquiler, con todos los servicios incluidos, de carretillas LOTE 1 - PREPARADOR DE PEDIDOS.</p>

<p>Estas Reglas regulan la fase final, de mejora de ofertas, del concurso iniciado mediante petici&oacute;n de propuestas (Ref. CARGLASS-FC039-II) y complementan, pero no anulan, el Pliego de Condiciones de la fase precedente (ver pliego adjunto&nbsp;REV01).</p>

<p>&nbsp;En caso de dudas sobre c&oacute;digos de acceso, introducci&oacute;n de ofertas o cualquier cuesti&oacute;n relativa al uso de la plataforma de negociaci&oacute;n ITBID, por favor contactar con: Mario L&aacute;zaro Cerv&aacute;n - M&oacute;vil 676 953 333 Telf. 933 218 614 mlazaro@itbid.com o Josep Puig ,&nbsp;M&oacute;vil 655347607 - jpuig@itbid.com</p>', false, NULL, NULL, '2019-03-21 18:03:54', '2019-03-20 19:51:55', NULL, 16, 16, NULL, 310000, NULL, false, 0, '- -', 8, 'Hub Ciempozuelos: Calle de las Moreras, 28350 Ciempozuelos, Madrid.', NULL, 0, 'DDP - Ciempozuelos', 'Alquiler Maquinaria', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, 0, 9, NULL, NULL, NULL, '2019-03-25 16:05:00', NULL, 492000, 3, NULL, 0, 0, false, false, 0, 0, 0, 3, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, 'cantidad', 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (123, 1, 2, 2, 'CFC024-1', 'TRANSPORTE PROGRAMADO', '2017-10-10 10:16:04', '2018-01-17 08:15:00', false, true, true, false, 9, true, '2020-02-29 00:00:00', '2018-03-01 00:00:00', NULL, NULL, NULL, false, 1, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación del servicio de TRANSPORTE Y DISTRIBUCIÓN PROGRAMADA DE PRODUCTOS Y ACCESORIOS DESDE LOS ALMACENES DE CARGLASS A SUS CENTROS, a partir del día 1 de marzo de 2018 hasta el 28 de Febrero de 2020.

Los Sres Licitadores pueden seleccionar presentar su propuesta para uno o varios de los lotes.

Leer detenidamente el Pliego de Condiciones adjunto.

Para presentar una propuesta:

* Puede hacerlo "a mano", aunque le sugerimos descargar y cumplimentar un Excel (Ver adjunto "INSTRUCTIVO DESCARGAR Y COTIZAR EXCEL.PDF"
* Adjunte el resto de documentos e información solicitados en el Pliego de Condiciones', false, NULL, 'Las dudas sobre los contenidos o alcance de este pliego y/o sobre el proceso de negociación en general, deberán canalizarse exclusivamente a través del CENTRO DE MENSAJES de la plataforma.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:


Josep Puig

Móvil 655347607
Telf.: 933 218 614
jpuig@itbid.com

Jose Manuel Iañez

Móvil 676953333
Telf.: 933 218 614
jpuig@itbid.com', '2017-09-29 18:13:53', '2017-10-10 10:16:04', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 4, 'NO PROCEDE --', NULL, 999, NULL, 'Logistica', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-25 11:28:37', 'LAS CANTIDADES HAN SIDO MULTIPLICADAS POR UN FACTOR 2,27 PARA OBTENER EL SUBTOTAL A PARTIR DE PRECIOS SUMINISTRADOS', NULL, '2017-10-10 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, 13, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, 'ponderado', false, 3, 3, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (150, 1, 818, 16, 'CFC036-Fase II', 'CFC036 -FASE II - LICITACION SERVICIO DE LIMPIEZA HUBS Y RED DE CENTROS', '2018-11-16 16:50:00', '2018-11-20 23:55:00', false, true, true, false, 19, true, NULL, NULL, NULL, NULL, false, false, 2, 1, 'El objeto de esta negociación es determinar los proveedores y condiciones óptimas para la prestación de diversos servicios de LIMPIEZA de los diferentes Hubs y Red de Centros de Carglass para el periodo  diciembre de 2018 a diciembre  2020 (24 meses).

Ver adjuntos.

Para cotizar, se debe descargar, cumplimentar y devolver el Excel "Tabla Cotización Limpieza".', false, NULL, 'En caso de dudas sobre las condiciones del servicio o este pliego, los Sres. Proveedores deben comunicarlas a través del “Centro de Mensajes” de la propia negociación electrónica. Las dudas serán atendidas a la mayor brevedad posible.

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa al uso de la plataforma de negociación ITBID, por favor contactar con:

Mario Lázaro
Móvil 676 953 333
Telf. 933 218 614
mlazaro@itbid.com', '2018-11-16 14:31:03', '2018-11-16 16:24:53', NULL, 16, 16, NULL, NULL, NULL, true, 0, NULL, 5, 'No aplica - El servicio se realizará en cada centro.', NULL, NULL, NULL, 'Limpieza', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-25 11:30:07', NULL, NULL, '2018-11-08 18:21:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (152, 1, 818, 16, '152', 'CFC037 - Central de Alarmas', '2018-12-04 10:43:41', '2018-12-23 23:55:00', false, true, true, false, 18, true, '2018-12-31 00:00:00', '2018-12-04 00:00:00', NULL, NULL, NULL, false, 1, 1, 'El objeto de esta negociación es la prestación del servicio de Central de Alarmas y otros servicios, para la Red de Centros y Hubs de Carglass en España hasta 31 de diciembre de 2020

CARGLASS  garantiza a los proveedores seleccionados la transparencia de la negociación y concurrencia de ofertas en igualdad de condiciones.

Ver Documentación  Adjunta:
*Guia_Rápida_RFQ_RFP_Proveedor_ES.pdf
*Oferta_AC_2019.xlsx
*Anexo_I.xlsx
*Pliego_de_condiciones_Servicio_Alarmas_CFC037.pdf

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

MARIO LÁZARO
ITBID
Móvil +34 676 953 333
mlazaro@itbid.com', false, NULL, 'En caso de dudas sobre las condiciones de esta licitación o sobre características de los equipos, los licitadores deben entrarlas exclusivamente a través del “Centro de Mensajes” de la propia negociación electrónica. 

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

MARIO LÁZARO 
ITBID 
Móvil +34 676 953 333
mlazaro@itbid.com', '2018-12-03 17:45:46', '2018-12-04 10:43:41', NULL, 16, 16, NULL, NULL, NULL, true, 0, NULL, 1, 'NO PROCEDE - VER PLEIGO DE CONDICIONES', NULL, 999, NULL, 'Serv Seguridad / Central de Alarmas', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-25 11:40:09', NULL, NULL, '2018-12-17 12:10:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, 3, 5, 20, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (158, 1, 818, 16, 'CFC038 A', 'CFC038 A -  REGOGIDA Y GESTIÓN DE RESIDUO DE VIDRIO', '2019-01-16 10:43:50', '2019-03-25 08:00:00', false, true, true, false, 17, true, NULL, NULL, NULL, NULL, false, false, 1, 1, 'Se requiere que para la gestión de residuo de vidrio (16 01 20) se cotice la recogida y gestión para dos escenarios: Escenario Actual y Escenario Futuro.

Revisar documentación adjunta:
*Guia Rapida RFQ RFP Proveedor
*CFC038 - A - Pliego de Condiciones

En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITBID, por favor contactar con:

MARIO LÁZARO
ITBID
Móvil +34 676 953 333
mlazaro@itbid.com', false, NULL, NULL, '2019-01-16 09:19:37', '2019-01-16 10:43:50', NULL, 16, 16, NULL, NULL, NULL, true, NULL, NULL, 3, 'ESCENARIO ACTUAL:
*PUNTO DE RECOGIDA: Hub Ciempozuelos, Calle de las Moreras, 151, 28350 Ciempozuelos, Madrid.

ESCENARIO FUTURO:
*PUNTO DE RECOGIDA 1: CAT Logística Cargo - Península Ibérica. C/ Francisco Rabal, nº1
28806 - Alcalá de Henares (Madrid)

*PUNTO DE RECOGIDA 2: Truck & Wheel Pamplona. Pol. Arazuri-Orcoyen. Calle E, parcela 2-3, 31170 Arazuri, Navarra.

*PUNTO DE RECOGIDA 3: Truck & Wheel Guadalajara.Camino Alovera nº 5, 19171 Cabanillas del Campo, Guadalajara.

*PUNTO DE RECOGIDA 4: Hub Ciempozuelos, Calle de las Moreras, 151, 28350 Ciempozuelos, Madrid.', NULL, NULL, NULL, 'Recogida Residuos', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-25 11:48:49', NULL, NULL, '2019-01-16 09:19:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (169, 1, 818, 16, 'CFC-039 - FASE CIERRE - LOTE 2', 'FASE CIERRE - LOTE 2 - CARRETILLA RETRÁCTIL - CFC-039 II - Suministro de maquinaria almacen', '2019-03-25 16:20:00', '2019-03-25 16:35:00', false, true, true, false, 2, true, '2025-03-31 00:00:00', '2019-05-27 00:00:00', NULL, NULL, true, false, 2, 1, '<p style="margin-top:0cm; margin-right:0cm; margin-bottom:7.5pt; margin-left:0cm"><span style="background:white">El objeto de esta negociaci&oacute;n es determinar el proveedor y condiciones &oacute;ptimas para el alquiler, con todos los servicios incluidos, de carretillas LOTE 2 - CARRETILLA RETR&Aacute;CTIL</span></p>

<p style="margin-top:0cm; margin-right:0cm; margin-bottom:7.5pt; margin-left:0cm; text-align:start; -webkit-text-stroke-width:0px"><span style="background:white"><span style="font-variant-ligatures:normal"><span style="font-variant-caps:normal"><span style="orphans:2"><span style="widows:2"><span style="text-decoration-style:initial"><span style="text-decoration-color:initial"><span style="word-spacing:0px">Estas Reglas regulan la fase final, de mejora de ofertas, del concurso iniciado mediante petici&oacute;n de propuestas (Ref. CARGLASS-FC039-II) y complementan, pero no anulan, el Pliego de Condiciones de la fase precedente (ver pliego adjunto REV01).</span></span></span></span></span></span></span></span></p>

<p style="margin-top:0cm; margin-right:0cm; margin-bottom:7.5pt; margin-left:0cm; text-align:start; -webkit-text-stroke-width:0px"><span style="background:white"><span style="font-variant-ligatures:normal"><span style="font-variant-caps:normal"><span style="orphans:2"><span style="widows:2"><span style="text-decoration-style:initial"><span style="text-decoration-color:initial"><span style="word-spacing:0px">En caso de dudas sobre c&oacute;digos de acceso, introducci&oacute;n de ofertas o cualquier cuesti&oacute;n relativa al uso de la plataforma de negociaci&oacute;n ITBID, por favor contactar con: Mario L&aacute;zaro Cerv&aacute;n - M&oacute;vil 676 953 333 Telf. 933 218 614 mlazaro@itbid.com o Josep Puig , M&oacute;vil 655347607 - jpuig@itbid.com</span></span></span></span></span></span></span></span></p>', false, NULL, NULL, '2019-03-20 21:14:03', '2019-03-20 21:32:00', NULL, 16, 16, NULL, 0, NULL, true, 0, '- -', 10, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 0, 'DDP - Ciempozuelos', 'Carretillas', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, 8, '2019-05-10 12:13:31', NULL, 30, '2019-03-25 16:35:00', NULL, 0, 1, NULL, 0, 0, false, true, 0, 0, NULL, 6, false, false, -1, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, 'porcentaje', 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (149, 1, 818, 16, '149', 'CFC034 Colaboración Carglass - Calibrado Sistemas ADAS', '2018-11-15 18:19:47', '2019-04-30 23:55:00', false, false, false, true, 21, true, NULL, NULL, NULL, NULL, false, false, 1, 1, 'Actualmente Carglass®  se encuentra inmerso en un proyecto de desarrollo de una red de colaboradores prioritarios a nivel nacional. Como objetivo principal se quiere establecer relaciones de colaboración entre ambas partes que dé solución a distintos servicios y suministros requeridos.

Una de las necesidades que se presentan a día de hoy es conseguir proveedores que presten el  servicio de calibración de los sistemas ADAS de distintos vehículos.', false, NULL, NULL, '2018-11-13 18:09:35', '2018-11-15 18:19:47', NULL, 16, 16, NULL, NULL, NULL, true, NULL, NULL, 3, 'Ver tabla de centros Carglass', NULL, NULL, NULL, 'Consulting / Consultoría', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-01 19:15:31', NULL, NULL, '2019-01-03 18:09:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'desierta', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (164, 1, 818, 16, 'CFC-039 - FASE CIERRE - LOTE 3', 'FASE CIERRE - LOTE 3 - CARRETILLA FRONTAL- CFC-039 II - Suministro de maquinaria almacen', '2019-03-25 15:15:00', '2019-03-25 15:29:22', false, true, true, false, 24, true, '2025-05-31 00:00:00', '2019-05-20 00:00:00', NULL, NULL, true, false, 2, 1, '<p>El objeto de esta negociaci&oacute;n es determinar el proveedor y condiciones &oacute;ptimas para el alquiler, con todos los servicios incluidos, de carretillas LOTE 3&nbsp;- CARRETILLA FRONTAL.</p>

<p>Estas Reglas regulan la fase final, de mejora de ofertas, del concurso iniciado mediante petici&oacute;n de propuestas (Ref. CARGLASS-FC039-II) y complementan, pero no anulan, el Pliego de Condiciones de la fase precedente (ver pliego adjunto&nbsp;REV01).</p>

<p>&nbsp;En caso de dudas sobre c&oacute;digos de acceso, introducci&oacute;n de ofertas o cualquier cuesti&oacute;n relativa al uso de la plataforma de negociaci&oacute;n ITBID, por favor contactar con: Mario L&aacute;zaro Cerv&aacute;n - M&oacute;vil 676 953 333 Telf. 933 218 614 mlazaro@itbid.com o Josep Puig ,&nbsp;M&oacute;vil 655347607 - jpuig@itbid.com</p>', false, NULL, NULL, '2019-03-20 20:01:38', '2019-03-20 20:21:14', NULL, 16, 16, NULL, 75500, NULL, true, 0, '- -', 8, 'Hub Ciempozuelos: Calle de las Moreras, 28350 Ciempozuelos, Madrid.', NULL, 0, 'DDP - Ciempozuelos', 'Alquiler Maquinaria', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, 0, 9, '2019-05-10 12:16:35', NULL, NULL, '2019-03-25 15:35:00', NULL, 114500, 3, NULL, 0, 0, false, false, 0, 0, 0, 3, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, 'cantidad', 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (172, 1, 49, 7, '172', NULL, '2019-05-16 13:05:23', '2019-05-16 13:05:23', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2019-05-16 13:05:23', NULL, NULL, 7, 7, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (6, 1, 19, 6, 'ALM-01-S1', ' SISTEMA DE ESTANTERÍAS METÁLICAS - MEJORA', '2012-09-27 11:00:00', '2012-09-27 11:30:00', false, true, true, false, 2, true, NULL, NULL, NULL, NULL, true, false, 2, 1, 'Fase de mejora a partir de las ofertas introducidas en la fase precedente.

VER EN FICHEROS ANEXOS:

* Reglas de la Negociación
* Borrador de contrato (igual anterior)
* Guía rápida de uso del Proveedor

La duración mínima de esta subasta es de 15 min, con un máximo de 30 min.

Las ofertas iniciales han sido introducidas, y están bloqueadas hasta el inicio de la subasta.
', false, NULL, 'En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa a la plataforma de negociación ITbid, por favor contactar con:

SR. D. JOSE MANUEL IÁÑEZ
ITBID TECHNOLOGIES
Móvil 676 953 333
jmianez@itbid.org
', '2012-09-26 12:15:52.813041', '2012-09-26 12:27:36.082309', NULL, 6, 2, NULL, NULL, NULL, true, 999, 'La establecida y pactada en contrato', 22, 'Ciempozuelos - Madrid
', 'EUR', 0, 'DDP - Ciempozuelos', 'Instalaciones', false, false, '', NULL, true, 2, '2012-10-11 13:23:20.376837', NULL, NULL, 0.25, 8, '2012-10-11 13:23:20.376837', NULL, 1, '2012-09-27 11:30:00', 15, NULL, NULL, 4, 2, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (173, 1, 49, 7, 'N-D1-001', 'Renting de Vehículos Turismo 2019 - Fase I', '2019-05-17 16:00:52', '2019-05-27 12:00:00', false, false, false, true, 25, true, NULL, NULL, NULL, NULL, false, false, 1, 1, '<p>El objeto de esta negociaci&oacute;n es determinar la contrataci&oacute;n de parte de la flota de veh&iacute;culos que CARGLASS&reg; dispone, a d&iacute;a de hoy en renting. El objeto a negociar son 30 veh&iacute;culos que vencen a final de a&ntilde;o y deben ser sustituidos.</p>

<p>La licitaci&oacute;n se ha configurado en sucesivas rondas; el objetivo de la primera ronda es elegir, dentro del rango de veh&iacute;culos que cada empresa de renting ofertar&aacute;, los m&aacute;s adecuados en base a precios generales de mercado y prestaciones.</p>

<p>CARGLASS&reg; garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociaci&oacute;n y concurrencia de ofertas en igualdad de condiciones.</p>

<p>Se a&ntilde;ade, en el Anexo I, el c&oacute;digo de conducta para proveedores de CARGLASS&reg; como integrante del Grupo Belron que debe ser aceptado por los proveedores para poder optar a la adjudicaci&oacute;n.</p>', false, NULL, NULL, '2019-05-16 13:25:57', '2019-05-17 16:00:52', NULL, 7, 7, NULL, NULL, NULL, true, NULL, NULL, 3, 'En el concesionario más cercano al puesto de trabajo de cada empleado que se facilitará mas adelante.', NULL, NULL, NULL, 'Renting', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-04 16:44:19', NULL, NULL, '2019-05-24 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, true, false, false, false, false, false, 'avanzada', 'desierta', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (176, 1, 2, 2, '176', 'Suministro energía eléctrica 2017-2019 - Fase de Cierre', '2019-06-26 17:41:02', '2019-06-26 17:41:02', false, false, false, false, 6, false, '2019-07-31 00:00:00', '2017-09-01 00:00:00', NULL, NULL, NULL, false, 2, 1, 'El objeto de esta negociación es determinar las comercializadoras y condiciones óptimas para el suministro de energía eléctrica en baja tensión para los centros y Hubs de CARGLASS BV SUCURSAL EN ESPAÑA que se detallan en el Excel de Cotización, por un consumo anual estimado de 4,5 GWH.

DESCARGAR FICHEROS ADJUNTOS:

* Pliego_Sobre_Cerrado_Fase_Cierre_.pdf:  Pliego condiciones de esta licitación
* Excel_cotizacion_2017.xlsx: datos de los centros y consumos, A SER CUMPLIMENTADO Y DEVUELTO EN LA PLATAFORMA


SE DEBEN COMPLETAR TODOS LOS CASILLEORS DE LA OFERTA, Y ANEXAR EL EXCEL DE COTIZACION COMPLETADO

Opcionalmente, los licitadores podrán incluir cuantos ficheros deseen.', false, NULL, 'Las dudas sobre los contenidos o alcance de este pliego y/o sobre el proceso de negociación en general, deberán canalizarse exclusivamente a través de plataforma ITBID.

Si algún licitador experimentara dificultades técnicas para el acceso a la plataforma, la entrada de la oferta o la subida de ficheros, deberá contactar con:

Maricarmen Luzon
ITBID Technologies
e-mail: mcluzon@itbid.com
Móv. 636 10 93 45
Tel. 933 218 614', '2019-06-26 17:41:02', '2017-07-24 15:38:01', NULL, 2, 2, NULL, NULL, NULL, false, 0, 'Ver pliego', 5, 'N/A', NULL, 999, 'N/A', 'Electricidad', false, false, '', NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-06-04 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (174, 1, 2, 2, 'K2-2019-01', 'Suministro energía eléctrica 2019-2022', '2019-05-31 07:06:55', '2019-06-14 18:00:00', false, true, true, false, 6, true, '2022-07-31 00:00:00', '2019-08-01 00:00:00', NULL, NULL, NULL, false, 2, 1, '<p>El objeto de esta negociaci&oacute;n es determinar las comercializadoras y condiciones &oacute;ptimas para el suministro de energ&iacute;a el&eacute;ctrica en baja tensi&oacute;n para los centros y Hubs de CARGLASS S.L. que se detallan en el Excel de Cotizaci&oacute;n, por un consumo anual estimado de 4,5 GWH.</p>

<p><strong>DESCARGAR FICHEROS ADJUNTOS</strong>:</p>

<ul>
	<li>Pliego_condiciones_RFQ_CFC007-2019-2.pdf: Pliego condiciones de esta licitaci&oacute;n&nbsp;</li>
	<li>ANEXO_1_CONSUMO_POTENCIA_CENTROS_2019:datos de los centros y consumos</li>
	<li>ANEXO_2_EXCEL_COTIZACI&Oacute;N_2019: A&nbsp;SER CUMPLIMENTADO Y DEVUELTO EN LA PLATAFORMA SE DEBEN ANEXAR A LA OFERTA</li>
</ul>

<p>OBLIGATORIAMENTE LOS SIGUIENTES FICHEROS:</p>

<ol>
	<li><strong>ANEXO_2_EXCEL_COTIZACI&Oacute;N_2019</strong>: El Excel de Cotizaci&oacute;n completado</li>
	<li><strong>Oferta t&eacute;cnica y econ&oacute;mica completa</strong>, incluyendo mejoras si las proponen</li>
	<li>Opcionalmente, los licitadores podr&aacute;n incluir cuantos ficheros deseen.</li>
</ol>', false, NULL, 'Las dudas sobre los contenidos o alcance de este pliego y/o sobre el proceso de negociación en general, deberán canalizarse exclusivamente a través de plataforma ITBID. 

Si algún licitador experimentara dificultades técnicas para el acceso a la plataforma, la entrada de la oferta o la subida de ficheros, deberá contactar con:

Josep Puig
ITBID Technologies
e-mail: jpuig@itbid.com
Tel. 933 218 614

Elton do Santos
ITBID Technologies
e-mail: edsantos@itbid.com
Tel. 933 218 614', '2019-05-28 11:38:20', '2019-05-31 07:06:55', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'Ver pliego', 1, 'N/A', NULL, 999, 'N/A', 'Electricidad', false, false, '', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-27 17:32:05', NULL, NULL, '2015-06-04 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, true, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (178, 1, 2, 2, '178', NULL, '2019-06-26 18:09:57', '2019-06-26 18:09:57', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2019-06-26 18:09:57', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (179, 1, 2, 2, '179', NULL, '2019-07-08 10:22:18', '2019-07-08 10:22:18', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2019-07-08 10:22:18', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, 'ponderado', true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (177, 1, 2, 2, 'K2-2019-02', 'Suministro energía eléctrica 2019-2022', '2019-06-27 10:45:00', '2019-07-02 18:00:00', false, true, true, false, 6, true, '2022-07-31 00:00:00', '2019-08-01 00:00:00', NULL, NULL, NULL, false, 2, 1, '<p>El objeto de esta negociaci&oacute;n es la selecci&oacute;n del proveedor y condiciones id&oacute;neas para el suministro de Energ&iacute;a El&eacute;ctrica, seg&uacute;n especificaciones y requerimientos de CARGLASS, para un periodo de 2 a&ntilde;os, desde el 1 de agosto 2019 hasta 31 de Julio 2022.</p>

<p>El presente pliego regula las normas que regir&aacute;n esta fase de mejora de ofertas,fase 2 de licitaci&oacute;n de sobre cerrado, perfeccionando y complementando el Pliego de Condiciones de la fase de petici&oacute;n de ofertas precedente, <b>pero no lo sustituye ni lo anula.</b></p>

<p>Descripci&oacute;n del proceso</p>

<p>Esta fase final, de mejora de ofertas, se configura en modo &ldquo;Sobre Cerrado&rdquo;: su objetivo es obtener una definitiva y final oferta para la adjudicaci&oacute;n definitiva de los proveedores &oacute;ptimos.</p>

<h2><strong style="font-size: 13px;">DESCARGAR FICHEROS ADJUNTOS</strong><span style="font-size: 13px;">:</span></h2>

<ul>
	<li>Pliego_Sobre_Cerrado_Fase_Cierre_LSC2-CFC07-2019-01</li>
	<li>ANEXO_1_Excel_cotizaci&oacute;n_Calculo_real</li>
	<li>ANEXO_2_Excel_modelos_LSC2</li>
</ul>

<p>&nbsp;</p>', false, NULL, 'Las dudas sobre los contenidos o alcance de este pliego y/o sobre el proceso de negociación en general, deberán canalizarse exclusivamente a través de plataforma ITBID. 

Si algún licitador experimentara dificultades técnicas para el acceso a la plataforma, la entrada de la oferta o la subida de ficheros, deberá contactar con:

Josep Puig
ITBID Technologies
e-mail: jpuig@itbid.com
Tel. 933 218 614', '2019-06-26 17:41:38', '2019-06-27 10:39:21', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'Ver pliego', 5, 'N/A', NULL, 999, 'N/A', 'Electricidad', false, false, '', NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-10 15:15:08', NULL, NULL, '2015-06-04 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, true, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (175, 1, 49, 7, 'N-D1-002', 'Renting de Turismos 2019 - Fase II', '2019-06-05 16:52:38', '2019-06-13 14:00:00', false, true, true, false, 25, true, NULL, NULL, NULL, NULL, false, false, 1, 1, '<p>El objeto de esta negociaci&oacute;n es determinar la contrataci&oacute;n de parte de la flota de veh&iacute;culos que CARGLASS&reg; dispone, a d&iacute;a de hoy en renting. El objeto a negociar son 30 veh&iacute;culos que vencen a final de a&ntilde;o y deben ser sustituidos.</p>

<p>La licitaci&oacute;n se ha configurado en sucesivas rondas; el objetivo de esta segunda ronda es la de elegir el veh&iacute;culo o veh&iacute;culos a contratar y el proveedor o proveedores seleccionados, sea para la adjudicaci&oacute;n o una &uacute;ltima fase de negociaci&oacute;n.</p>

<p>CARGLASS&reg; garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociaci&oacute;n y concurrencia de ofertas en igualdad de condiciones.</p>

<p>Se a&ntilde;ade, en el Anexo I, el c&oacute;digo de conducta para proveedores de CARGLASS&reg; como integrante del Grupo Belron que se acepta t&aacute;citamente con la presentaci&oacute;n de ofertas.</p>', false, NULL, 'Las dudas sobre los servicios a contratar, sobre este Pliego y/o sobre el proceso de negociación en general, deberán canalizarse exclusivamente a través del Centro de Mensajes de plataforma ITBID. 
En caso de dudas sobre códigos de acceso, introducción de ofertas o cualquier cuestión relativa al uso de la plataforma de negociación ITBID, por favor contactar con:

Contacto Principal				         Otro Contacto

Elton Dos Santos
Telf: 936 345 046
edsantos@itbid.com
	
Alvaro Bultó
Móvil 629391818
abulto@itbid.com', '2019-06-04 16:45:15', '2019-06-05 16:52:38', NULL, 7, 7, NULL, NULL, NULL, true, NULL, NULL, 4, 'En el concesionario más cercano al puesto de trabajo de cada empleado que se facilitará mas adelante.', NULL, NULL, NULL, 'Renting', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-12 16:48:46', NULL, NULL, '2019-05-24 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, 'ponderado', true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (180, 1, 771, 14, '180', NULL, '2019-11-27 11:29:26', '2019-11-27 11:29:26', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2019-11-27 11:29:26', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (182, 1, 771, 14, 'CFC042 - FASE 2', 'Servicio Protección Contra Incendios (PCI) 2020-2022', '2019-12-16 15:00:00', '2019-12-19 12:00:00', false, true, true, false, 18, true, '2022-12-31 00:00:00', '2020-01-01 00:00:00', NULL, NULL, true, false, 1, 1, '<p><span style="text-autospace:none">El objeto de esta negociaci&oacute;n es la contrataci&oacute;n del servicio de mantenimiento preventivo, correctivo y la conservaci&oacute;n de las Instalaciones, Equipos y Sistemas de Protecci&oacute;n Contra Incendios, para la Red de Centros y Hubs de Carglass en Espa&ntilde;a <u>hasta 31 de diciembre de 2022.</u></span></p>

<p><span style="text-autospace:none">El presente pliego regula las normas que regir&aacute;n esta fase de <b>mejora de ofertas</b>, fase 2 de licitaci&oacute;n de <b>sobre cerrado</b>, perfeccionando y complementando el Pliego de Condiciones de la fase de petici&oacute;n de ofertas precedente, pero no lo sustituye ni lo anula.</span></p>

<p><span style="text-autospace:none">Por favor leer toda la documentaci&oacute;n adjunta, as&iacute; como la gu&Iacute;a de ayuda para presentaci&oacute;n de oferta debidamente complementada.</span></p>

<p><span style="text-autospace:none">As&iacute; mismo le recordamos la&nbsp;siguiente&nbsp;fecha&nbsp;clave&nbsp;durante este proceso:</span></p>

<ul>
	<li><span style="text-autospace:none">2 - presentaci&oacute;n fecha l&iacute;mite de ofertas (19&nbsp;Dic 19).</span></li>
</ul>', false, NULL, NULL, '2019-12-16 11:44:10', '2019-12-16 14:09:32', NULL, 14, 14, NULL, NULL, NULL, true, 999, 'Ver condiciones Pliego', 5, 'Según servicio y centro que aplique. Ver anexo de listado de centros Carglass.', NULL, NULL, NULL, 'A10. Soft FM - Security & Alarms', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-23 16:10:28', NULL, NULL, '2019-12-04 12:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (183, 1, 771, 14, '183', NULL, '2020-03-02 12:43:58', '2020-03-02 12:43:58', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-03-02 12:43:58', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (181, 1, 771, 14, 'CFC042-1', 'Servicio Protección Contra Incendios (PCI) 2020-2022', '2019-11-27 15:27:37', '2019-12-10 12:10:00', false, true, true, false, 18, true, '2022-12-31 00:00:00', '2020-01-01 00:00:00', NULL, NULL, true, false, 1, 1, '<p><span style="text-autospace:none">El objeto de esta negociaci&oacute;n es la contrataci&oacute;n del servicio de mantenimiento preventivo, correctivo y la conservaci&oacute;n de las Instalaciones, Equipos y Sistemas de Protecci&oacute;n Contra Incendios, para la Red de Centros y Hubs de Carglass en Espa&ntilde;a hasta 31 de diciembre de 2022.</span></p>

<p><span style="text-autospace:none">Por favor leer toda la documentaci&oacute;n adjunta, as&iacute; como la gu&Iacute;a de ayuda para presentaci&oacute;n de oferta debidamente complementada.</span></p>

<p><span style="text-autospace:none">As&iacute; mismo le recordamos las siguientes fechas claves durante este proceso:</span></p>

<ul>
	<li><span style="text-autospace:none">1- presentaci&oacute;n de preguntas y respuestas (02 Dic 19).</span></li>
	<li><span style="text-autospace:none">2 - presentaci&oacute;n fecha l&iacute;mite de ofertas (10&nbsp;Dic 19).</span></li>
</ul>
', false, NULL, NULL, '2019-11-27 12:29:15', '2019-11-27 15:27:37', NULL, 14, 7, NULL, NULL, NULL, true, 999, 'Ver condiciones Pliego', 1, 'Según servicio y centro que aplique. Ver anexo de listado de centros Carglass.', NULL, NULL, NULL, 'A10. Soft FM - Security & Alarms', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-17 13:14:14', NULL, NULL, '2019-12-04 12:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, false, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (184, 1, 771, 14, '184', NULL, '2020-03-03 16:29:27', '2020-03-03 16:29:27', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-03-03 16:29:27', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (185, 1, 771, 14, '185', NULL, '2020-03-03 16:32:18', '2020-03-03 16:32:18', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-03-03 16:32:18', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (186, 1, 771, 14, '186', NULL, '2020-03-03 16:41:02', '2020-03-03 16:41:02', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-03-03 16:41:02', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (187, 1, 771, 14, '187', NULL, '2020-03-03 16:41:41', '2020-03-03 16:41:41', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-03-03 16:41:41', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (188, 1, 771, 14, '188', NULL, '2020-03-03 16:43:20', '2020-03-03 16:43:20', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-03-03 16:43:20', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (190, 1, 771, 14, '190', NULL, '2020-04-23 16:33:43', '2020-04-23 16:33:43', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-04-23 16:33:43', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (26, 1, 19, 6, 'CFC003P', 'SUMINISTROS INDUSTRIALES 2013-2014', '2013-06-07 11:59:11.271389', '2013-06-18 18:00:00', false, false, false, true, 2, true, '2014-06-30 00:00:00', '2013-07-01 00:00:00', NULL, NULL, NULL, false, 0, 1, 'Licitación para la seleccion del proveedor de suministros industriales para hacer frente a las necesidades de los distintos centros de la firma CARGLASS B.V Surcursal en España.

VER FICHEROS ADJUNTOS:

Reglas y Condiciodes (PDF) --> normas de esta negociación
Excel de Cotizacion (XLSX) --> A cumplimentar y anexar a su oferta

****

Por favor, cumplimenten los casilleros con los resultados (Crierios 1 al 5) del Excel
', false, NULL, 'Si algún licitador experimentara dificultades técnicas para la entrada de la oferta o la subida de ficheros, deberá contactar con:

Sr. Antonio de Haro
ITbid Technologies
e-mail: adeharo@itbid.org
Telef: 933218614 
Móvil: 639757911
', '2013-06-07 11:58:37.208829', '2013-06-07 11:59:11.271389', '2013-06-27 09:48:07.64653', 6, 6, 7, NULL, NULL, true, 0, '30 días Transferencia', 3, 'Segun Condiciones - Ciempozuelos
', NULL, 999, NULL, 'Instalaciones', false, false, '[ 26--2013 17:08:26 ]  Antonio de Haro :   Se define 2a ronda negociación por subasta Ranking CFC003S
', NULL, false, 7, '2013-06-27 09:48:07.64653', NULL, NULL, NULL, NULL, '2013-06-27 09:48:07.64653', NULL, NULL, '2013-06-18 18:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'simple', 'desierta', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (4, 1, 19, 6, 'ALM-01-2', 'SISTEMA DE ESTANTERÍAS METÁLICAS - 2a RFQ', '2012-09-23 02:10:43.700706', '2012-09-25 23:55:00', false, true, true, false, 2, true, '2012-12-10 00:00:00', NULL, 'Acceso a la zona de trabajo para los instaladores, 24h al día, 7 días a la semana', NULL, true, false, 2, 1, 'ALMACEN DE CIEMPOZUELOS - UNIFICACION DE OFERTAS

Tras la reunión que han mantenido con los técnicos de Carglass, se ruega introduzcan su nueva oferta, con las modificiones solicitadas.

LA OFERTA DEBE SUMINISTRARSE ANTES DE LAS 23:55H DEL MARTES 25 DE SETIEMBRE.

* La adjudicación se realizará a primera hora del viernes 28.
* La firma del contrato, cuyo borrador se adjunta, será el día 1 de octubre
* El plazo de entrega de la instalación que figura en el contrato es un requerimiento.
* No se admitirán modificaciones sustanciales del contrato

*******

En la plataforma electrónica deberán entrar:

* Precio: Precio TOTAL ofertado
* Materiales: Precio de todos los materiales empleados para todos los racks
* Mano de obra: Precio de la mano de obra en la instalación
* Horas/hombre previstas: en la fase de instalación

Asimismo se ha de adjuntar su oferta completa en PDF; los términos del contrato prevalecen sobre los que puedan ustedes introducir en su oferta.

', false, NULL, 'En caso de dudas sobre la plataforma, entrada de ofertas o anexar ficheros, por favor, contactar con:

JOSE MANUEL IAÑEZ
Mov. 676 953 333
email jmianez@itbid.org
', '2012-09-23 02:08:29.269166', '2012-09-23 02:10:43.700706', NULL, 6, 2, NULL, NULL, NULL, true, 999, 'Indicada en Contrato', 1, 'Ciempozuelos - Madrid
', 'EUR', 0, 'DDP - Ciempozuelos', 'Instalaciones', false, false, '', NULL, true, 2, '2012-09-26 12:04:46.209147', NULL, NULL, NULL, NULL, '2012-09-26 12:04:46.209147', NULL, NULL, '2012-09-25 23:55:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (3, 1, 19, 6, 'ALM-01', 'SISTEMA DE ESTANTERÍAS METÁLICAS', '2012-08-22 11:38:04.828389', '2012-09-19 23:55:00', false, true, true, false, 2, true, NULL, NULL, NULL, NULL, NULL, false, 2, 1, 'El objeto de esta negociación es determinar el proveedor y condiciones óptimas para la adquisición e instalación de un sistema de estanterías metálicas para el almacén de CARGLASS ubicado en Ciempozuelos (Madrid).

VER FICHEROS ANEXOS:

* Pliego_Reglas_Condiciones.pdf --> Reglas y Condiciones
* Ciempozuelos-DC-Racking.pdf --> Requerimientos Carglass
* Zona-Molduras-Accesorios.pdf --> Zona a ser determinada

* 89bda_RFQ_RFP_Proveedor.pdf --> Pequeño manual ITbid

* Fotos-Muestra.zip --> Ejemplos de tipologías de almacenaje CARGLASS en Mollet y Milan

Junto con su oferta (casilleros cumplimentados) debe anexar los siguientes ficheros:

* Oferta completa (económica y técnica, ver Pliego)
* Ultimo recibo Seguro RC (ver Pliego)
* Impuesto Sociedades 2011 (ver Pliego)

ENVIO DE MUESTRAS

El proveedor deberá enviar a CARGLASS muestras de los principales perfiles y elementos (columnas, rejillas, etc…). Las muestras deben ser recibidas antes del día 5 de septiembre, en:

CARGLASS España
At. Sr. Rolando Herrera
Facundo Bacardi i Masso 7- 11 
MOLLET DEL VALLES (Barcelona)


DUDAS SOBRE ESTA RFQ
En caso de dudas sobre esta petición de oferta, deben plantearlas a través del "Centro de Mensajes" de esta negociación. La respuesta llegará por el mismo medio.

', false, NULL, 'En caso de dudas sobre la plataforma, entrada de ofertas o anexar ficheros, por favor, contactar con:

JOSE MANUEL IAÑEZ
Mov. 676 953 333
email jmianez@itbid.org
', '2012-08-22 11:36:51.049931', '2012-08-22 11:38:04.828389', NULL, 6, 2, NULL, NULL, NULL, true, 999, 'A SER NEGOCIADA', 1, 'Ciempozuelos - Madrid
', 'EUR', 0, 'DDP - Ciempozuelos', 'Instalaciones', false, false, '', NULL, true, 2, '2012-09-23 01:30:23.375393', NULL, NULL, NULL, NULL, '2012-09-23 01:30:23.375393', NULL, NULL, '2012-09-19 23:55:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, true, true, false, NULL, false, false, false, false, true, true, 'simple', 'adjudicacion_confirmada', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (36, 1, 137, 9, 'CG-MB01', 'RFI - MOBILE SHOPS CARGLASS', '2013-08-23 12:13:17.050835', '2013-09-03 14:00:00', false, false, false, true, 2, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'REQUEST FOR INFORMATION

Supplier selection process of MOBILE SHOPS - CONTAINER WORKSHOPS for Carglass (a Belron Group Company).

Please, download the RFI-Mobile-Shops.xlsx file, (Excel file to be filled) and sent back to us by using this e-Sourcing platform ("New Offer" button) and attaching also your Company''s Corporative Presentation, including customer references on similar projects (company, contact name and email) and photo gallery both for your facilities and products if convenient.	

Please, complete and provide the following information:

- General information about your company - RFI file / TO BE FILLED 
- Financial Questions - RFI file / TO BE FILLED
- Quality & Management Questions - RFI file / TO BE FILLED
- Company Corporate presentation - TO BE ATTACHED
- Pictures on similiar projects - TO BE ATTCACHED 

Additionally, any complementary informatio that could provide positive perception related to realized projects or creative solutions will be positivelly considered. (Candidates are free to attach as many complementary files as desired).

For any doubt or question, use the "Message Center" of this RFI to contact ITbid team:

Note: A Quick Reference Guide for RFI has been attached for supplier''s convenience. 
', false, NULL, 'For access issues or doubts using this electronic RFI, contact:

Josep Puig 

jpuig@itbid.org
Tel: +34 93 321 86 14
Cell: +34 655 34 76 07', '2013-08-23 12:10:15.72032', '2013-08-23 12:13:17.050835', '2013-11-07 09:45:14.051188', 9, 9, 2, NULL, NULL, true, 999, 'TBD', 3, '--
', NULL, 999, NULL, 'Instalaciones', false, false, '[ 23-Agosto-2013 13:04:46 ]  Jose Manuel Iáñez :   añado 9 proveedores
[ 26-August-2013 11:42:19 ]  Jose Manuel Iáñez :   New supplier Alco Grupo
[ 30-Agosto-2013 10:12:31 ]  Jose Manuel Iáñez :   Extensión para la presentación ultimas ofertas - Karmod 
[ 02-Septiembre-2013 09:27:21 ]  Jose Manuel Iáñez :   New extenstion due to pending supplier data - Karmod
[ 02-Septiembre-2013 09:27:32 ]  Jose Manuel Iáñez :   new extention
[ 02-Septiembre-2013 19:36:16 ]  Jose Manuel Iáñez :   Pending Vendap RFI
', NULL, false, 2, '2013-11-07 09:45:14.051188', NULL, NULL, NULL, NULL, '2013-11-07 09:45:14.051188', NULL, NULL, '2013-09-03 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 3, false, false, false, NULL, false, false, false, false, false, false, 'simple', 'desierta', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (191, 1, 771, 14, '191', NULL, '2020-06-08 10:38:34', '2020-06-08 10:38:34', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-06-08 10:38:34', NULL, NULL, 14, 14, NULL, NULL, NULL, false, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (189, 1, 771, 14, 'CFC043-1', 'SERVICIO DE GESTIÓN DE SUMINISTRO DE MATERIAL DE OFICINA', '2020-03-06 15:45:29', '2020-03-13 14:00:00', false, true, true, false, 20, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, '<p>El objeto de esta negociaci&oacute;n es determinar los proveedores y condiciones &oacute;ptimas para el <b>Suministro de material de oficina</b> <b>y papel</b> en las diferentes oficinas de CARGLASS&reg;, el periodo &nbsp;de contrataci&oacute;n se decidir&aacute; en funci&oacute;n de la opci&oacute;n que m&aacute;s se adecue a las necesidades de CARGLASS&reg;.</p>

<p>Por favor leer toda la documentaci&oacute;n adjunta, as&iacute; como la gu&Iacute;a de ayuda para presentaci&oacute;n de oferta debidamente complementada.</p>

<p>As&iacute; mismo le recordamos las siguientes fechas claves durante este proceso:</p>

<ul>
	<li>1- presentaci&oacute;n de preguntas y respuestas (10&nbsp;Mar 20).</li>
	<li>2 - presentaci&oacute;n fecha l&iacute;mite de ofertas (13&nbsp;Mar&nbsp;20&nbsp;- 14:00h).</li>
</ul>', false, NULL, NULL, '2020-03-03 16:44:04', '2020-03-06 15:45:29', NULL, 14, 14, NULL, NULL, NULL, true, 999, 'Ver condiciones Pliego.', 1, 'Según servicio y centro que aplique. Ver anexo de listado de centros Carglass.', NULL, 999, '- -', 'A7. Office Supplies', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-08 10:54:45', NULL, NULL, '2020-03-10 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (192, 1, 771, 14, 'CFC043 - Fase 2', 'SERVICIO DE GESTIÓN DE SUMINISTRO DE MATERIAL DE OFICINA', '2020-06-08 11:58:35', '2020-06-12 14:00:00', false, true, true, false, 20, true, NULL, NULL, NULL, NULL, NULL, false, 0, 1, '<p>El objeto de esta negociaci&oacute;n final de mejora de ofertas es determinar los proveedores y condiciones &oacute;ptimas para el <b>Suministro de Material de Oficina</b> <b>y Papel</b> en las diferentes oficinas de CARGLASS&reg;, el periodo &nbsp;de contrataci&oacute;n se decidir&aacute; en funci&oacute;n de la opci&oacute;n que m&aacute;s se adecue a las necesidades de CARGLASS&reg;.</p>

<p>Por favor leer toda la documentaci&oacute;n adjunta, as&iacute; como la gu&Iacute;a de ayuda para presentaci&oacute;n de oferta debidamente complementada.</p>

<p>As&iacute; mismo le recordamos las siguientes fechas claves durante este proceso:</p>

<ul>
	<li>1- presentaci&oacute;n de preguntas y respuestas (10 Jun&nbsp;20).</li>
	<li>2 - presentaci&oacute;n fecha l&iacute;mite de ofertas (12&nbsp;Jun&nbsp;20&nbsp;- 14:00h).</li>
</ul>', false, NULL, NULL, '2020-06-08 10:57:33', '2020-06-08 11:58:35', NULL, 14, 14, NULL, NULL, NULL, true, 999, 'Ver condiciones Pliego.', 3, 'Según servicio y centro que aplique. Ver anexo de listado de centros Carglass.', NULL, 999, '- -', 'A7. Office Supplies', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-29 18:06:46', NULL, NULL, '2020-03-10 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, true, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, 189, 2);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (247, 1, 771, 14, '247', 'INSTALACIÓN DE PLACAS FOTOVOLTAICAS CARGLASS CIEMPOZUELOS', '2021-12-02 18:17:56', '2021-12-02 18:17:56', false, false, false, false, 6, false, NULL, NULL, NULL, NULL, false, false, 1, 1, '<p>El objeto de esta negociaci&oacute;n es identificar potenciales proveedores que puedan llevar a cabo un proyecto de suministro, instalaci&oacute;n y mantenimiento de placas fotovoltaicas en el HUB principal de Carglass en Ciempozuelos, Madrid.</p>

<p>&nbsp;</p>

<p><b>Consumo estimado Ciempozuelos 689 MWh/a&ntilde;o.</b></p>

<p><b>Cubierta en chapa de 2mm, lana y chapa. </b></p>

<p><strong>Calendario del proyecto:</strong></p>

<div align="center">
<table class="Table" style="border-collapse:collapse; border:none">
	<thead>
		<tr>
			<td style="width:346.35pt; border:solid #999999 1.5pt; background:#e6e6e6" width="462">
			<p class="CxSpFirst" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>ACTIVIDAD</b></p>
			</td>
			<td style="width:118.85pt; border:solid #999999 1.5pt; border-left:none; background:#e6e6e6" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>FECHA PREVISTA</b></p>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Env&iacute;o de la petici&oacute;n de informaci&oacute;n</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>10/11/2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Plazo l&iacute;mite para la recepci&oacute;n de consultas de los licitadores</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>15/11/21</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>Plazo l&iacute;mite para la Recepci&oacute;n de propuestas in&iacute;ciales</b></p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt; background:#a8d08d" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>19/11/21</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Revisi&oacute;n, valoraci&oacute;n y an&aacute;lisis de las propuestas</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>22 al 26/11/2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Convocatoria de licitadores seleccionados a participar en la RFQ</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>29/11/2021</b></p>
			</td>
		</tr>
	</tbody>
</table>
</div>

<p>&nbsp;</p>', false, NULL, NULL, '2021-12-02 18:17:56', '2021-11-10 13:46:47', NULL, 14, 14, NULL, NULL, NULL, false, 999, NULL, 5, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 999, 'str_otras_condiciones_envio', 'K02. Electricity', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-19 16:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, false, false, false, NULL, false, true, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (196, 1, 2, 2, '196', NULL, '2020-11-04 22:36:38', '2020-11-04 22:36:38', false, false, false, false, NULL, false, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, '2020-11-04 22:36:38', NULL, NULL, 2, 2, NULL, NULL, NULL, false, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, NULL, false, false, false, false, false, false, 'avanzada', 'nueva', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (248, 1, 771, 14, 'CFC048_2', 'INSTALACIÓN DE PLACAS FOTOVOLTAICAS CARGLASS CIEMPOZUELOS', '2021-12-02 18:45:00', '2021-12-15 14:00:00', false, true, false, false, 6, true, NULL, NULL, NULL, NULL, NULL, false, 1, 1, '<p>El objeto de esta negociaci&oacute;n es identificar potenciales proveedores que puedan llevar a cabo un proyecto de suministro, instalaci&oacute;n y mantenimiento de placas fotovoltaicas en el HUB principal de Carglass en Ciempozuelos, Madrid.</p>

<p>El Alcance de la propuesta t&eacute;cnica y econ&oacute;mica, deber&aacute; cubrir como m&iacute;nimo las siguientes actividades:</p>

<ol>
	<li>Dise&ntilde;o, construcci&oacute;n, instalaci&oacute;n, puesta en marcha y entrega de la instalaci&oacute;n en servicio.</li>
	<li>Legalizaci&oacute;n del proyecto instalado.</li>
	<li>Gesti&oacute;n de la tramitaci&oacute;n de subvenciones.</li>
	<li>Servicio de mantenimiento de la instalaci&oacute;n (predictivo, preventivo y correctivo) con monitorizaci&oacute;n del funcionamiento para asegurar un optima producci&oacute;n y vida &uacute;til.</li>
	<li>Como alcance complementario, se valorar&aacute; positivamente cualquier actividad que se incluya como valor a&ntilde;adido en el proyecto para CARGLASS o sus empleados (formaci&oacute;n, condiciones especiales para empleados).</li>
</ol>

<p>En esta fase de revisi&oacute;n de oferta, el proveedor deber&aacute; presentar una propuesta t&eacute;cnica y econ&oacute;mica para <u>una instalaci&oacute;n b&aacute;sica</u> con un dimensionamiento de la misma que permita cubrir un <b>m&iacute;nimo de 35% y un m&aacute;ximo de 45%</b> del consumo anual actual (689.000 Kwh/a&ntilde;o) del centro de Carglass en CIEMPOZUELOS.</p>

<p>El presente pliego regula las normas que regir&aacute;n esta fase de mejora de ofertas, fase 2 de licitaci&oacute;n de sobre cerrado, perfeccionando y complementando el Pliego de Condiciones de la fase de petici&oacute;n de ofertas precedente anterior, pero no lo sustituye ni lo anula.</p>

<div align="center">
<table class="Table" style="border-collapse:collapse; border:none">
	<thead>
		<tr>
			<td style="width:346.35pt; border:solid #999999 1.5pt; background:#e6e6e6" width="462">
			<p class="CxSpFirst" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>ACTIVIDAD</b></p>
			</td>
			<td style="width:118.85pt; border:solid #999999 1.5pt; border-left:none; background:#e6e6e6" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>FECHA PREVISTA</b></p>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Env&iacute;o de la petici&oacute;n de informaci&oacute;n</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>2-12-2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Plazo l&iacute;mite para la recepci&oacute;n de consultas de los licitadores</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>9-12-2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>Plazo l&iacute;mite para la Recepci&oacute;n de propuestas</b></p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt; background:#a8d08d" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>15-12-2021 a las (14:00)</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Revisi&oacute;n, valoraci&oacute;n y an&aacute;lisis de las propuestas</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>17-12-2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Confirmaci&oacute;n de la Adjudicaci&oacute;n</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>20-12-2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Firma del Contrato</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>Enero/Febrero 2022*</b></p>
			</td>
		</tr>
	</tbody>
</table>
</div>', false, NULL, NULL, '2021-12-02 18:18:40', '2021-12-02 18:36:26', NULL, 14, 14, NULL, NULL, NULL, true, 999, 'Ver Pliego de Condiciones.', 5, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 999, 'str_otras_condiciones_envio', 'K02. Electricity', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-19 16:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, true, false, true, true, 'avanzada', 'pendiente_decision', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (194, 1, 2, 2, 'CFC0043-1-ESP', 'SERVICIO DE TRANSPORTE Y DISTRIBUCIÓN DE LOS ALMACENES DE CARGLASS A SUS CENTROS Y CLIENTES EXTERNOS en ESPAÑA -  CF043-1', '2020-10-09 11:52:03', '2020-10-23 16:15:00', false, true, true, false, 9, true, '2021-12-31 00:00:00', '2021-01-01 00:00:00', NULL, NULL, true, false, 2, 1, '<h3 align="center" style="text-align:center">NEGOCIACI&Oacute;N PARA LA PRESTACION DEL SERVICIO DE TRANSPORTE Y DISTRIBUCI&Oacute;N DE LOS ALMACENES DE CARGLASS A SUS CENTROS Y CLIENTES EXTERNOS EN ESPA&Ntilde;A Y PORTUGAL</h3>

<ul>
	<li>
	<h3><strong>Por favor Descargue y revise el pliego de condiciones y los anexos adjuntos.</strong></h3>
	</li>
</ul>

<p><span style="text-autospace:none">El objeto de esta negociaci&oacute;n es determinar los proveedores y condiciones &oacute;ptimas para la prestaci&oacute;n del servicio <b>de TRANSPORTE Y DISTRIBUCI&Oacute;N DE LOS ALMACENES DE CARGLASS A SUS CENTROS Y CLIENTES EXTERNOS, en ESPA&Ntilde;A </b>a partir del d&iacute;a 1 de enero de 2021 hasta el 31 de diciembre de 2021, con extensi&oacute;n hasta 31 diciembre de 2022 en caso de renovaci&oacute;n autom&aacute;tica.</span></p>

<p><span style="text-autospace:none">El material a trasladar, ser&aacute; material propiedad de CARGLASS y su destino ser&aacute; sus propios almacenes y centros, o sus clientes externos.CARGLASS garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociaci&oacute;n y concurrencia de ofertas en igualdad de condiciones.</span></p>', false, NULL, NULL, '2020-10-09 08:59:49', '2020-10-09 11:52:03', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 1, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, NULL, NULL, 'A01. Transportation National distribution', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-05 07:53:02', NULL, NULL, '2020-10-23 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (195, 1, 2, 2, 'CFC-044-1PO', 'SERVIÇO DE TRANSPORTE E DISTRIBUIÇÃO DOS ARMAZÉNS DA CARGLASS PARA OS SEUS CENTROS EM PORTUGAL CF044-1PO', '2020-10-20 12:05:29', '2020-11-04 23:35:00', false, true, true, false, 9, true, '2021-12-31 00:00:00', '2021-01-01 00:00:00', NULL, NULL, true, false, 2, 1, '<p align="center" style="text-align:center"><span style="text-autospace:none"><b>NEGOCIA&Ccedil;&Atilde;O PARA A PRESTA&Ccedil;&Atilde;O DO SERVI&Ccedil;O DE TRANSPORTE E DISTRIBUI&Ccedil;&Atilde;O DOS ARMAZ&Eacute;NS DA CARGLASS AOS SEUS CENTROS E CLIENTES EXTERNOS EM PORTUGAL</b></span></p>

<p align="center" style="text-align:center"><strong><u>Fa&ccedil;a download e reveja as especifica&ccedil;&otilde;es da licita&ccedil;&atilde;o&nbsp;e os anexos.</u></strong></p>

<p>Pretende-se com a presente negocia&ccedil;&atilde;o determinar os fornecedores e condi&ccedil;&otilde;es &oacute;ptimos para a presta&ccedil;&atilde;o do servi&ccedil;o de <b>TRANSPORTE E DISTRIBUI&Ccedil;&Atilde;O DOS ARMAZ&Eacute;NS DA CARGLASS PARA OS SEUS CENTROS, em PORTUGAL</b> de 1 de Janeiro de 2021 a 31 de Dezembro, 2022.</p>

<p><span style="text-autospace:none">O material a ser transportado ser&aacute; material de propriedade da CARGLASS e ter&aacute; como destino os dep&oacute;sitos e centros pr&oacute;prios ou clientes externos.A CARGLASS garante aos fornecedores selecionados a publicidade do pedido de ofertas, a transpar&ecirc;ncia da negocia&ccedil;&atilde;o e a concorr&ecirc;ncia das ofertas em igualdade de condi&ccedil;&otilde;es.</span></p>

<pre data-placeholder="Traducción" dir="ltr">


&nbsp;</pre>', false, NULL, NULL, '2020-10-20 09:42:49', '2020-10-20 12:05:29', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'revisar Caderno_de_Especificações_CFC0044-1PO', 1, 'Según Anexo A', NULL, NULL, NULL, 'A01. Transportation National distribution', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-11 11:44:16', NULL, NULL, '2020-10-23 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (8, 1, 19, 6, 'ALM-02', 'ALQUILER DE CARRETILLAS', '2012-10-11 16:46:09.872002', '2012-10-19 14:00:00', false, false, false, false, 2, false, NULL, NULL, NULL, NULL, NULL, false, 0, 1, 'ALQUILER DE DE CARRETILLAS Y VEHICULOS PARA ALMACENAJE PARA LOS ALMACENES CARGLASS DE CIEMPOZUELOS Y MOLLET

Ver ficheros adjuntos:

* Reglas_y_condiciones.PDF
* Anexo 1 Descripcion técnica y Cotizacion.XLS
* Anexo 3 Compra Activos.XLS

Trasladar a los casilleros habilitados en la plataforma los resultados de los casilleros ROSA de los adjuntos.

SE HAN DE ADJUNTAR A LA OFERTA:

* Anexo1, cumplimentado
* Anexo 3, cumplimentado
* Folletos de los productos, con características técnicas
* Borrador de contrato tipo
* Documento explicativo de penalizaciones por resolución anticipada de contrato de alquiler (si no incluido en borrador de contrato)
* Imp Sociedades 2011

', false, 'carretillas / diversos tipos', 'En caso de dudas sobre la plataforma, entrada de
ofertas o anexar ficheros, por favor, contactar
con:

JOSE MANUEL IAÑEZ
Mov. 676 953 333
email jmianez@itbid.org

', '2012-10-11 16:46:09.872002', NULL, NULL, 6, NULL, NULL, NULL, NULL, false, 999, 'Cuota Mensual', 3, 'Ciempozuelos - Madrid
Mollet - Barcelona
', NULL, 999, NULL, 'Carretillas', true, false, '', NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2012-10-19 14:00:00', NULL, NULL, NULL, NULL, 0, 0, true, true, NULL, 0, NULL, NULL, false, false, 0, 1, false, false, false, NULL, false, false, false, false, false, false, 'simple', 'plantilla', false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (204, 1, 2, 2, 'CF043-2-CE', 'LOTE 1 - SERVICIO DE TRANSPORTE Y DISTRIBUCIÓN DE CARGLASS A SUS CLIENTES EXTERNOS -  CF043-2-CE', '2020-11-05 10:00:00', '2020-11-16 17:00:00', false, true, true, false, 9, true, '2021-12-31 00:00:00', '2021-01-01 00:00:00', NULL, NULL, NULL, false, 2, 1, '<h3 align="center" style="text-align:center">FASE 2 DE LA NEGOCIACI&Oacute;N PARA LA PRESTACION DEL SERVICIO DE TRANSPORTE Y DISTRIBUCI&Oacute;N DE LOS ALMACENES DE CARGLASS A SUS CLIENTES EXTERNOS</h3>

<ul>
	<li>
	<h3><strong>Por favor Descargue y revise el pliego de condiciones y los anexos adjuntos.</strong></h3>
	</li>
</ul>

<p><span style="text-autospace:none">El objeto de esta negociaci&oacute;n es determinar los proveedores y condiciones &oacute;ptimas para la prestaci&oacute;n del servicio <b>de TRANSPORTE Y DISTRIBUCI&Oacute;N DE LOS ALMACENES DE CARGLASS A SUS CLIENTES en ESPA&Ntilde;A </b>a partir del d&iacute;a 1 de enero de 2021 hasta el 31 de diciembre de 2021, con extensi&oacute;n hasta 31 diciembre de 2022 en caso de renovaci&oacute;n autom&aacute;tica.</span></p>

<p><span style="text-autospace:none">El material a trasladar, ser&aacute; material propiedad de CARGLASS y su destino ser&aacute; sus propios almacenes y centros, o sus clientes externos.CARGLASS garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociaci&oacute;n y concurrencia de ofertas en igualdad de condiciones.</span></p>', false, NULL, NULL, '2020-11-05 08:42:15', '2020-11-05 08:57:31', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 5, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, NULL, NULL, 'A01. Transportation National distribution', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-27 16:45:21', NULL, NULL, '2020-10-23 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, 194, 2);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (201, 1, 2, 2, 'CF043-2-L2', 'LOTE 1 - SERVICIO DE TRANSPORTE Y DISTRIBUCIÓN DE CARGLASS A SUS CENTROS DE NORTE 1 Y NORTE 2 -  CF043-2-L2', '2020-11-05 10:00:00', '2020-11-16 17:15:00', false, true, true, false, 9, true, '2021-12-31 00:00:00', '2021-01-01 00:00:00', NULL, NULL, NULL, false, 2, 1, '<h3 align="center" style="text-align:center">FASE 2 DE LA NEGOCIACI&Oacute;N PARA LA PRESTACION DEL SERVICIO DE TRANSPORTE Y DISTRIBUCI&Oacute;N DE LOS ALMACENES DE CARGLASS A SUS CENTROS DE ZONA NORTE&nbsp; 1 Y NORTE 2</h3>

<ul>
	<li>
	<h3><strong>Por favor Descargue y revise el pliego de condiciones y los anexos adjuntos.</strong></h3>
	</li>
</ul>

<p><span style="text-autospace:none">El objeto de esta negociaci&oacute;n es determinar los proveedores y condiciones &oacute;ptimas para la prestaci&oacute;n del servicio <b>de TRANSPORTE Y DISTRIBUCI&Oacute;N DE LOS ALMACENES DE CARGLASS A SUS CENTROS DE LAS ZONAS INDICADAS en ESPA&Ntilde;A </b>a partir del d&iacute;a 1 de enero de 2021 hasta el 31 de diciembre de 2021, con extensi&oacute;n hasta 31 diciembre de 2022 en caso de renovaci&oacute;n autom&aacute;tica.</span></p>

<p><span style="text-autospace:none">El material a trasladar, ser&aacute; material propiedad de CARGLASS y su destino ser&aacute; sus propios almacenes y centros, o sus clientes externos.CARGLASS garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociaci&oacute;n y concurrencia de ofertas en igualdad de condiciones.</span></p>', false, NULL, NULL, '2020-11-05 08:11:44', '2020-11-05 08:17:13', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 5, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, NULL, NULL, 'A01. Transportation National distribution', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 17:58:09', NULL, NULL, '2020-10-23 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, 194, 2);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (203, 1, 2, 2, 'CF043-2-L4', 'LOTE 1 - SERVICIO DE TRANSPORTE Y DISTRIBUCIÓN DE CARGLASS A SUS CENTROS DE LEVANTE Y NOROESTE -  CF043-2-L4', '2020-11-05 10:00:00', '2020-11-16 17:15:00', false, true, true, false, 9, true, '2021-12-31 00:00:00', '2021-01-01 00:00:00', NULL, 'Incluya en la zona de documentos adjuntos el ANEX F rellenado con sus cálculos y datos', NULL, false, 2, 1, '<h3 align="center" style="text-align:center">FASE 2 DE LA NEGOCIACI&Oacute;N PARA LA PRESTACION DEL SERVICIO DE TRANSPORTE Y DISTRIBUCI&Oacute;N DE LOS ALMACENES DE CARGLASS A SUS CENTROS DE ZONA LEVANTE Y NOROESTE</h3>

<ul>
	<li>
	<h3><strong>Por favor Descargue y revise el pliego de condiciones y los anexos adjuntos.</strong></h3>
	</li>
</ul>

<p><span style="text-autospace:none">El objeto de esta negociaci&oacute;n es determinar los proveedores y condiciones &oacute;ptimas para la prestaci&oacute;n del servicio <b>de TRANSPORTE Y DISTRIBUCI&Oacute;N DE LOS ALMACENES DE CARGLASS A SUS CENTROS DE LAS ZONAS INDICADAS en ESPA&Ntilde;A </b>a partir del d&iacute;a 1 de enero de 2021 hasta el 31 de diciembre de 2021, con extensi&oacute;n hasta 31 diciembre de 2022 en caso de renovaci&oacute;n autom&aacute;tica.</span></p>

<p><span style="text-autospace:none">El material a trasladar, ser&aacute; material propiedad de CARGLASS y su destino ser&aacute; sus propios almacenes y centros, o sus clientes externos.CARGLASS garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociaci&oacute;n y concurrencia de ofertas en igualdad de condiciones.</span></p>', false, NULL, NULL, '2020-11-05 08:21:40', '2020-11-05 08:27:08', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 5, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, NULL, NULL, 'A01. Transportation National distribution', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 18:13:08', NULL, NULL, '2020-10-23 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, 194, 2);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (200, 1, 2, 2, 'CFC0043 - Fase 2 - L1', 'LOTE 1 - SERVICIO DE TRANSPORTE Y DISTRIBUCIÓN DE CARGLASS A SUS CENTROS DE CENTRO Y EXTREMADURA -  CF043-2-L1', '2020-11-05 10:00:00', '2020-11-16 17:15:00', false, true, true, false, 9, true, '2021-12-31 00:00:00', '2021-01-01 00:00:00', NULL, NULL, NULL, false, 2, 1, '<h3 align="center" style="text-align:center">FASE 2 DE LA NEGOCIACI&Oacute;N PARA LA PRESTACION DEL SERVICIO DE TRANSPORTE Y DISTRIBUCI&Oacute;N DE LOS ALMACENES DE CARGLASS A SUS CENTROS DE ZONA CENTRO Y EXTREMADURA</h3>

<ul>
	<li>
	<h3><strong>Por favor Descargue y revise el pliego de condiciones y los anexos adjuntos.</strong></h3>
	</li>
</ul>

<p><span style="text-autospace:none">El objeto de esta negociaci&oacute;n es determinar los proveedores y condiciones &oacute;ptimas para la prestaci&oacute;n del servicio <b>de TRANSPORTE Y DISTRIBUCI&Oacute;N DE LOS ALMACENES DE CARGLASS A SUS CENTROS DE LAS ZONAS INDICADAS en ESPA&Ntilde;A </b>a partir del d&iacute;a 1 de enero de 2021 hasta el 31 de diciembre de 2021, con extensi&oacute;n hasta 31 diciembre de 2022 en caso de renovaci&oacute;n autom&aacute;tica.</span></p>

<p><span style="text-autospace:none">El material a trasladar, ser&aacute; material propiedad de CARGLASS y su destino ser&aacute; sus propios almacenes y centros, o sus clientes externos.CARGLASS garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociaci&oacute;n y concurrencia de ofertas en igualdad de condiciones.</span></p>', false, NULL, NULL, '2020-11-05 07:56:37', '2020-11-05 08:11:35', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 5, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, NULL, NULL, 'A01. Transportation National distribution', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 17:50:45', NULL, NULL, '2020-10-23 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, 194, 2);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (202, 1, 2, 2, 'CF043-2-L3', 'LOTE 1 - SERVICIO DE TRANSPORTE Y DISTRIBUCIÓN DE CARGLASS A SUS CENTROS DE SUR Y CATALUNYA -  CF043-2-L3', '2020-11-05 10:00:00', '2020-11-16 17:15:00', false, true, true, false, 9, true, '2021-12-31 00:00:00', '2021-01-01 00:00:00', NULL, NULL, NULL, false, 2, 1, '<h3 align="center" style="text-align:center">FASE 2 DE LA NEGOCIACI&Oacute;N PARA LA PRESTACION DEL SERVICIO DE TRANSPORTE Y DISTRIBUCI&Oacute;N DE LOS ALMACENES DE CARGLASS A SUS CENTROS DE ZONA SUR Y CATALUNYA</h3>

<ul>
	<li>
	<h3><strong>Por favor Descargue y revise el pliego de condiciones y los anexos adjuntos.</strong></h3>
	</li>
</ul>

<p><span style="text-autospace:none">El objeto de esta negociaci&oacute;n es determinar los proveedores y condiciones &oacute;ptimas para la prestaci&oacute;n del servicio <b>de TRANSPORTE Y DISTRIBUCI&Oacute;N DE LOS ALMACENES DE CARGLASS A SUS CENTROS DE LAS ZONAS INDICADAS en ESPA&Ntilde;A </b>a partir del d&iacute;a 1 de enero de 2021 hasta el 31 de diciembre de 2021, con extensi&oacute;n hasta 31 diciembre de 2022 en caso de renovaci&oacute;n autom&aacute;tica.</span></p>

<p><span style="text-autospace:none">El material a trasladar, ser&aacute; material propiedad de CARGLASS y su destino ser&aacute; sus propios almacenes y centros, o sus clientes externos.CARGLASS garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociaci&oacute;n y concurrencia de ofertas en igualdad de condiciones.</span></p>', false, NULL, NULL, '2020-11-05 08:17:22', '2020-11-05 08:21:30', NULL, 2, 2, NULL, NULL, NULL, true, 0, NULL, 5, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, NULL, NULL, 'A01. Transportation National distribution', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 18:03:33', NULL, NULL, '2020-10-23 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, 194, 2);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (205, 1, 2, 2, 'CFC044-F2- Negociação Envelope Fechado', 'SERVIÇO DE TRANSPORTE E DISTRIBUIÇÃO DOS ARMAZÉNS DA CARGLASS PARA OS SEUS CENTROS EM PORTUGAL CF044-1PO', '2020-11-11 12:10:00', '2020-11-20 14:00:00', false, true, true, false, 9, true, '2021-12-31 00:00:00', '2021-01-01 00:00:00', NULL, NULL, true, false, 2, 1, '<p align="center" style="text-align:center"><b>SERVI&Ccedil;O DE TRANSPORTE E DISTRIBUI&Ccedil;&Atilde;O DOS ARMAZ&Eacute;NS DA CARGLASS PARA OS SEUS CENTROS EM PORTUGAL &nbsp;&nbsp;CF044-1PO</b></p>

<p align="center" style="text-align:center"><strong><u>Fa&ccedil;a download e reveja as especifica&ccedil;&otilde;es da licita&ccedil;&atilde;o&nbsp;e os anexos.</u></strong></p>

<p>Pretende-se com a presente negocia&ccedil;&atilde;o determinar os fornecedores e condi&ccedil;&otilde;es &oacute;ptimos para a presta&ccedil;&atilde;o do servi&ccedil;o de <b>TRANSPORTE E DISTRIBUI&Ccedil;&Atilde;O DOS ARMAZ&Eacute;NS DA CARGLASS PARA OS SEUS CENTROS, em PORTUGAL</b> de 1 de Janeiro de 2021 a 31 de Dezembro, 2022.</p>

<p><span style="text-autospace:none">O material a ser transportado ser&aacute; material de propriedade da CARGLASS e ter&aacute; como destino os dep&oacute;sitos e centros pr&oacute;prios ou clientes externos.A CARGLASS garante aos fornecedores selecionados a publicidade do pedido de ofertas, a transpar&ecirc;ncia da negocia&ccedil;&atilde;o e a concorr&ecirc;ncia das ofertas em igualdade de condi&ccedil;&otilde;es.</span></p>

<pre data-placeholder="Traducción" dir="ltr">


&nbsp;</pre>', false, NULL, NULL, '2020-11-11 11:46:49', '2020-11-11 11:57:35', NULL, 2, 2, NULL, NULL, NULL, true, 999, 'revisar Caderno_de_Especificações_CFC0044-1PO', 5, 'Según Anexo A', NULL, NULL, NULL, 'A01. Transportation National distribution', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 18:18:33', NULL, NULL, '2020-10-23 14:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, 195, 2);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (210, 1, 771, 14, 'CFC046', 'RECOGIDA, GESTIÓN Y RECICLADO DE RESIDUOS', '2021-07-21 16:46:00', '2021-07-28 16:00:00', false, true, true, false, 17, true, NULL, NULL, NULL, NULL, false, false, 1, 1, '<p style="margin-bottom:.0001pt"><span style="text-autospace:none">El objeto de esta negociaci&oacute;n es determinar los proveedores y condiciones &oacute;ptimas para la prestaci&oacute;n de diversos servicios de Recogida, Gesti&oacute;n y Reciclado de Residuos de Vidrio Templado Roto de los diferentes centros de Carglass&reg; en Espa&ntilde;a por el periodo de 2 a&ntilde;os renovables.</span></p>

<p>Carglass&reg; garantiza a los proveedores seleccionados la publicidad de la solicitud de ofertas, la transparencia de la negociaci&oacute;n y concurrencia de ofertas en igualdad de condiciones.</p>

<p><strong>El proceso para la selecci&oacute;n del proveedor, y las fechas del mismo, es el siguiente:</strong></p>

<div align="center">
<table class="Table" style="border-collapse:collapse; border:none">
	<thead>
		<tr>
			<td style="width:343.45pt; border:solid #999999 1.5pt; background:#e6e6e6" width="458">
			<p class="CxSpFirst" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>ACTIVIDAD</b></p>
			</td>
			<td style="width:118.2pt; border:solid #999999 1.5pt; border-left:none; background:#e6e6e6" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>FECHA PREVISTA</b></p>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width:343.45pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="458">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Env&iacute;o de la petici&oacute;n de oferta a los licitadores</p>
			</td>
			<td style="width:118.2pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><span style="tab-stops:21.0pt">21 julio de 2021</span></p>
			</td>
		</tr>
		<tr>
			<td style="width:343.45pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="458">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Plazo l&iacute;mite para la recepci&oacute;n de consultas de los licitadores</p>
			</td>
			<td style="width:118.2pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">26 julio de 2021</p>
			</td>
		</tr>
		<tr>
			<td style="width:343.45pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="458">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Recepci&oacute;n de ofertas in&iacute;ciales</p>
			</td>
			<td style="width:118.2pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">28 julio de 2021</p>
			</td>
		</tr>
		<tr>
			<td style="width:343.45pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="458">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Revisi&oacute;n, valoraci&oacute;n y an&aacute;lisis ofertas</p>
			</td>
			<td style="width:118.2pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">04 agosto de 2021</p>
			</td>
		</tr>
		<tr>
			<td style="width:343.45pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.0pt" width="458">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Comunicaci&oacute;n de licitadores finalistas</p>
			</td>
			<td style="width:118.2pt; border-top:none; border-left:none; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.5pt" width="158">
			<p align="left" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:left; padding:1.4pt 5.4pt 1.4pt 5.4pt">06 agosto de 2021</p>
			</td>
		</tr>
	</tbody>
</table>
</div>', false, NULL, NULL, '2021-07-21 11:30:58', '2021-07-21 16:46:00', NULL, 14, 14, NULL, NULL, NULL, true, NULL, NULL, 1, 'Ver pliego de condiciones.', NULL, NULL, NULL, 'C05. Waste Management', false, false, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-10 12:38:07', NULL, NULL, '2021-07-28 16:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, false, false, false, true, true, 'avanzada', 'adjudicacion_confirmada', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (212, 1, 771, 14, 'CFC047', 'SUMINISTRO DE ENERGIA ELECTRICA E INSTALACIÓN DE PLACAS FOTOVOLTAICAS', '2021-10-19 15:01:00', '2021-10-29 16:00:00', false, true, false, false, 6, true, NULL, NULL, NULL, NULL, false, false, 1, 1, '<p>El objeto de esta negociaci&oacute;n es identificar potenciales proveedores que puedan llevar a cabo un proyecto de suministro, instalaci&oacute;n y mantenimiento de placas fotovoltaicas en el HUB principal de Carglass&reg; en Ciempozuelos, Madrid, y puedan ser tambi&eacute;n el proveedor comercializador de electricidad para todos los centros de Carglass&reg; Espa&ntilde;a.</p>

<p>Calendario del proyecto:</p>

<div align="center">
<table class="Table" style="border-collapse:collapse; border:none">
	<thead>
		<tr>
			<td style="width:346.35pt; border:solid #999999 1.5pt; background:#e6e6e6" width="462">
			<p class="CxSpFirst" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>ACTIVIDAD</b></p>
			</td>
			<td style="width:118.85pt; border:solid #999999 1.5pt; border-left:none; background:#e6e6e6" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>FECHA PREVISTA</b></p>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Env&iacute;o de la petici&oacute;n de informaci&oacute;n</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>19/10/2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Plazo l&iacute;mite para la recepci&oacute;n de consultas de los licitadores</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>25/10/2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>Plazo l&iacute;mite para la Recepci&oacute;n de propuestas in&iacute;ciales</b></p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt; background:#92d050" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>29/10/2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Revisi&oacute;n, valoraci&oacute;n y an&aacute;lisis de las propuestas</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>01/11/2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Convocatoria de proveedores adjudicados a segunda fase</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>04/11/2021</b></p>
			</td>
		</tr>
	</tbody>
</table>
</div>', false, NULL, NULL, '2021-10-19 13:50:40', '2021-10-19 15:01:00', NULL, 14, 14, NULL, NULL, NULL, true, NULL, NULL, 1, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, NULL, NULL, 'K02. Electricity', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-29 16:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'pendiente_decision', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);
INSERT INTO public.subastas (id_subasta, id_empresa, id_usuario, id_comprador, subtitulo, subdesc, subini, subfin, subvisible, subautorizado, subadjudicado, subdesierto, id_subcategoria, subfinalizado, subfecentregamax, subfecentregamin, subsuministracli, subsuministrapro, subgastosenvio, subproverprecio, id_unidad, id_divisa, subdescdetalle, subborrador, subunidad, subobs, subsyscreado, subsyslanzado, subsysadjudicado, subsyscreadopor, subsyslanzadopor, subsysadjudicadopor, subprecioobjetivo, subsubasta, subvalida, id_formapago, subformapago, subtipo, subdirentrega, subdivisa, id_condenv, subcondenv, subsubcat, subplantilla, subabortada, obsoperacion, subobsadjudicado, subconfirmaadj, subsysconfadjpor, subsysconfadj, subsysabortado, subsysabortadopor, submejora, id_mejora, subcie, subobsprv, submaximunextension, subfechahoralimite, duracion_max, precioreserva, numganadores, id_desempate, subdesempate, visibilidad, mejorarprecio, mejorarporproducto, preciocierre, tipopreciocierre, submejoramaxima, id_mejoramaxima, submostrarcantidades, submostrarpreciototal, subsuperarposicion, id_sede, autorizadalanzamiento, autorizadaanalisis, autorizadaadjudicacion, id_proyecto, faseiniciacion, fasenegociacion, fasecierre, sublanzado, aprobada_lanzamiento, aprobada_analisis, modalidad, estado, multidivisa, peso_subtotal_mro, tipo_mro, cantidades_visibles, minutosparaextender, minutosextension, segundostramoholandesa, subtipomejora, tipomejorarprecio, negociacion_padre, fase) VALUES (213, 1, 771, 14, 'CFC048', 'INSTALACIÓN DE PLACAS FOTOVOLTAICAS CARGLASS CIEMPOZUELOS', '2021-11-10 13:46:47', '2021-11-19 16:00:00', false, true, false, false, 6, true, NULL, NULL, NULL, NULL, false, false, 1, 1, '<p>El objeto de esta negociaci&oacute;n es identificar potenciales proveedores que puedan llevar a cabo un proyecto de suministro, instalaci&oacute;n y mantenimiento de placas fotovoltaicas en el HUB principal de Carglass en Ciempozuelos, Madrid.</p>

<p>&nbsp;</p>

<p><b>Consumo estimado Ciempozuelos 689 MWh/a&ntilde;o.</b></p>

<p><b>Cubierta en chapa de 2mm, lana y chapa. </b></p>

<p><strong>Calendario del proyecto:</strong></p>

<div align="center">
<table class="Table" style="border-collapse:collapse; border:none">
	<thead>
		<tr>
			<td style="width:346.35pt; border:solid #999999 1.5pt; background:#e6e6e6" width="462">
			<p class="CxSpFirst" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>ACTIVIDAD</b></p>
			</td>
			<td style="width:118.85pt; border:solid #999999 1.5pt; border-left:none; background:#e6e6e6" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>FECHA PREVISTA</b></p>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Env&iacute;o de la petici&oacute;n de informaci&oacute;n</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>10/11/2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Plazo l&iacute;mite para la recepci&oacute;n de consultas de los licitadores</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>15/11/21</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>Plazo l&iacute;mite para la Recepci&oacute;n de propuestas in&iacute;ciales</b></p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt; background:#a8d08d" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>19/11/21</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Revisi&oacute;n, valoraci&oacute;n y an&aacute;lisis de las propuestas</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.0pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>22 al 26/11/2021</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:346.35pt; border-top:none; border-left:solid #999999 1.5pt; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.0pt" width="462">
			<p class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; padding:1.4pt 5.4pt 1.4pt 5.4pt">Convocatoria de licitadores seleccionados a participar en la RFQ</p>
			</td>
			<td style="width:118.85pt; border-top:none; border-left:none; border-bottom:solid #999999 1.5pt; border-right:solid #999999 1.5pt" width="158">
			<p align="center" class="CxSpMiddle" style="margin-top:1.0pt; margin-right:0cm; margin-bottom:1.0pt; margin-left:0cm; text-align:center; padding:1.4pt 5.4pt 1.4pt 5.4pt"><b>29/11/2021</b></p>
			</td>
		</tr>
	</tbody>
</table>
</div>

<p>&nbsp;</p>', false, NULL, NULL, '2021-11-10 12:43:30', '2021-11-10 13:46:47', NULL, 14, 14, NULL, NULL, NULL, true, 999, NULL, 1, 'FACUNDO BACARDI I MASSO 7- 11, MOLLET DEL VALLES, Barcelona, 08100, SPAIN', NULL, 999, 'str_otras_condiciones_envio', 'K02. Electricity', false, false, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-19 16:00:00', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, true, true, false, NULL, false, true, false, false, true, true, 'avanzada', 'pendiente_decision', false, NULL, NULL, true, 3, 3, 20, NULL, 2, NULL, 1);


--
-- TOC entry 7489 (class 0 OID 0)
-- Dependencies: 1022
-- Name: subastas_id_subasta_seq; Type: SEQUENCE SET; Schema: public; Owner: rhc
--

--- SELECT pg_catalog.setval('public.subastas_id_subasta_seq', 248, true);


--
-- TOC entry 7290 (class 2606 OID 209065)
-- Name: subastas pk_subastas; Type: CONSTRAINT; Schema: public; Owner: rhc
--

--- ALTER TABLE ONLY public.subastas
---     ADD CONSTRAINT pk_subastas PRIMARY KEY (id_subasta);


--
-- TOC entry 7291 (class 1259 OID 210492)
-- Name: subastas_historico_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_historico_idx ON public.subastas USING btree (subabortada, subdesierto, subconfirmaadj);


--
-- TOC entry 7292 (class 1259 OID 210493)
-- Name: subastas_id_comprador; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_id_comprador ON public.subastas USING btree (id_comprador);


--
-- TOC entry 7293 (class 1259 OID 210494)
-- Name: subastas_rhc_cron1_fin_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_rhc_cron1_fin_idx ON public.subastas USING btree (subvalida, subborrador, subplantilla);


--
-- TOC entry 7294 (class 1259 OID 210495)
-- Name: subastas_rhc_cron2_fin_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_rhc_cron2_fin_idx ON public.subastas USING btree (subvalida, subborrador, subplantilla, subfinalizado);


--
-- TOC entry 7295 (class 1259 OID 210496)
-- Name: subastas_subabortada_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_subabortada_idx ON public.subastas USING btree (subabortada);


--
-- TOC entry 7296 (class 1259 OID 210497)
-- Name: subastas_subadjudicado_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_subadjudicado_idx ON public.subastas USING btree (subadjudicado);


--
-- TOC entry 7297 (class 1259 OID 210498)
-- Name: subastas_subautorizado_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_subautorizado_idx ON public.subastas USING btree (subautorizado);


--
-- TOC entry 7298 (class 1259 OID 210499)
-- Name: subastas_subborrador_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_subborrador_idx ON public.subastas USING btree (subborrador);


--
-- TOC entry 7299 (class 1259 OID 210500)
-- Name: subastas_subcie_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_subcie_idx ON public.subastas USING btree (subcie);


--
-- TOC entry 7300 (class 1259 OID 210501)
-- Name: subastas_subconfirmaadj_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_subconfirmaadj_idx ON public.subastas USING btree (subconfirmaadj);


--
-- TOC entry 7301 (class 1259 OID 210502)
-- Name: subastas_subdesierto_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_subdesierto_idx ON public.subastas USING btree (subdesierto);


--
-- TOC entry 7302 (class 1259 OID 210503)
-- Name: subastas_subfinalizado_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_subfinalizado_idx ON public.subastas USING btree (subfinalizado);


--
-- TOC entry 7303 (class 1259 OID 210504)
-- Name: subastas_subplantilla_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_subplantilla_idx ON public.subastas USING btree (subplantilla);


--
-- TOC entry 7304 (class 1259 OID 210505)
-- Name: subastas_subvalida_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX subastas_subvalida_idx ON public.subastas USING btree (subvalida);


--
-- TOC entry 7305 (class 2606 OID 211645)
-- Name: subastas fk_875bf3a4a1bbfed3; Type: FK CONSTRAINT; Schema: public; Owner: rhc
--

--- ALTER TABLE ONLY public.subastas
---     ADD CONSTRAINT fk_875bf3a4a1bbfed3 FOREIGN KEY (id_sede) REFERENCES public.sedes(id_sede);


--
-- TOC entry 7306 (class 2606 OID 212910)
-- Name: subastas fk_subastas_proyectos; Type: FK CONSTRAINT; Schema: public; Owner: rhc
--

--- ALTER TABLE ONLY public.subastas
---     ADD CONSTRAINT fk_subastas_proyectos FOREIGN KEY (id_proyecto) REFERENCES public.proyectos(id_proyecto);


--
-- TOC entry 7486 (class 0 OID 0)
-- Dependencies: 618
-- Name: TABLE subastas; Type: ACL; Schema: public; Owner: rhc
--

--- GRANT ALL ON TABLE public.subastas TO itbid WITH GRANT OPTION;


--
-- TOC entry 7488 (class 0 OID 0)
-- Dependencies: 1022
-- Name: SEQUENCE subastas_id_subasta_seq; Type: ACL; Schema: public; Owner: rhc
--

--- GRANT ALL ON SEQUENCE public.subastas_id_subasta_seq TO itbid WITH GRANT OPTION;


-- Completed on 2022-03-14 14:36:56 CET

--
-- PostgreSQL database dump complete
--

