--
-- PostgreSQL database dump
--

-- Dumped from database version 10.19
-- Dumped by pg_dump version 10.19

-- Started on 2022-03-14 14:38:11 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 836 (class 1259 OID 204336)
-- Name: relsubpro; Type: TABLE; Schema: public; Owner: rhc
--

--- Database
\c rhcitquot_157;

CREATE TABLE public.relsubpro (
    id_subasta integer,
    id_proveedor integer,
    id_contacto integer,
    rspr_factor numeric DEFAULT 100 NOT NULL,
    divisa integer,
    id integer NOT NULL,
    enviado boolean DEFAULT false,
    excel boolean DEFAULT false
);


--- ALTER TABLE public.relsubpro OWNER TO rhc;

--
-- TOC entry 837 (class 1259 OID 204345)
-- Name: relsubpro_id_seq; Type: SEQUENCE; Schema: public; Owner: rhc
--

--- CREATE SEQUENCE public.relsubpro_id_seq
---     START WITH 1
---     INCREMENT BY 1
---     NO MINVALUE
---     NO MAXVALUE
---     CACHE 1;


--- ALTER TABLE public.relsubpro_id_seq OWNER TO rhc;

--
-- TOC entry 7446 (class 0 OID 0)
-- Dependencies: 837
-- Name: relsubpro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rhc
--

--- ALTER SEQUENCE public.relsubpro_id_seq OWNED BY public.relsubpro.id;


--
-- TOC entry 7257 (class 2604 OID 207746)
-- Name: relsubpro id; Type: DEFAULT; Schema: public; Owner: rhc
--

--- ALTER TABLE ONLY public.relsubpro ALTER COLUMN id SET DEFAULT nextval('public.relsubpro_id_seq'::regclass);


--
-- TOC entry 7438 (class 0 OID 204336)
-- Dependencies: 836
-- Data for Name: relsubpro; Type: TABLE DATA; Schema: public; Owner: rhc
--

INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (12, 19, 18, 100, 1, 1, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (12, 1, 1, 100, 1, 2, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (12, 14, 13, 100, 1, 3, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (12, 17, 16, 100, 1, 4, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (3, 6, 6, 100, 1, 5, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (3, 3, 3, 100, 1, 6, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (3, 1, 1, 100, 1, 7, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (3, 4, 4, 100, 1, 8, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (3, 5, 5, 100, 1, 9, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (3, 2, 2, 100, 1, 10, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (3, 7, 7, 100, 1, 11, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (3, 10, 9, 100, 1, 12, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (3, 11, 10, 100, 1, 13, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (3, 9, 8, 100, 1, 14, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (3, 12, 11, 100, 1, 15, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (3, 13, 12, 100, 1, 16, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (12, 18, 17, 100, 1, 17, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (12, 16, 15, 100, 1, 18, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (12, 15, 14, 100, 1, 19, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (12, 20, 19, 100, 1, 20, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (15, 19, 18, 100, 1, 21, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (15, 1, 1, 98, 1, 22, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (15, 14, 13, 100, 1, 23, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (15, 18, 17, 102, 1, 24, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (15, 16, 15, 98, 1, 25, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (4, 3, 3, 100, 1, 26, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (4, 1, 1, 100, 1, 27, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (4, 4, 4, 100, 1, 28, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (4, 12, 11, 100, 1, 29, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (4, 9, 8, 100, 1, 30, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (15, 15, 14, 98, 1, 31, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (17, 22, 21, 100, 1, 40, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (6, 1, 1, 100, 1, 41, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (6, 12, 11, 100, 1, 42, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (6, 9, 8, 97, 1, 43, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (6, 4, 4, 103, 1, 44, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (6, 3, 3, 103, 1, 45, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 6, 6, 100, NULL, 46, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 9, 8, 100, NULL, 47, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 13, 12, 100, NULL, 48, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 1, 1, 100, NULL, 49, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 10, 9, 100, NULL, 50, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 2, 2, 100, NULL, 51, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 3, 3, 100, NULL, 52, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 11, 10, 100, NULL, 53, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 7, 7, 100, NULL, 54, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 12, 11, 100, NULL, 55, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 4, 4, 100, NULL, 56, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 5, 5, 100, NULL, 57, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 15, 14, 100, NULL, 58, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 19, 18, 100, NULL, 59, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 14, 13, 100, NULL, 60, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 18, 17, 100, NULL, 61, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 20, 19, 100, NULL, 62, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 16, 15, 100, NULL, 63, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (8, 17, 16, 100, NULL, 64, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (17, 24, 23, 100, 1, 65, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (17, 28, 26, 100, 1, 66, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (17, 25, 24, 100, 1, 67, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (17, 27, 25, 100, 1, 68, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (17, 23, 22, 100, 1, 69, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (17, 21, 20, 100, 1, 70, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (17, 29, 27, 100, 1, 71, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (17, 30, 28, 100, 1, 72, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (9, 19, 18, 100, 1, 73, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (9, 1, 1, 100, 1, 74, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (9, 14, 13, 100, 1, 75, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (9, 17, 16, 100, 1, 76, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (9, 18, 17, 100, 1, 77, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (9, 16, 15, 100, 1, 78, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (9, 15, 14, 100, 1, 79, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (9, 20, 19, 100, 1, 80, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (20, 34, 30, 100, 1, 85, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (20, 35, 31, 100, 1, 86, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 106, 101, 100, 1, 212, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (20, 37, 33, 100, 1, 88, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (20, 38, 34, 100, 1, 89, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (20, 39, 35, 100, 1, 90, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (20, 40, 36, 100, 1, 91, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 107, 102, 100, 1, 213, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 109, 104, 100, 1, 214, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 110, 105, 100, 1, 215, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 111, 106, 100, 1, 216, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (22, 52, 48, 100, 1, 109, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (22, 51, 47, 100, 1, 110, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (22, 56, 52, 100, 1, 111, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (22, 54, 50, 100, 1, 112, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (22, 53, 49, 100, 1, 113, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (22, 50, 46, 100, 1, 114, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (22, 55, 51, 100, 1, 115, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (22, 49, 45, 100, 1, 116, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (23, 52, 48, 100, 1, 121, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (23, 53, 49, 100, 1, 122, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (23, 49, 45, 100, 1, 123, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (23, 55, 51, 100, 1, 124, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (25, 52, 48, 100, NULL, 132, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (25, 56, 52, 100, NULL, 133, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (25, 54, 50, 100, NULL, 134, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (25, 53, 49, 100, NULL, 135, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (25, 50, 46, 100, NULL, 136, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (25, 55, 51, 100, NULL, 137, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (25, 49, 45, 100, NULL, 138, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (26, 64, 60, 100, 1, 150, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (26, 63, 59, 100, 1, 151, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (26, 62, 58, 100, 1, 152, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (26, 61, 57, 100, 1, 153, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (27, 74, 70, 100, 1, 159, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (27, 71, 67, 100, 1, 160, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (27, 73, 69, 100, 1, 161, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (27, 72, 68, 100, 1, 162, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (27, 70, 66, 100, 1, 163, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (28, 63, 59, 100.00, 1, 169, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (28, 61, 57, 127.07, 1, 170, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (28, 62, 58, 114.63, 1, 171, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (28, 64, 60, 106.56, 1, 172, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (30, 71, 67, 100, 1, 178, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (30, 72, 68, 100, 1, 179, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (30, 70, 66, 100, 1, 180, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 82, 77, 100, 1, 186, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 80, 75, 100, 1, 187, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 81, 76, 100, 1, 188, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 83, 78, 100, 1, 189, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 101, 96, 100, 1, 199, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 100, 95, 100, 1, 200, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 99, 94, 100, 1, 201, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 98, 93, 100, 1, 202, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 97, 92, 100, 1, 203, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 96, 91, 100, 1, 204, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 95, 90, 100, 1, 205, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 94, 89, 100, 1, 206, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 93, 88, 100, 1, 207, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (34, 111, 106, 100, 1, 245, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (34, 119, 114, 100, 1, 246, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (34, 118, 113, 100, 1, 247, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (34, 117, 112, 100, 1, 248, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (32, 120, 115, 100, 1, 249, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (45, 63, 59, 100, 1, 383, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (36, 136, 131, 100, 1, 269, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (36, 135, 130, 100, 1, 270, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (36, 138, 133, 100, 1, 271, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (36, 133, 128, 100, 1, 272, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (36, 134, 129, 100, 1, 273, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (36, 141, 136, 100, 1, 274, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (36, 139, 134, 100, 1, 275, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (36, 137, 132, 100, 1, 276, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (36, 140, 135, 100, 1, 277, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (36, 143, 138, 100, 1, 279, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 224, 219, 100, 1, 438, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 223, 218, 100, 1, 439, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 222, 217, 100, 1, 440, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 221, 216, 100, 1, 441, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 220, 215, 100, 1, 442, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 219, 214, 100, 1, 443, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 218, 213, 100, 1, 444, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (38, 93, 88, 100, 1, 299, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (38, 80, 75, 100, 1, 300, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (38, 99, 94, 100, 1, 301, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (38, 98, 93, 100, 1, 302, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (38, 110, 105, 100, 1, 303, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 217, 212, 100, 1, 445, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 216, 211, 100, 1, 446, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 215, 210, 100, 1, 447, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 214, 209, 100, 1, 448, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 213, 208, 100, 1, 449, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 212, 207, 100, 1, 450, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 211, 206, 100, 1, 451, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 210, 205, 100, 1, 452, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 209, 204, 100, 1, 453, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 208, 203, 100, 1, 454, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 207, 202, 100, 1, 455, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 206, 201, 100, 1, 456, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 205, 200, 100, 1, 457, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 204, 199, 100, 1, 458, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 203, 198, 100, 1, 459, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 202, 197, 100, 1, 460, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 201, 196, 100, 1, 461, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 225, 220, 100, 1, 462, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 226, 221, 100, 1, 463, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (39, 93, 88, 100, 1, 323, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (39, 99, 94, 100, 1, 324, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (39, 110, 105, 100, 1, 325, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 227, 222, 100, 1, 464, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 228, 223, 100, 1, 465, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 229, 224, 100, 1, 466, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 230, 225, 100, 1, 467, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 231, 226, 100, 1, 468, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 233, 228, 100, 1, 469, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 234, 229, 100, 1, 470, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (42, 155, 150, 100, 1, 333, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (42, 154, 149, 100, 1, 334, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (42, 153, 148, 100, 1, 335, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (42, 152, 147, 100, 1, 336, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (42, 151, 146, 100, 1, 337, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (42, 150, 145, 100, 1, 338, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (46, 232, 227, 100, 1, 471, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (44, 175, 170, 100, 1, 356, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (44, 174, 169, 100, 1, 357, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (44, 173, 168, 100, 1, 358, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (44, 172, 167, 100, 1, 359, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (44, 171, 166, 100, 1, 360, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (44, 170, 165, 100, 1, 361, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (44, 169, 164, 100, 1, 362, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (44, 168, 163, 100, 1, 363, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (44, 167, 162, 100, 1, 364, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (44, 166, 161, 100, 1, 365, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (44, 176, 171, 100, 1, 366, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (45, 19, 18, 100, 1, 375, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (45, 1, 1, 100, 1, 376, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (45, 14, 13, 100, 1, 377, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (45, 17, 16, 100, 1, 378, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (45, 18, 17, 100, 1, 379, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (45, 16, 15, 100, 1, 380, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (45, 15, 14, 100, 1, 381, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (45, 20, 19, 100, 1, 382, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (52, 219, 214, 100, 1, 496, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (52, 228, 223, 100, 1, 497, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (52, 205, 200, 100, 1, 498, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (53, 155, 150, 100, 1, 509, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (53, 150, 145, 100, 1, 510, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (53, 152, 147, 100, 1, 511, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (53, 153, 148, 100, 1, 512, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (53, 154, 149, 100, 1, 513, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (53, 151, 146, 100, 1, 514, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (53, 240, 234, 100, 1, 515, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (53, 239, 233, 100, 1, 516, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (55, 152, 147, 100, 1, 526, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (55, 240, 234, 100, 1, 527, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (55, 153, 148, 100, 1, 528, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (55, 151, 146, 100, 1, 529, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (55, 239, 233, 100, 1, 530, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (55, 244, 238, 100, 1, 531, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (55, 243, 237, 100, 1, 532, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (53, 247, 241, 100, 1, 539, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (53, 248, 242, 100, 1, 540, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (55, 247, 241, 100, 1, 541, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (55, 248, 242, 100, 1, 542, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (55, 250, 244, 100, 1, 545, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (57, 152, 147, 100, 1, 555, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (57, 240, 234, 100, 1, 556, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (57, 243, 237, 100, 1, 557, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (59, 259, 253, 100, 1, 575, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (59, 258, 252, 100, 1, 576, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (59, 257, 251, 100, 1, 577, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (59, 256, 250, 100, 1, 578, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (59, 261, 255, 100, 1, 581, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (64, 150, 145, 100, 1, 600, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (64, 152, 147, 100, 1, 601, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (64, 155, 150, 100, 1, 602, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (64, 240, 234, 100, 1, 603, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (64, 153, 148, 100, 1, 604, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (64, 154, 149, 100, 1, 605, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (64, 247, 241, 100, 1, 606, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (64, 263, 257, 100, 1, 609, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (64, 265, 259, 100, 1, 612, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (65, 277, 271, 100, 1, 630, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (65, 276, 270, 100, 1, 631, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (65, 275, 269, 100, 1, 632, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (65, 274, 268, 100, 1, 633, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (65, 273, 267, 100, 1, 634, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (65, 272, 266, 100, 1, 635, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 99, 94, 100, 1, 718, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 106, 101, 100, 1, 719, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 98, 93, 100, 1, 720, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 81, 76, 100, 1, 721, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 338, 332, 100, 1, 722, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 337, 331, 100, 1, 723, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 336, 330, 100, 1, 724, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 335, 329, 100, 1, 725, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 334, 328, 100, 1, 726, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 333, 327, 100, 1, 727, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 332, 326, 100, 1, 728, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 331, 325, 100, 1, 729, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 330, 324, 100, 1, 730, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 329, 323, 100, 1, 731, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 328, 322, 100, 1, 732, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 327, 321, 100, 1, 733, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 326, 320, 100, 1, 734, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 325, 319, 100, 1, 735, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 324, 318, 100, 1, 736, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (69, 323, 317, 100, 1, 737, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (70, 323, 317, 100, 1, 762, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (70, 324, 318, 100, 1, 763, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (70, 99, 94, 100, 1, 764, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (70, 106, 101, 100, 1, 765, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (70, 327, 321, 100, 1, 766, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (70, 328, 322, 100, 1, 767, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (70, 330, 324, 100, 1, 768, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (72, 323, 317, 100, 1, 800, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (72, 324, 318, 100, 1, 801, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (72, 99, 94, 100, 1, 802, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (72, 327, 321, 100, 1, 803, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (72, 330, 324, 100, 1, 804, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (74, 323, 317, 100, 1, 829, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (74, 324, 318, 100, 1, 830, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (74, 99, 94, 100, 1, 831, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (74, 327, 321, 100, 1, 832, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (74, 330, 324, 100, 1, 833, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (76, 343, 337, 100, 1, 848, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (76, 341, 335, 100, 1, 849, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (76, 342, 336, 100, 1, 850, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (76, 152, 147, 100, 1, 851, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (76, 240, 234, 100, 1, 852, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (76, 153, 148, 100, 1, 853, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (76, 154, 149, 100, 1, 854, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (76, 344, 338, 100, 1, 855, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (71, 340, 334, 100, 1, 856, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (71, 339, 333, 100, 1, 857, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (71, 72, 68, 100, 1, 858, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (71, 70, 66, 100, 1, 859, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (79, 341, 335, 100, 1, 874, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (79, 153, 148, 100, 1, 875, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (81, 341, 335, 104, NULL, 890, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (81, 153, 148, 100, NULL, 891, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (83, 341, 335, 100, 1, 906, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (83, 153, 148, 100, 1, 907, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (84, 324, 318, 100, 1, 932, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (84, 99, 94, 100, 1, 933, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (84, 330, 324, 100, 1, 934, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (86, 153, 148, 100, 1, 959, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (86, 344, 338, 100, 1, 960, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (86, 354, 348, 100, 1, 961, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (86, 353, 347, 100, 1, 962, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (86, 352, 346, 100, 1, 963, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (86, 351, 345, 100, 1, 964, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (86, 350, 344, 100, 1, 965, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (87, 366, 360, 100, 1, 1014, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (87, 367, 361, 100, 1, 1015, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (87, 368, 362, 100, 1, 1016, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (87, 370, 364, 100, 1, 1017, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (87, 365, 359, 100, 1, 1018, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (87, 369, 363, 100, 1, 1019, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (87, 373, 367, 100, 1, 1020, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (87, 372, 366, 100, 1, 1021, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (87, 371, 365, 100, 1, 1022, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (88, 366, 360, 100, 1, 1032, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (88, 367, 361, 100, 1, 1033, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (88, 368, 362, 100, 1, 1034, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (88, 370, 364, 100, 1, 1035, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (88, 365, 359, 100, 1, 1036, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (88, 369, 363, 100, 1, 1037, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (88, 373, 367, 100, 1, 1038, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (88, 372, 366, 100, 1, 1039, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (88, 371, 365, 100, 1, 1040, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (86, 376, 370, 100, 1, 1043, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (86, 378, 372, 100, 1, 1045, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (87, 380, 374, 100, 1, 1047, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (88, 380, 374, 100, 1, 1049, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (89, 376, 370, 100, 1, 1066, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (89, 354, 348, 100, 1, 1067, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (94, 386, 380, 100, 1, 1090, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (94, 385, 379, 100, 1, 1091, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (94, 384, 378, 100, 1, 1092, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (94, 388, 382, 100, 1, 1094, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (94, 390, 384, 100, 1, 1096, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (94, 392, 386, 100, 1, 1098, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (102, 448, 446, 100, 1, 1179, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (102, 463, 461, 100, 1, 1178, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (102, 462, 460, 100, 1, 1177, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (102, 461, 459, 100, 1, 1176, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (102, 460, 458, 100, 1, 1175, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (102, 459, 457, 100, 1, 1174, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (102, 458, 456, 100, 1, 1173, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (102, 457, 455, 100, 1, 1172, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (102, 453, 451, 100, 1, 1171, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (102, 452, 450, 100, 1, 1170, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (102, 447, 445, 100, 1, 1169, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 400, 394, 100, 1, 1104, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 398, 392, 100, 1, 1103, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 397, 391, 100, 1, 1102, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 396, 390, 100, 1, 1101, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 395, 389, 100, 1, 1100, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 394, 388, 100, 1, 1099, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 459, 457, 100, 1, 1161, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 460, 458, 100, 1, 1162, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 461, 459, 100, 1, 1163, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 447, 445, 100, 1, 1150, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 449, 447, 100, 1, 1152, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 452, 450, 100, 1, 1155, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 453, 451, 100, 1, 1156, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 458, 456, 100, 1, 1164, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 462, 460, 100, 1, 1165, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 428, 422, 100, 1, 1132, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 415, 409, 100, 1, 1119, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 416, 410, 100, 1, 1120, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 417, 411, 100, 1, 1121, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 418, 412, 100, 1, 1122, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 419, 413, 100, 1, 1123, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 420, 414, 100, 1, 1124, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 421, 415, 100, 1, 1125, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 422, 416, 100, 1, 1126, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 423, 417, 100, 1, 1127, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 424, 418, 100, 1, 1128, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 425, 419, 100, 1, 1129, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 426, 420, 100, 1, 1130, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 427, 421, 100, 1, 1131, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 463, 461, 100, 1, 1166, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 430, 424, 100, 1, 1134, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 433, 428, 100, 1, 1136, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (96, 435, 430, 100, 1, 1138, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 457, 455, 100, 1, 1160, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 448, 446, 100, 1, 1151, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 454, 452, 100, 1, 1157, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (99, 465, 464, 100, 1, 1168, false, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 547, 545, 100, 1, 1296, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 548, 546, 100, 1, 1297, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 549, 547, 100, 1, 1298, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 550, 548, 100, 1, 1299, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 551, 549, 100, 1, 1300, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (112, 540, 538, 100, 1, 1277, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 259, 253, 100, 1, 1213, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (112, 539, 537, 100, 1, 1276, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (112, 538, 536, 100, 1, 1275, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (112, 537, 535, 100, 1, 1274, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (112, 536, 534, 100, 1, 1273, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (112, 542, 540, 100, 1, 1279, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (112, 541, 539, 100, 1, 1278, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 468, 467, 100, 1, 1212, true, NULL);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (112, 543, 541, 100, 1, 1280, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (113, 540, 538, 100, 1, 1284, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (113, 539, 537, 100, 1, 1283, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (113, 537, 535, 100, 1, 1282, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (113, 536, 534, 100, 1, 1281, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 495, 494, 100, 1, 1222, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 504, 503, 100, 1, 1223, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 494, 493, 100, 1, 1211, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 106, 101, 100, 1, 1301, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 499, 498, 100, 1, 1218, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (114, 540, 538, 100, 1, 1286, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (114, 536, 534, 100, 1, 1285, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 257, 251, 100, 1, 1216, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 496, 495, 100, 1, 1214, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 497, 496, 100, 1, 1215, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 498, 497, 100, 1, 1217, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 500, 499, 100, 1, 1219, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 501, 500, 100, 1, 1220, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 502, 501, 100, 1, 1221, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (105, 503, 502, 100, 1, 1224, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (108, 117, 112, 100, 1, 1225, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (108, 119, 114, 100, 1, 1226, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (109, 468, 467, 100, 1, 1227, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (109, 503, 502, 100, 1, 1228, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (109, 504, 503, 100, 1, 1229, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (108, 521, 520, 100, 1, 1245, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (108, 522, 521, 100, 1, 1246, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (108, 523, 522, 100, 1, 1247, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (108, 524, 523, 100, 1, 1248, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (108, 525, 524, 100, 1, 1249, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (108, 526, 525, 100, 1, 1250, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (108, 527, 526, 100, 1, 1251, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (108, 528, 527, 100, 1, 1252, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (109, 530, 529, 100, 1, 1256, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (109, 257, 251, 100, 1, 1255, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (110, 119, 114, 100, 1, 1257, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 93, 88, 100, 1, 1302, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (110, 533, 532, 100, 1, 1272, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (110, 525, 524, 100, 1, 1271, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (110, 524, 523, 100, 1, 1270, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (110, 523, 522, 100, 1, 1269, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (110, 521, 520, 100, 1, 1268, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 552, 551, 100, 1, 1303, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 554, 553, 100, 1, 1304, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 555, 554, 100, 1, 1305, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 557, 556, 100, 1, 1306, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 559, 558, 100, 1, 1307, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 95, 90, 100, 1, 1308, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (121, 598, 597, 100, 1, 1357, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 585, 584, 100, 1, 1342, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (121, 597, 596, 100, 1, 1356, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 587, 586, 100, 1, 1344, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 340, 334, 100, 1, 1326, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 339, 333, 100, 1, 1325, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 277, 271, 100, 1, 1324, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 72, 68, 100, 1, 1320, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 70, 66, 100, 1, 1319, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 81, 76, 100, 1, 1287, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 82, 77, 100, 1, 1288, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 97, 92, 100, 1, 1289, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 99, 94, 100, 1, 1290, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 120, 115, 100, 1, 1291, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 323, 317, 100, 1, 1292, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 544, 542, 100, 1, 1293, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 545, 543, 100, 1, 1294, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 546, 544, 100, 1, 1295, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 589, 588, 100, 1, 1346, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 566, 565, 100, 1, 1314, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (118, 568, 567, 100, 1, 1316, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (120, 589, 588, 100, 1, 1349, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (120, 72, 68, 100, 1, 1348, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (120, 70, 66, 100, 1, 1347, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 582, 581, 100, 1, 1339, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 578, 577, 100, 1, 1335, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 579, 578, 100, 1, 1336, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 580, 579, 100, 1, 1337, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (119, 581, 580, 100, 1, 1338, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (123, 555, 554, 100, 1, 1381, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (124, 608, 609, 100, 1, 1370, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (121, 602, 601, 100, 1, 1361, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (121, 601, 600, 100, 1, 1360, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (121, 600, 599, 100, 1, 1359, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (121, 599, 598, 100, 1, 1358, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (122, 610, 611, 100, 1, 1369, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (122, 609, 610, 100, 1, 1368, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (122, 608, 609, 100, 1, 1367, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (122, 607, 608, 100, 1, 1366, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (123, 554, 553, 100, 1, 1380, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (123, 546, 544, 100, 1, 1379, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (123, 106, 101, 100, 1, 1378, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (123, 99, 94, 100, 1, 1377, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (123, 612, 613, 100, 1, 1376, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (123, 568, 567, 100, 1, 1375, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (123, 566, 565, 100, 1, 1374, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (125, 612, 613, 100, 1, 1390, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (125, 568, 567, 100, 1, 1389, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (125, 566, 565, 100, 1, 1388, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (125, 555, 554, 100, 1, 1387, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (125, 554, 553, 100, 1, 1386, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (125, 546, 544, 100, 1, 1385, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (125, 106, 101, 100, 1, 1384, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (125, 99, 94, 100, 1, 1383, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (125, 93, 88, 100, 1, 1382, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (137, 653, 655, 100, 1, 1457, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (137, 652, 654, 100, 1, 1456, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (137, 649, 651, 100, 1, 1455, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (137, 648, 650, 100, 1, 1454, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (137, 646, 648, 100, 1, 1453, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (126, 612, 613, 100, 1, 1397, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (126, 568, 567, 100, 1, 1396, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (126, 566, 565, 100, 1, 1395, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (126, 559, 558, 100, 1, 1394, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (126, 554, 553, 100, 1, 1393, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (126, 546, 544, 100, 1, 1392, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (126, 99, 94, 100, 1, 1391, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (152, 746, 749, 100, 1, 1558, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (127, 566, 565, 100, 1, 1402, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (127, 554, 553, 100, 1, 1401, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (127, 99, 94, 100, 1, 1400, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (127, 614, 615, 100, 1, 1399, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (150, 448, 446, 100, 1, 1538, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (150, 718, 720, 100, 1, 1534, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (150, 717, 719, 100, 1, 1533, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (144, 701, 703, 100, 1, 1498, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (144, 700, 702, 100, 1, 1497, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (144, 699, 701, 100, 1, 1496, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (144, 698, 700, 100, 1, 1495, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (144, 697, 699, 100, 1, 1494, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (144, 696, 698, 100, 1, 1493, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (144, 691, 693, 100, 1, 1492, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (144, 690, 692, 100, 1, 1491, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (144, 666, 668, 100, 1, 1490, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (144, 665, 667, 100, 1, 1489, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (144, 664, 666, 100, 1, 1488, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (146, 697, 699, 100, 1, 1502, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (146, 691, 693, 100, 1, 1501, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (146, 690, 692, 100, 1, 1500, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (146, 664, 666, 100, 1, 1499, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 685, 687, 100, 1, 1476, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 678, 680, 100, 1, 1469, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 679, 681, 100, 1, 1470, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 680, 682, 100, 1, 1471, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 681, 683, 100, 1, 1472, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 646, 648, 100, 1, 1434, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 647, 649, 100, 1, 1435, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 648, 650, 100, 1, 1436, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 649, 651, 100, 1, 1437, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 650, 652, 100, 1, 1438, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 651, 653, 100, 1, 1439, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 652, 654, 100, 1, 1440, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 653, 655, 100, 1, 1441, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 654, 656, 100, 1, 1442, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 655, 657, 100, 1, 1443, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 656, 658, 100, 1, 1444, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 658, 660, 100, 1, 1446, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 660, 662, 100, 1, 1448, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 682, 684, 100, 1, 1473, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (136, 663, 665, 100, 1, 1452, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 683, 685, 100, 1, 1474, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 684, 686, 100, 1, 1475, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 686, 688, 100, 1, 1477, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 687, 689, 100, 1, 1478, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 688, 690, 100, 1, 1479, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 693, 695, 100, 1, 1481, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (147, 461, 459, 100, 1, 1523, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (139, 695, 697, 100, 1, 1483, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (147, 448, 446, 100, 1, 1522, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (147, 721, 723, 100, 1, 1521, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (147, 720, 722, 100, 1, 1520, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (147, 719, 721, 100, 1, 1519, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (143, 695, 697, 100, 1, 1487, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (143, 693, 695, 100, 1, 1486, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (143, 684, 686, 100, 1, 1485, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (143, 678, 680, 100, 1, 1484, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (147, 718, 720, 100, 1, 1518, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (147, 717, 719, 100, 1, 1517, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (147, 716, 718, 100, 1, 1516, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (154, 70, 66, 100, 1, 1588, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (154, 72, 68, 100, 1, 1589, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (152, 750, 753, 100, 1, 1562, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (152, 752, 755, 100, 1, 1564, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (152, 754, 757, 100, 1, 1566, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (152, 763, 766, 100, 1, 1575, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (154, 272, 266, 100, 1, 1590, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (154, 273, 267, 100, 1, 1591, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (154, 274, 268, 100, 1, 1592, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (154, 462, 460, 100, 1, 1593, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (154, 580, 579, 100, 1, 1594, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (158, 373, 367, 100, 1, 1599, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (154, 585, 584, 100, 1, 1595, false, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (154, 773, 776, 100, 1, 1587, false, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (158, 372, 366, 100, 1, 1598, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (159, 763, 766, 100, 1, 1608, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (159, 781, 784, 100, 1, 1607, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (159, 780, 783, 100, 1, 1606, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (155, 801, 804, 100, 1, 1627, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (162, 371, 365, 100, 1, 1674, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (162, 367, 361, 100, 1, 1673, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (162, 365, 359, 100, 1, 1677, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (162, 835, 838, 100, 1, 1672, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (161, 19, 18, 100, 1, 1652, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (162, 834, 837, 100, 1, 1671, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (162, 837, 840, 100, 1, 1676, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (162, 842, 845, 100, 1, 1681, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (161, 17, 16, 100, 1, 1637, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (161, 14, 13, 100, 1, 1634, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (161, 1, 1, 100, 1, 1633, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (161, 801, 804, 100, 1, 1646, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (161, 811, 814, 100, 1, 1650, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (161, 812, 815, 100, 1, 1651, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (161, 814, 817, 100, 1, 1654, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (161, 819, 822, 100, 1, 1656, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (161, 820, 823, 100, 1, 1657, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (163, 15, 14, 100, 1, 1783, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (163, 801, 804, 100, 1, 1782, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (163, 678, 680, 100, 1, 1781, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (163, 19, 18, 100, 1, 1780, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (163, 16, 15, 100, 1, 1779, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (163, 820, 823, 100, 1, 1778, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (149, 918, 921, 100, 1, 1757, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (164, 19, 18, 100, 1, 1795, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (164, 16, 15, 100, 1, 1794, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (164, 15, 14, 100, 1, 1793, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (164, 678, 680, 100, 1, 1787, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (169, 801, 804, 100, 1, 1799, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (169, 678, 680, 100, 1, 1798, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (169, 19, 18, 100, 1, 1797, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (169, 16, 15, 100, 1, 1801, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (149, 739, 742, 100, 1, 1551, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (149, 765, 768, 100, 1, 1577, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (149, 788, 791, 100, 1, 1615, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (149, 789, 792, 100, 1, 1616, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (149, 828, 831, 100, 1, 1665, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (149, 829, 832, 100, 1, 1666, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (149, 832, 835, 100, 1, 1669, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (149, 875, 878, 100, 1, 1714, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (149, 929, 932, 100, 1, 1768, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (164, 801, 804, 100, 1, 1796, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (169, 15, 14, 100, 1, 1800, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 976, 985, 100, 1, 1850, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 589, 588, 100, 1, 1842, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 585, 584, 100, 1, 1841, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 582, 581, 100, 1, 1840, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 580, 579, 100, 1, 1839, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 340, 334, 100, 1, 1838, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 339, 333, 100, 1, 1837, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 273, 267, 100, 1, 1836, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 72, 68, 100, 1, 1835, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 70, 66, 100, 1, 1834, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (177, 980, 989, 100, 1, 1884, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (173, 952, 958, 100, 1, 1817, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (173, 950, 956, 100, 1, 1815, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (173, 949, 955, 100, 1, 1814, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (173, 948, 954, 100, 1, 1813, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (173, 947, 953, 100, 1, 1812, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (173, 946, 952, 100, 1, 1811, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (177, 979, 988, 100, 1, 1883, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (175, 948, 954, 100, 1, 1872, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (175, 949, 955, 100, 1, 1873, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (175, 992, 1002, 100, 1, 1876, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (175, 991, 1001, 100, 1, 1875, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (175, 947, 953, 100, 1, 1871, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (175, 946, 952, 100, 1, 1870, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (175, 950, 956, 100, 1, 1877, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (181, 781, 784, 100, 1, 1887, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (181, 994, 1006, 100, 1, 1888, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (181, 993, 1005, 100, 1, 1889, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (181, 395, 389, 100, 1, 1890, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (181, 995, 1008, 100, 1, 1891, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (181, 394, 388, 100, 1, 1892, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (181, 996, 1012, 100, 1, 1893, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (182, 996, 1012, 100, 1, 1898, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (182, 994, 1006, 100, 1, 1897, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (182, 781, 784, 100, 1, 1896, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (182, 395, 389, 100, 1, 1895, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (182, 394, 388, 100, 1, 1894, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (195, 1139, 1159, 100, 1, 1913, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (195, 546, 544, 100, 1, 1912, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (195, 1189, 1216, 100, 1, 1914, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 982, 991, 100, 1, 1856, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 981, 990, 100, 1, 1855, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 980, 989, 100, 1, 1854, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 979, 988, 100, 1, 1853, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 978, 987, 100, 1, 1852, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (174, 977, 986, 100, 1, 1851, true, true);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (189, 543, 541, 100, 1, 1902, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (189, 542, 540, 100, 1, 1901, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (189, 540, 538, 100, 1, 1900, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (189, 537, 535, 100, 1, 1899, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (189, 998, 1017, 100, 1, 1903, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (177, 580, 579, 100, 1, 1881, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (212, 580, 579, 100, 1, 1945, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (177, 72, 68, 100, 1, 1879, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (177, 70, 66, 100, 1, 1878, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (177, 981, 990, 100, 1, 1882, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (200, 546, 544, 100, 1, 1916, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (192, 537, 535, 100, 1, 1907, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (192, 540, 538, 100, 1, 1906, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (192, 542, 540, 100, 1, 1905, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (192, 543, 541, 100, 1, 1904, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (200, 1139, 1159, 100, 1, 1915, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (194, 546, 544, 100, 1, 1910, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (201, 1139, 1159, 100, 1, 1918, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (194, 1139, 1159, 100, 1, 1911, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (201, 546, 544, 100, 1, 1917, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (202, 1139, 1159, 100, 1, 1920, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (202, 546, 544, 100, 1, 1919, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (203, 1139, 1159, 100, 1, 1922, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (203, 546, 544, 100, 1, 1921, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (212, 2040, 2088, 100, 1, 1946, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (204, 1139, 1159, 100, 1, 1923, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (212, 2053, 2101, 100, 1, 1947, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (207, 447, 445, 100, 1, 1932, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (205, 546, 544, 100, 1, 1925, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (205, 1139, 1159, 100, 1, 1924, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (207, 1982, 2015, 100, 1, 1931, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (207, 1980, 2012, 100, 1, 1930, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (207, 1977, 2009, 100, 1, 1929, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (207, 720, 722, 100, 1, 1928, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (207, 460, 458, 100, 1, 1927, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (207, 448, 446, 100, 1, 1926, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (208, 1982, 2015, 100, 1, 1935, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (208, 460, 458, 100, 1, 1934, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (208, 448, 446, 100, 1, 1933, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (210, 792, 795, 100, 1, 1936, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (212, 589, 588, 100, 1, 1942, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (212, 585, 584, 100, 1, 1941, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (212, 340, 334, 100, 1, 1940, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (212, 339, 333, 100, 1, 1939, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (212, 72, 68, 100, 1, 1938, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (212, 70, 66, 100, 1, 1937, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (212, 981, 990, 100, 1, 1943, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (212, 1002, 1021, 100, 1, 1944, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (213, 2054, 2102, 0, 1, 1950, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (213, 2006, 2047, 0, 1, 1949, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (213, 2050, 2098, 0, 1, 1948, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (248, 2054, 2102, 0, 1, 1985, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (248, 2050, 2098, 0, 1, 1984, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (248, 2006, 2047, 0, 1, 1983, true, false);
INSERT INTO public.relsubpro (id_subasta, id_proveedor, id_contacto, rspr_factor, divisa, id, enviado, excel) VALUES (248, 72, 68, 0, 1, 1982, true, false);


--
-- TOC entry 7448 (class 0 OID 0)
-- Dependencies: 837
-- Name: relsubpro_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rhc
--

--- SELECT pg_catalog.setval('public.relsubpro_id_seq', 1985, true);


--
-- TOC entry 7264 (class 2606 OID 209239)
-- Name: relsubpro relsubpro_pkey; Type: CONSTRAINT; Schema: public; Owner: rhc
--

--- ALTER TABLE ONLY public.relsubpro
---     ADD CONSTRAINT relsubpro_pkey PRIMARY KEY (id);


--
-- TOC entry 7260 (class 1259 OID 210441)
-- Name: index_relsubpro_subasta; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX index_relsubpro_subasta ON public.relsubpro USING btree (id_subasta);


--
-- TOC entry 7261 (class 1259 OID 210485)
-- Name: relsubpro_id_contacto_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX relsubpro_id_contacto_idx ON public.relsubpro USING btree (id_contacto);


--
-- TOC entry 7262 (class 1259 OID 210486)
-- Name: relsubpro_id_proveedor_idx; Type: INDEX; Schema: public; Owner: rhc
--

--- CREATE INDEX relsubpro_id_proveedor_idx ON public.relsubpro USING btree (id_proveedor);


--
-- TOC entry 7265 (class 2606 OID 211160)
-- Name: relsubpro fk_551f5706779aa5a9; Type: FK CONSTRAINT; Schema: public; Owner: rhc
--

--- ALTER TABLE ONLY public.relsubpro
---     ADD CONSTRAINT fk_551f5706779aa5a9 FOREIGN KEY (id_contacto) REFERENCES public.contactos(id_contacto);


--
-- TOC entry 7266 (class 2606 OID 211165)
-- Name: relsubpro fk_551f570696f5d4e9; Type: FK CONSTRAINT; Schema: public; Owner: rhc
--

--- ALTER TABLE ONLY public.relsubpro
---     ADD CONSTRAINT fk_551f570696f5d4e9 FOREIGN KEY (id_proveedor) REFERENCES public.proveedores(id_proveedor);


--
-- TOC entry 7445 (class 0 OID 0)
-- Dependencies: 836
-- Name: TABLE relsubpro; Type: ACL; Schema: public; Owner: rhc
--

--- GRANT ALL ON TABLE public.relsubpro TO itbid WITH GRANT OPTION;


--
-- TOC entry 7447 (class 0 OID 0)
-- Dependencies: 837
-- Name: SEQUENCE relsubpro_id_seq; Type: ACL; Schema: public; Owner: rhc
--

--- GRANT ALL ON SEQUENCE public.relsubpro_id_seq TO itbid WITH GRANT OPTION;


-- Completed on 2022-03-14 14:38:11 CET

--
-- PostgreSQL database dump complete
--

