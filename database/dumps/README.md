Downloading all three databases is unfeasible because they are larger than 17 GB.

We need to download tables separately. Only data without references to unused objects.

    01_rhcitquot_157_plain_20220314_proveedores.sql
    01_rhcitquot_157_plain_20220314_subastas.sql
    01_rhcitquot_157_plain_20220314_variantes.sql
    01_rhcitquot_157_plain_20220314_ofertas.sql
    01_rhcitquot_157_plain_20220314_relsubpro.sql
    01_rhcitquot_157_plain_20220314_cat_proveedores.sql
    01_rhcitquot_157_plain_20220314_ofecriterios.sql
    01_rhcitquot_157_plain_20220314_criterios.sql

Then, comment these lines:

    - CREATE 
    - GRANT 
    - ALTER 
    - SELECT pg_catalog.setval
    - nextval

To do the same with this database:

    02_rhcitquot_220_plain_20220314_proveedores.sql
    02_rhcitquot_220_plain_20220314_subastas.sql
    02_rhcitquot_220_plain_20220314_variantes.sql
    02_rhcitquot_220_plain_20220314_ofertas.sql
    02_rhcitquot_220_plain_20220314_relsubpro.sql
    02_rhcitquot_220_plain_20220314_cat_proveedores.sql
    02_rhcitquot_220_plain_20220314_ofecriterios.sql
    02_rhcitquot_220_plain_20220314_criterios.sql

This database is too big:

    03_rhcitquot_250_plain_20220314_proveedores.sql
    03_rhcitquot_250_plain_20220314_subastas.sql
    03_rhcitquot_250_plain_20220314_variantes.sql
    03_rhcitquot_250_plain_20220314_ofertas.sql
    03_rhcitquot_250_plain_20220314_relsubpro.sql
    03_rhcitquot_250_plain_20220314_cat_proveedores.sql
    03_rhcitquot_250_plain_20220314_ofecriterios.sql
    03_rhcitquot_250_plain_20220314_criterios.sql
