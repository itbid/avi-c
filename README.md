# You can run the AI recommender model in three ways:

1. By installing all the required apps and libraries in your computer and running the AI recommender script using the command line.
   
2. By using pre-configured docker containers and running the AI recommender script from the command line inside the model's container.
   
3. By using pre-configured docker containers and using the graphical interface.

---

## Run the AI recommender model by installing all the required apps and libraries in your computer and running it using the command line

* Install Python 3 in your computer (macOS)

    ```
    brew install python
    brew install python-tk
    ```

* Install Python Libraries

    Run the following command to install all required python libraries:
    ```
    cd model
    pip3 install -r requirements.txt
    ```

* Run test with Affinity client (id=2081) 

    ```
    python3 recommend.py --subcat 1 --pesos 1 1 1 --json
    ```
---

## Install the pre-configured docker containers

* Close the RabbitMQ page and the pgAdmin page if they are open. 

* Install the system using Docker-Compose

    Run the following command line:
    ```
    ./install.sh
    ```
    Wait until the database container finishes loading data.
    Run the following command line:
    ```
    docker logs -t <postgres_container_id>
    ```
    If you see "INSERT" instructions, the loading is not finished. You have to see:
    ```
    database system is ready to accept connections
    ```

* See logs for ensuring that all containers are ready:

    ```
    docker logs -t <postgres_container_id>
    ...
    database system is ready to accept connections
    ```
    ```
    docker logs -t <rabbitmq_container_id>
    ...
    accepting AMQP connection
    ```
    ```
    docker logs -t <interface_container_id>
    ...
    Compiled successfully.
    ```
    ```
    docker logs -t <model_container_id>
    ...
    DO_NOT_WORRY_THIS_IS_OK_FOR_NOT_CLOSING_THE_CONTAINER
    ```



* Problems & Solutions:
    - Run again the "install.sh" script if this error appears: "npm ERR! code ERR_SOCKET_TIMEOUT"


---

## Run the Docker's AI recommender model from command line

* Open the model's container:

    ```
    docker exec -it model bash
    ```

* Run test 

    ```
    python3 recommend.py --subcat 1 --pesos 1 1 1 --json
    ```
---

## Run the Docker's AI recommender model using the graphical interface

* The interface will be accessed at:

    ```
    http://localhost:4200/
    ```

---

## Access to the Database using pgAdmin

* Access the pgAdmin website at:

    ```
    http://localhost:5050
    user: admin@admin.com
    pass: root
    ```

* Connection to the local database:
    ```
    Servers > Register > Server...
    General >
    Name: Local Database
    Connection >
    Host: localhost <--- or docker inspect <container> and get the IPAddress
    Port: 5432
    Maintenance database: postgres
    Username: postgres
    Password: root
    Save password: True
    ```

---

## Access to the RabbitMQ broker

* Access the RabbitMQ broker website at:

    ```
    http://localhost:15672
    user: guest
    pass: guest
    ```

    You will see two pre-created queues: "clients" and "results".
